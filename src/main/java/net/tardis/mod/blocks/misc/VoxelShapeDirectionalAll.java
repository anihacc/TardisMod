package net.tardis.mod.blocks.misc;

import net.minecraft.util.Direction;
import net.minecraft.util.math.shapes.VoxelShape;
import net.tardis.mod.helper.VoxelShapeUtils;

public class VoxelShapeDirectionalAll {

    private VoxelShape defaultShape;
    private VoxelShape eastShape;
    private VoxelShape southShape;
    private VoxelShape westShape;
    private VoxelShape upShape;
    private VoxelShape downShape;

    public VoxelShapeDirectionalAll(VoxelShape shape, VoxelShape up, VoxelShape down) {
        this.defaultShape = shape;
        this.eastShape = VoxelShapeUtils.rotate(shape, Direction.WEST);
        this.southShape = VoxelShapeUtils.rotate(shape, Direction.NORTH);
        this.westShape = VoxelShapeUtils.rotate(shape, Direction.EAST);
        this.upShape = up;
        this.downShape = down;
    }

    public VoxelShape getFor(Direction dir) {
        switch (dir) {
            case EAST:
                return eastShape;
            case SOUTH:
                return southShape;
            case WEST:
                return westShape;
            case UP:
            	return this.upShape;
            case DOWN:
            	return this.downShape;
            default:
                return this.defaultShape;
        }
    }
}
