package net.tardis.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.client.particle.ParticleManager;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class SolidTileBlock extends TileBlock{

    public SolidTileBlock(Properties prop) {
        super(prop);
    }
}
