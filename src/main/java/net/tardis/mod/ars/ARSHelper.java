package net.tardis.mod.ars;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ArmorStandEntity;
import net.minecraft.entity.item.ItemFrameEntity;
import net.minecraft.entity.item.LeashKnotEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.items.CapabilityItemHandler;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.items.DataCrystalItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tags.TardisBlockTags;
import net.tardis.mod.tileentities.ReclamationTile;
import net.tardis.mod.tileentities.WaypointBankTile;

public class ARSHelper {

	public static List<ItemStack> fillReclamationUnitAndRemove(World world, BlockPos start, BlockPos end){
	    return fillReclamationUnitAndRemove(world, start, end, false);
	}

	public static List<ItemStack> fillReclamationUnitAndRemove(World world, BlockPos start, BlockPos end, boolean removeEntities) {
		
		List<ItemStack> allInvs = Lists.newArrayList();
		
		BlockPos.getAllInBox(start, end).forEach(pos -> {
			TileEntity tile = world.getTileEntity(pos);

			//If this is not a tile, we can't add anything
			if(tile == null) {
				world.setBlockState(pos, Blocks.AIR.getDefaultState(), 34);
				return;
			}

			//Do not save contents from tiles whose blocks are in our block tag. Fixes an item duplication exploit like leaving items in Modded Ender Chests which use the item handler capability
			if (tile.getBlockState().isIn(TardisBlockTags.RECLAMATION_BLACKLIST)) {
			    return;
			}
			if(tile instanceof IInventory) {
				IInventory inv = (IInventory)tile;
				for(int i = 0; i < inv.getSizeInventory(); ++ i) {
					ItemStack stack = inv.getStackInSlot(i);
					if(!stack.isEmpty())
						allInvs.add(stack.copy());
				}
			}
			else if(tile != null) {
				//Handle vanilla containers and those that use item handler capability
				tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(cap -> {
					for(int i = 0; i < cap.getSlots(); ++i) {
						ItemStack stack = cap.getStackInSlot(i);
						if(!stack.isEmpty())
							allInvs.add(stack.copy());
					}
				});
			}
			//Waypoint saving
			if(tile instanceof WaypointBankTile) {
				List<SpaceTimeCoord> stored = Lists.newArrayList(((WaypointBankTile)tile).getWaypoints());
				stored.removeIf(coord -> coord.equals(SpaceTimeCoord.UNIVERAL_CENTER));
				
				if(!stored.isEmpty()) {
					ItemStack stack = new ItemStack(TItems.DATA_CRYSTAL.get());
					DataCrystalItem.setStoredWaypoints(stack, stored);
					allInvs.add(stack);
				}
			}

			world.setBlockState(pos, Blocks.AIR.getDefaultState(), 34);
		});
		
		if (removeEntities) {
			AxisAlignedBB killBox = new AxisAlignedBB(start, end);
			for(Entity entity : world.getEntitiesWithinAABB(Entity.class, killBox)){
				
				if(entity.getTags().contains(TardisConstants.Strings.INTERIOR_KILL_TAG)) //TODO: Document this. Tell people that you can use /tag to add "tardis_interior_change_kill" tag to blacklist entities to kill them when interior change, regardless of type
					entity.remove();
				
				if(!(entity instanceof LivingEntity))
					entity.remove();
				
				if(entity instanceof ArmorStandEntity) {
					for (ItemStack armor : entity.getArmorInventoryList())
						allInvs.add(armor.copy());
					entity.remove();
				}
				if (entity instanceof ItemFrameEntity) {
					allInvs.add(((ItemFrameEntity)entity).getDisplayedItem());
					entity.remove();
				}
				if (entity instanceof LeashKnotEntity) {
					allInvs.add(new ItemStack(Items.LEAD));
					entity.remove();
				}
				
			}
		}

		return allInvs;
	}
	
	public static void spawnReclamationUnit(World world, BlockPos spawnPos, List<ItemStack> stacksToStore) {
		if(!stacksToStore.isEmpty()) {
			if (world.getBlockState(spawnPos).getBlock() != TBlocks.reclamation_unit.get())
			    world.setBlockState(spawnPos, TBlocks.reclamation_unit.get().getDefaultState());
			TileEntity te = world.getTileEntity(spawnPos);
			if(te instanceof ReclamationTile) {
				ReclamationTile unit = (ReclamationTile)te;
				for(ItemStack stack : stacksToStore) {
					unit.addItemStack(stack);
				}
			}
		}
	}
}
