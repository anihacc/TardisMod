package net.tardis.mod.misc;

import java.util.List;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public interface IItemTooltipProvider {
    /** Add tooltips that usually display statistical information like energy values*/
    default void createStatisticTooltips(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {}
    /** Add tooltips relevant to description of the item*/
    default void createDescriptionTooltips(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {}
    /** Add tooltips that display by default, when shift or control keys are not held*/
    default void createDefaultTooltips(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {}
    
    /** Add tooltips that usually display statistical information like energy values*/
    default void createStatisticTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {}
    /** Add tooltips relevant to description of the item*/
    default void createDescriptionTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {}
    /** Add tooltips that display by default, when shift or control keys are not held*/
    default void createDefaultTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {}

    boolean shouldShowTooltips();
    
    void setShowTooltips(boolean showTooltips);
    
    boolean hasStatisticsTooltips();
    
    void setHasStatisticsTooltips(boolean hasStatisticsTooltips);
    
    boolean hasDescriptionTooltips();
    
    void setHasDescriptionTooltips(boolean hasDescriptionTooltips);

}
