package net.tardis.mod.misc;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Supplier;

import org.apache.logging.log4j.Logger;

import com.google.gson.JsonElement;
import com.mojang.serialization.Codec;
import com.mojang.serialization.JsonOps;

import net.minecraft.util.ResourceLocation;
/** Generic template to allow us to populate some default data from code*/
public class PrepopulatedCodecListener<T> extends CodecJsonDataListener<T> {
    
	private Supplier<Map<ResourceLocation, T>> initialValues;
	
	public PrepopulatedCodecListener(String folderName, Codec<T> codec, Logger logger, Supplier<Map<ResourceLocation, T>> initialValues) {
		super(folderName, codec, logger);
		this.initialValues = initialValues;
	}

	@Override
	public Map<ResourceLocation, T> mapValues(Map<ResourceLocation, JsonElement> inputs) {
		Map<ResourceLocation, T> map = this.initialValues.get();
		Map<ResourceLocation, T> parsedInData = new HashMap<>();
		for (Entry<ResourceLocation, JsonElement> entry : inputs.entrySet()){
			ResourceLocation key = entry.getKey();
			JsonElement element = entry.getValue();
			// if we fail to parse json, log an error and continue
			// if we succeeded, add the resulting object to the map
			this.codec.decode(JsonOps.INSTANCE, element)
				.get()
				.ifLeft(result -> {parsedInData.put(key, result.getFirst()); this.logger.info("Added Datapack entry: {}", key.toString());})
				.ifRight(partial -> this.logger.error("Failed to parse data json for {} due to: {}", key.toString(), partial.message()));
		}
		
		map.putAll(parsedInData);
		return map;
	}
}
