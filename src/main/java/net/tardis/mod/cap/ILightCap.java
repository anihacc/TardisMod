package net.tardis.mod.cap;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;

import java.util.List;

public interface ILightCap extends INBTSerializable<CompoundNBT> {

    List<BlockPos> getLightPoses();

    void addLightPos(BlockPos pos);

    void onLoad();

    void setLight(int level);


    public static class LightProvider implements ICapabilitySerializable<CompoundNBT> {

        private ILightCap light;

        public LightProvider(ILightCap light) {
            this.light = light;
        }

        @SuppressWarnings("unchecked")
        @Override
        public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
            return cap == Capabilities.LIGHT ? (LazyOptional<T>) LazyOptional.of(() -> light) : LazyOptional.empty();
        }

        @Override
        public CompoundNBT serializeNBT() {
            return light.serializeNBT();
        }

        @Override
        public void deserializeNBT(CompoundNBT nbt) {
            light.deserializeNBT(nbt);
        }

    }

    /** 1.17: IStorage interface removed with no replacement. Move these methods to the provider that implements {@linkplain ICapabilitySerializable}*/
    @Deprecated
    public static class LightStorage implements IStorage<ILightCap> {

        @Override
        public INBT writeNBT(Capability<ILightCap> capability, ILightCap instance, Direction side) {
            return instance.serializeNBT();
        }

        @Override
        public void readNBT(Capability<ILightCap> capability, ILightCap instance, Direction side, INBT nbt) {
            instance.deserializeNBT((CompoundNBT) nbt);
        }

    }
}
