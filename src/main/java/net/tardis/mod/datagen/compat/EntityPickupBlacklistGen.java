package net.tardis.mod.datagen.compat;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.EntityTypeTagsProvider;
import net.minecraft.entity.EntityType;
import net.minecraft.tags.ITag;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.tags.TardisEntityTypeTags;
/** Data Generator to prevent our entities being moved by mods like Carry On, which can cause a game crash*/
public class EntityPickupBlacklistGen extends EntityTypeTagsProvider{
	
	public static final ITag.INamedTag<EntityType<?>> CARRY_ON_PROOF = TardisEntityTypeTags.makeEntity(new ResourceLocation("carryon", "entity_blacklist"));
    
	private List<EntityType<?>> blacklistedTypes = new ArrayList<>();
	
	public EntityPickupBlacklistGen(DataGenerator generator, ExistingFileHelper existingFileHelper) {
		super(generator, Tardis.MODID, existingFileHelper);
	}

	@Override
	protected void registerTags() {
		this.setupBlacklist();
		for (EntityType<?> type : ForgeRegistries.ENTITIES) {
			if (blacklistedTypes.contains(type)) {
				add(CARRY_ON_PROOF, type);
			}
		}
	}
	
	public void add(ITag.INamedTag<EntityType<?>> branch, EntityType<?> type) {
        this.getOrCreateBuilder(branch).add(type);
    }

    public void add(ITag.INamedTag<EntityType<?>> branch, EntityType<?>... type) {
        this.getOrCreateBuilder(branch).add(type);
    }
    
    private void setupBlacklist() {
    	blacklistedTypes.add(TEntities.DISPLAY_TARDIS.get());
    	blacklistedTypes.add(TEntities.TARDIS.get());
    	blacklistedTypes.add(TEntities.CONTROL.get());
    	blacklistedTypes.add(TEntities.CHAIR.get());
    	blacklistedTypes.add(TEntities.DOOR.get());
    	blacklistedTypes.add(TEntities.TARDIS_BACKDOOR.get());
    	blacklistedTypes.add(TEntities.LASER.get());
    }

}
