package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.sounds.InteriorHum;

public class ChangeHumMessage {
    
	private InteriorHum hum;
	private boolean hasHumChanged;

    public ChangeHumMessage(InteriorHum name) {
        this.hum = name;
    }
    
    public ChangeHumMessage(InteriorHum hum, boolean hasHumChanged){
        this.hum = hum;
        this.hasHumChanged = hasHumChanged;
    }

    public static void encode(ChangeHumMessage mes, PacketBuffer buf) {
        buf.writeRegistryId(mes.hum);
        buf.writeBoolean(mes.hasHumChanged);
    }

    public static ChangeHumMessage decode(PacketBuffer buf) {
        return new ChangeHumMessage(buf.readRegistryId(), buf.readBoolean());
    }

    public static void handle(ChangeHumMessage mes, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> {
        	ctx.get().getSender().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
        	    data.getInteriorEffectsHandler().setHum(mes.hum);
        	    data.getInteriorEffectsHandler().setHumChanged(mes.hasHumChanged);
            });
        });
        ctx.get().setPacketHandled(true);
    }
}

