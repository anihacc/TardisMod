package net.tardis.mod.network.packets;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.client.ClientPacketHandler;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.TelepathicUtils.Search;


public class VMGetWorldsMessage {
	
	private List<RegistryKey<World>> worlds = new ArrayList<>();
	
	public VMGetWorldsMessage(List<RegistryKey<World>> worlds) {
		this.worlds.clear();
		this.worlds.addAll(worlds);
	}
	
	public static VMGetWorldsMessage create(MinecraftServer server) {
		List<RegistryKey<World>> worldsToAdd = new ArrayList<>();
	    for (ServerWorld world : server.getWorlds()) {
	        if (WorldHelper.canVMTravelToDimension(world))
	            worldsToAdd.add(world.getDimensionKey());
	    }
	    return new VMGetWorldsMessage(worldsToAdd);
	}


	public static void encode(VMGetWorldsMessage mes,PacketBuffer buf) {
		buf.writeInt(mes.worlds.size());
		for(RegistryKey<World> key : mes.worlds) {
			buf.writeString(key.getLocation().toString());
		}
	}
	
	public static VMGetWorldsMessage decode(PacketBuffer buf) {
		int size = buf.readInt();
		List<RegistryKey<World>> worlds = new ArrayList<>();
	    for(int i = 0; i < size; ++i) {
	    	String key = buf.readString(TardisConstants.PACKET_STRING_LENGTH);
			RegistryKey<World> world = RegistryKey.getOrCreateKey(Registry.WORLD_KEY, new ResourceLocation(key));
		    worlds.add(world);
        }
		return new VMGetWorldsMessage(worlds);
	}
	
    public static void handle(VMGetWorldsMessage mes,  Supplier<NetworkEvent.Context> ctx){
        ctx.get().enqueueWork(()->{
    	    ClientPacketHandler.handleVMWorlds(mes);
        });
        ctx.get().setPacketHandled(true);
    }
    
    public List<RegistryKey<World>> getWorlds(){
		return worlds;
	}
    
}
