package net.tardis.mod.commands.subcommands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.commands.argument.ConsoleRoomArgument;
import net.tardis.mod.commands.argument.ConsoleUnitArgument;
import net.tardis.mod.commands.argument.ExteriorArgument;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.misc.Console;
import net.tardis.mod.sounds.TSounds;

public class CreateCommand extends TCommand{
    
    private static int createTardis(CommandSource source, ServerWorld sWorld, AbstractExterior ext, ConsoleRoom room, Console console) {
        BlockPos sourcePos = new BlockPos(source.getPos().x, source.getPos().y, source.getPos().z).offset(Direction.NORTH);
        BlockPos placePos = sourcePos.up();
        if (room != null) {//Null check as ConsoleRooms are data driven so they can be null
            ServerWorld tardisInteriorWorld = TardisHelper.setupTardisDim(sWorld.getServer(), console.getState(), room);
            TardisHelper.getConsoleInWorld(tardisInteriorWorld).ifPresent(tile -> {
                tile.getUnlockManager().addConsole(console);
                tile.getUnlockManager().addConsoleRoom(room);
                tile.getUnlockManager().addExterior(ext);
                tile.setExteriorType(ext);
                tile.setCurrentLocation(sWorld.getDimensionKey(), sourcePos);
                tile.setDestination(sWorld.getDimensionKey(), sourcePos);
                tile.getExteriorType().place(tile, sWorld.getDimensionKey(), placePos);
                tile.getExteriorType().getExteriorTile(tile).setInteriorDimensionKey(tardisInteriorWorld);
                tile.getExteriorType().getExteriorTile(tile).setDoorState(EnumDoorState.ONE);
                if(source.getEntity() instanceof PlayerEntity) {
                	PlayerEntity player = (PlayerEntity)source.getEntity();
                	tile.getEmotionHandler().addLoyalty(player, 100);
                }
            });
            sWorld.playSound(null, sourcePos, TSounds.DOOR_UNLOCK.get(), SoundCategory.BLOCKS, 0.75F, 1F);
            TextComponent tardisDimName = TextHelper.getTardisDimObject(tardisInteriorWorld);
            TextComponent exteriorName = TextHelper.createTextComponentWithTip(ext.getDisplayName().getString(), ext.getRegistryName().toString());
            TextComponent roomName = TextHelper.createTextComponentWithTip(room.getDisplayName().getString(), room.getRegistryName().toString());
            TextComponent consoleUnitName = TextHelper.createTextComponentWithTip(console.getDisplayName().getString(), console.getRegistryName().toString());
            source.sendFeedback(new TranslationTextComponent("command.tardis.create.success", tardisDimName, exteriorName, roomName, consoleUnitName), true);
        }
        return Command.SINGLE_SUCCESS;
    }
    
    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher){
        return Commands.literal("create").requires(context -> context.hasPermissionLevel(2))
            .then(Commands.argument("exterior", ExteriorArgument.getExteriorArgument())
                .then(Commands.argument("interior", ConsoleRoomArgument.getConsoleRoomArgument())
                    .then(Commands.argument("console_unit", ConsoleUnitArgument.getConsoleUnitArgument())
                        .executes(context -> createTardis(context.getSource(), context.getSource().getWorld(), ExteriorArgument.getExterior(context, "exterior"), ConsoleRoomArgument.getConsoleRoom(context, "interior"), ConsoleUnitArgument.getConsoleUnit(context, "console_unit"))
                        )//End execute
                    )//End console unit argument
                )//End interior argument
            );//End exterior argument
    }
}
