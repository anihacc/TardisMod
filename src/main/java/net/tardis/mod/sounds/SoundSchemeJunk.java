package net.tardis.mod.sounds;

public class SoundSchemeJunk  extends SoundSchemeBase {
	
	public SoundSchemeJunk() {
		super(() -> TSounds.JUNK_TAKEOFF.get(), () -> TSounds.JUNK_LAND.get(), () -> TSounds.TARDIS_FLY_LOOP.get());
	}
	
	@Override
	public int getLandTime() {
		return 260;
	}

	@Override
	public int getTakeoffTime() {
		return 360;
	}

}
