package net.tardis.mod.particles.client;

import net.minecraft.client.particle.IAnimatedSprite;
import net.minecraft.client.particle.IParticleFactory;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.particles.BasicParticleType;

public class ArtronParticleFactory implements IParticleFactory<BasicParticleType> {
	
	private final IAnimatedSprite spriteSet;

    public ArtronParticleFactory(IAnimatedSprite p_i51045_1_) {
       this.spriteSet = p_i51045_1_;
    }

	@Override
	public Particle makeParticle(BasicParticleType typeIn, ClientWorld worldIn, double x, double y, double z,
	        double xSpeed, double ySpeed, double zSpeed) {
		ArtronParticle part = new ArtronParticle(worldIn, x, y, z, xSpeed, ySpeed, zSpeed, this.spriteSet);
		part.setColor(0.1F, 0.37F, 0.36F);
		return part;
	}

}
