package net.tardis.mod.entity.ai;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.ai.goal.MoveToBlockGoal;

public abstract class MoveToSideOfBlockGoal extends MoveToBlockGoal {

    public MoveToSideOfBlockGoal(CreatureEntity creature, double speedIn, int length) {
        super(creature, speedIn, length);
    }

    public MoveToSideOfBlockGoal(CreatureEntity creatureIn, double speed, int length, int p_i48796_5_) {
        super(creatureIn, speed, length, p_i48796_5_);
    }


    @Override
    protected boolean searchForDestination() {
        return super.searchForDestination();
    }
}
