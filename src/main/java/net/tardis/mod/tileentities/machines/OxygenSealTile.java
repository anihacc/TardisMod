package net.tardis.mod.tileentities.machines;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.api.space.IOxygenSealer;
import net.tardis.api.space.cap.OxygenSealerCapability;
import net.tardis.api.space.thread.OxygenThread;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.tileentities.TTiles;


public class OxygenSealTile extends TileEntity implements ITickableTileEntity{

	private LazyOptional<IOxygenSealer> oxygenSupplier;
	private OxygenSealerCapability capability;
	
	public OxygenSealTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
		this.capability = new OxygenSealerCapability(100);
		this.oxygenSupplier = LazyOptional.of(() -> this.capability);
	}
	
	public OxygenSealTile() {
		this(TTiles.OXYGEN_SEALER.get());
	}

	@Override
	public void tick() {
		this.capability.tick(this.world, this.pos);
	}

	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		return cap == Capabilities.OXYGEN_SEALER ? this.oxygenSupplier.cast() : super.getCapability(cap, side);
	}
	
}
