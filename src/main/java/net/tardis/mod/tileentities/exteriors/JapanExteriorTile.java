package net.tardis.mod.tileentities.exteriors;

import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.tileentities.TTiles;

public class JapanExteriorTile extends ExteriorTile{
	
    public static final AxisAlignedBB RENDER = new AxisAlignedBB(-1, -1, -1, 2, 2, 2);

	public JapanExteriorTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public JapanExteriorTile() {
		super(TTiles.EXTERIOR_JAPAN.get());
	}

	@Override
	public AxisAlignedBB getDoorAABB() {
		return this.getDefaultEntryBox();
	}
	
	@Override
    public AxisAlignedBB getRenderBoundingBox() {
        return RENDER.offset(getPos());
    }

}
