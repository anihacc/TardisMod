package net.tardis.mod.tileentities;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.network.Network;

public class PlaqueTile extends TileEntity{
	
	private String[] text = null;
	private String tardisName = "";

	public PlaqueTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public PlaqueTile() {
		this(TTiles.PLAQUE.get());
	}
	
	public String[] getText() {
		if(this.text == null || this.text.length <= 0 && this.world != null && world.isRemote) {
			this.text = new String[]{
					new TranslationTextComponent("text.tardis.plaque.line1").getString(),
					new TranslationTextComponent("text.tardis.plaque.line2").getString(),
					new TranslationTextComponent("text.tardis.plaque.line3").getString() + this.getTardisName()
			};
		}
		return text;
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		super.onDataPacket(net, pkt);
		this.tardisName = pkt.getNbtCompound().getString("tardis_name");
		this.text = null;
	}

	@Override
	public void handleUpdateTag(BlockState state, CompoundNBT tag) {
		this.read(state, tag);
		this.text = null;
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return Network.createTEUpdatePacket(this);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		CompoundNBT tag = this.serializeNBT();
		return this.write(tag);
	}

	@Override
	public void read(BlockState state, CompoundNBT nbt) {
		super.read(state, nbt);
		this.tardisName = nbt.getString("tardis_name");
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.putString("tardis_name", this.getTardisName());
		return super.write(compound);
	}
	
	public String getTardisName() {
		if (this.tardisName.isEmpty() || this.tardisName == null) {
			if (world != null && world instanceof ServerWorld) {
				ServerWorld serverWorld = world.getServer().getWorld(world.getDimensionKey());
				serverWorld.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
					this.tardisName = data.getTARDISName() != null ? data.getTARDISName() : "Empty Name";
				});//Find the Tardis Name. If not found, give it a value so on the next render tick the tardis name is cached. Users can refresh this check by replacing the block
			}
		}
		return this.tardisName;
	}
	
}
