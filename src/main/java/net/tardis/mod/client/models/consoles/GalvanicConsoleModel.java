package net.tardis.mod.client.models.consoles;

import java.util.function.Function;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.tardis.mod.client.models.IHaveMonitor;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.controls.DimensionControl;
import net.tardis.mod.controls.DoorControl;
import net.tardis.mod.controls.FacingControl;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.IncModControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.RandomiserControl;
import net.tardis.mod.controls.RefuelerControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;


public class GalvanicConsoleModel extends AbstractConsoleRenderTypedModel<GalvanicConsoleTile> implements IHaveMonitor{
    private final LightModelRenderer glow_shaft_1;
    private final LightModelRenderer glow_shaft_2;
    private final LightModelRenderer glow_shaft_3;
    private final ModelRenderer console;
    private final ModelRenderer bottom_plate;
    private final ModelRenderer fins;
    private final ModelRenderer fin_1;
    private final ModelRenderer fin_2;
    private final ModelRenderer fin_3;
    private final ModelRenderer chunks;
    private final ModelRenderer chunk_1;
    private final ModelRenderer chunk_2;
    private final ModelRenderer chunk_3;
    private final ModelRenderer stations;
    private final ModelRenderer station_1;
    private final ModelRenderer edge_1;
    private final ModelRenderer cooling_blades_1;
    private final ModelRenderer plasma_coil_1;
    private final LightModelRenderer glow_coil_1;
    private final ModelRenderer belly_1;
    private final ModelRenderer plane_1;
    private final ModelRenderer rib_1;
    private final ModelRenderer rib_tilt_1;
    private final ModelRenderer rib_deco_1;
    private final ModelRenderer base_fin_1;
    private final ModelRenderer clawfoot_1;
    private final ModelRenderer leg_1;
    private final ModelRenderer ball_1;
    private final LightModelRenderer glow_ball_1;
    private final ModelRenderer station_2;
    private final ModelRenderer edge_2;
    private final ModelRenderer cooling_blades_2;
    private final ModelRenderer plasma_coil_2;
    private final LightModelRenderer glow_coil_2;
    private final ModelRenderer belly_2;
    private final ModelRenderer plane_2;
    private final ModelRenderer rib_2;
    private final ModelRenderer rib_tilt_2;
    private final ModelRenderer rib_deco_2;
    private final ModelRenderer base_fin_2;
    private final ModelRenderer clawfoot_2;
    private final ModelRenderer leg_2;
    private final ModelRenderer ball_2;
    private final LightModelRenderer glow_ball_2;
    private final ModelRenderer station_3;
    private final ModelRenderer edge_3;
    private final ModelRenderer cooling_blades_3;
    private final ModelRenderer plasma_coil_3;
    private final LightModelRenderer glow_coil_3;
    private final ModelRenderer belly_3;
    private final ModelRenderer plane_3;
    private final ModelRenderer rib_3;
    private final ModelRenderer rib_tilt_3;
    private final ModelRenderer rib_deco_3;
    private final ModelRenderer base_fin_3;
    private final ModelRenderer clawfoot_3;
    private final ModelRenderer leg_3;
    private final ModelRenderer ball_3;
    private final LightModelRenderer glow_ball_3;
    private final ModelRenderer station_4;
    private final ModelRenderer edge_4;
    private final ModelRenderer cooling_blades_4;
    private final ModelRenderer plasma_coil_4;
    private final LightModelRenderer glow_coil_4;
    private final ModelRenderer belly_4;
    private final ModelRenderer plane_4;
    private final ModelRenderer rib_4;
    private final ModelRenderer rib_tilt_4;
    private final ModelRenderer rib_deco_4;
    private final ModelRenderer base_fin_4;
    private final ModelRenderer clawfoot_4;
    private final ModelRenderer leg_4;
    private final ModelRenderer ball_4;
    private final LightModelRenderer glow_ball_4;
    private final ModelRenderer station_5;
    private final ModelRenderer edge_5;
    private final ModelRenderer cooling_blades_5;
    private final ModelRenderer plasma_coil_5;
    private final LightModelRenderer glow_coil_5;
    private final ModelRenderer belly_5;
    private final ModelRenderer plane_5;
    private final ModelRenderer rib_5;
    private final ModelRenderer rib_tilt_5;
    private final ModelRenderer rib_deco_5;
    private final ModelRenderer base_fin_5;
    private final ModelRenderer clawfoot_5;
    private final ModelRenderer leg_5;
    private final ModelRenderer ball_5;
    private final LightModelRenderer glow_ball_5;
    private final ModelRenderer station_6;
    private final ModelRenderer edge_6;
    private final ModelRenderer cooling_blades_6;
    private final ModelRenderer plasma_coil_6;
    private final LightModelRenderer glow_coil_6;
    private final ModelRenderer belly_6;
    private final ModelRenderer plane_6;
    private final ModelRenderer rib_6;
    private final ModelRenderer rib_tilt_6;
    private final ModelRenderer rib_deco_6;
    private final ModelRenderer base_fin_6;
    private final ModelRenderer clawfoot_6;
    private final ModelRenderer leg_6;
    private final ModelRenderer ball_6;
    private final LightModelRenderer glow_ball_6;
    private final ModelRenderer rotor_rotate_y;
    private final LightModelRenderer glow_rotor_rotate_y;
    private final ModelRenderer controls;
    private final ModelRenderer controls_1;
    private final ModelRenderer fast_return;
    private final ModelRenderer return_button;
    private final LightModelRenderer glow_fast_return;
    private final ModelRenderer waypoints;
    private final ModelRenderer way_dial_1;
    private final ModelRenderer way_dial_2;
    private final ModelRenderer way_dial_3;
    private final ModelRenderer way_dial_4;
    private final ModelRenderer way_dial_5;
    private final ModelRenderer way_dial_6;
    private final ModelRenderer way_dial_7;
    private final ModelRenderer way_dial_8;
    private final ModelRenderer way_dial_9;
    private final LightModelRenderer dummy_aa_1;
    private final LightModelRenderer dummy_aa_4;
    private final LightModelRenderer dummy_aa_2;
    private final LightModelRenderer dummy_aa_5;
    private final LightModelRenderer dummy_aa_3;
    private final LightModelRenderer dummy_aa_6;
    private final ModelRenderer dummy_aa_7;
    private final ModelRenderer toggle_aa_7;
    private final ModelRenderer dummy_aa_8;
    private final ModelRenderer toggle_aa_2;
    private final ModelRenderer dummy_aa_9;
    private final ModelRenderer toggle_aa_3;
    private final ModelRenderer dummy_aa_10;
    private final ModelRenderer toggle_aa_4;
    private final ModelRenderer dummy_aa_11;
    private final ModelRenderer toggle_aa_5;
    private final ModelRenderer decoration_1;
    private final ModelRenderer controls_2;
    private final ModelRenderer stablizers;
    private final ModelRenderer stablizer_button_rotate_x;
    private final ModelRenderer telpathic_circuits;
    private final ModelRenderer tp_screen;
    private final LightModelRenderer glow_tp_screen;
    private final ModelRenderer tp_frame;
    private final ModelRenderer eye;
    private final LightModelRenderer glow_eye;
    private final ModelRenderer door;
    private final ModelRenderer keyhole_base;
    private final ModelRenderer key_rotate_y;
    private final ModelRenderer dummy_b_1;
    private final ModelRenderer toggle_b_1_rotate_x;
    private final ModelRenderer togglebottom_b1;
    private final ModelRenderer toggletop_b1;
    private final ModelRenderer dummy_b_2;
    private final ModelRenderer toggle_b2_rotate_x;
    private final ModelRenderer togglebottom_b2;
    private final ModelRenderer toggletop_b2;
    private final ModelRenderer dummy_b_3;
    private final ModelRenderer toggle_b3_rotate_x;
    private final ModelRenderer togglebottom_b3;
    private final ModelRenderer toggletop_b3;
    private final ModelRenderer dummy_b_4;
    private final ModelRenderer toggle_b4_rotate_x;
    private final ModelRenderer togglebottom_b4;
    private final ModelRenderer toggletop_b4;
    private final ModelRenderer dummy_b_5;
    private final ModelRenderer dummy_b_6;
    private final ModelRenderer dummy_b_7;
    private final ModelRenderer decoration_2;
    private final ModelRenderer controls_3;
    private final ModelRenderer refueler;
    private final ModelRenderer dial_base;
    private final LightModelRenderer round;
    private final ModelRenderer curve;
    private final ModelRenderer pointer_rotate_y;
    private final ModelRenderer sonic_port;
    private final ModelRenderer sonic_base;
    private final ModelRenderer randomizer;
    private final ModelRenderer rando_plate;
    private final LightModelRenderer glow_randomizer;
    private final ModelRenderer diamond;
    private final ModelRenderer dummy_c_1;
    private final ModelRenderer toggle_c_1;
    private final ModelRenderer dummy_c_2;
    private final ModelRenderer toggle_c_2;
    private final ModelRenderer dummy_c_3;
    private final ModelRenderer toggle_c_3;
    private final ModelRenderer dummy_c_4;
    private final ModelRenderer toggle_c_4;
    private final ModelRenderer dummy_c_5;
    private final ModelRenderer toggle_c_5;
    private final ModelRenderer dummy_c_6;
    private final ModelRenderer toggle_c_6;
    private final ModelRenderer dummy_c_7;
    private final ModelRenderer dummy_c_9;
    private final LightModelRenderer glow_c9;
    private final ModelRenderer dummy_c_8;
    private final LightModelRenderer glow_c8;
    private final ModelRenderer dummy_c_10;
    private final ModelRenderer controls_4;
    private final ModelRenderer throttle;
    private final ModelRenderer throttle_plate;
    private final ModelRenderer throttle_rotate_x;
    private final ModelRenderer dimentional_con;
    private final ModelRenderer dim_screen;
    private final LightModelRenderer glow_dim;
    private final ModelRenderer handbreak;
    private final ModelRenderer handbreak_plate;
    private final ModelRenderer handbreak_rotate_y;
    private final LightModelRenderer dummy_d_1;
    private final LightModelRenderer dummy_d_2;
    private final LightModelRenderer dummy_d_3;
    private final LightModelRenderer dummy_d_4;
    private final LightModelRenderer dummy_d_5;
    private final ModelRenderer dummy_d_6;
    private final ModelRenderer dummy_d_7;
    private final ModelRenderer dummy_d_8;
    private final ModelRenderer decoration_4;
    private final ModelRenderer controls_5;
    private final ModelRenderer landing_type;
    private final ModelRenderer landing_screen;
    private final LightModelRenderer glow_landing;
    private final ModelRenderer increments;
    private final ModelRenderer inc_slider;
    private final ModelRenderer position_rotate_x;
    private final ModelRenderer xyz;
    private final ModelRenderer dial_x;
    private final ModelRenderer dial_y;
    private final ModelRenderer dial_z;
    private final ModelRenderer direction;
    private final ModelRenderer direction_screen;
    private final LightModelRenderer glow_direction;
    private final ModelRenderer dummy_e_1;
    private final ModelRenderer dummy_e_2;
    private final ModelRenderer dummy_e_3;
    private final ModelRenderer decoration_5;
    private final ModelRenderer arrows;
    private final ModelRenderer arrows2;
    private final ModelRenderer controls_6;
    private final ModelRenderer monitor;
    private final ModelRenderer screen;
    private final LightModelRenderer glow_screen_base;
    private final LightModelRenderer circles;
    private final ModelRenderer frame;
    private final LightModelRenderer glow_camera;
    private final ModelRenderer coms;
    private final ModelRenderer speaker;
    private final ModelRenderer dummy_ff_5;
    private final ModelRenderer toggle_ff_5;
    private final ModelRenderer dummy_ff_1;
    private final ModelRenderer dummy_ff_2;
    private final ModelRenderer dummy_ff_3;
    private final ModelRenderer toggle_ff_3;
    private final ModelRenderer dummy_ff_4;
    private final ModelRenderer toggle_ff_4;
    private final ModelRenderer dummy_ff_6;
    private final ModelRenderer dummy_ff_7;
    private final ModelRenderer dummy_ff_8;
    private final ModelRenderer decoration_6;

    public GalvanicConsoleModel(Function<ResourceLocation, RenderType> function) {
        super(function);
        textureWidth = 512;
        textureHeight = 512;

        glow_shaft_1 = new LightModelRenderer(this);
        glow_shaft_1.setRotationPoint(0.0F, 24.0F, 0.0F);
        glow_shaft_1.setTextureOffset(404, 330).addBox(-8.0F, -149.0F, -12.0F, 16.0F, 148.0F, 24.0F, 0.0F, false);
        glow_shaft_1.setTextureOffset(384, 365).addBox(-8.0F, -221.0F, -22.0F, 16.0F, 72.0F, 44.0F, 0.0F, false);

        glow_shaft_2 = new LightModelRenderer(this);
        glow_shaft_2.setRotationPoint(0.0F, 24.0F, 0.0F);
        setRotationAngle(glow_shaft_2, 0.0F, -1.0472F, 0.0F);
        glow_shaft_2.setTextureOffset(404, 330).addBox(-8.0F, -149.0F, -12.0F, 16.0F, 148.0F, 24.0F, 0.0F, false);
        glow_shaft_2.setTextureOffset(384, 365).addBox(-8.0F, -221.0F, -22.0F, 16.0F, 72.0F, 44.0F, 0.0F, false);

        glow_shaft_3 = new LightModelRenderer(this);
        glow_shaft_3.setRotationPoint(0.0F, 24.0F, 0.0F);
        setRotationAngle(glow_shaft_3, 0.0F, -2.0944F, 0.0F);
        glow_shaft_3.setTextureOffset(404, 330).addBox(-8.0F, -149.0F, -12.0F, 16.0F, 148.0F, 24.0F, 0.0F, false);
        glow_shaft_3.setTextureOffset(384, 365).addBox(-8.0F, -221.0F, -22.0F, 16.0F, 72.0F, 44.0F, 0.0F, false);

        console = new ModelRenderer(this);
        console.setRotationPoint(0.0F, 24.0F, 0.0F);
        

        bottom_plate = new ModelRenderer(this);
        bottom_plate.setRotationPoint(0.0F, 0.0F, 0.0F);
        console.addChild(bottom_plate);
        bottom_plate.setTextureOffset(256, 98).addBox(-20.0F, -1.6F, -8.0F, 40.0F, 2.0F, 16.0F, 0.0F, false);
        bottom_plate.setTextureOffset(256, 98).addBox(-16.0F, -1.6F, -16.0F, 32.0F, 2.0F, 8.0F, 0.0F, false);
        bottom_plate.setTextureOffset(256, 98).addBox(-16.0F, -1.6F, 8.0F, 32.0F, 2.0F, 8.0F, 0.0F, false);

        fins = new ModelRenderer(this);
        fins.setRotationPoint(0.0F, 0.0F, 0.0F);
        console.addChild(fins);
        

        fin_1 = new ModelRenderer(this);
        fin_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        fins.addChild(fin_1);
        setRotationAngle(fin_1, 0.0F, -0.5236F, 0.0F);
        fin_1.setTextureOffset(153, 193).addBox(-2.4F, -150.0F, -16.0F, 5.0F, 86.0F, 32.0F, 0.0F, false);
        fin_1.setTextureOffset(168, 188).addBox(-6.4F, -221.8F, -24.0F, 13.0F, 82.0F, 48.0F, 0.0F, false);
        fin_1.setTextureOffset(184, 187).addBox(-2.4F, -218.0F, -28.0F, 5.0F, 74.0F, 56.0F, 0.0F, false);

        fin_2 = new ModelRenderer(this);
        fin_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        fins.addChild(fin_2);
        setRotationAngle(fin_2, 0.0F, -1.5708F, 0.0F);
        fin_2.setTextureOffset(127, 195).addBox(-2.4F, -150.0F, -16.0F, 5.0F, 86.0F, 32.0F, 0.0F, false);
        fin_2.setTextureOffset(171, 189).addBox(-6.4F, -222.0F, -24.0F, 13.0F, 82.0F, 48.0F, 0.0F, false);
        fin_2.setTextureOffset(157, 189).addBox(-2.4F, -218.0F, -28.0F, 5.0F, 74.0F, 56.0F, 0.0F, false);

        fin_3 = new ModelRenderer(this);
        fin_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        fins.addChild(fin_3);
        setRotationAngle(fin_3, 0.0F, -2.618F, 0.0F);
        fin_3.setTextureOffset(132, 195).addBox(-2.4F, -150.0F, -16.0F, 5.0F, 86.0F, 32.0F, 0.0F, false);
        fin_3.setTextureOffset(159, 191).addBox(-6.4F, -222.1F, -24.0F, 13.0F, 82.0F, 48.0F, 0.0F, false);
        fin_3.setTextureOffset(179, 188).addBox(-2.4F, -218.0F, -28.0F, 5.0F, 74.0F, 56.0F, 0.0F, false);

        chunks = new ModelRenderer(this);
        chunks.setRotationPoint(0.0F, -1.0F, 0.0F);
        console.addChild(chunks);
        

        chunk_1 = new ModelRenderer(this);
        chunk_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        chunks.addChild(chunk_1);
        chunk_1.setTextureOffset(113, 226).addBox(-13.0F, -64.0F, -24.0F, 25.0F, 16.0F, 48.0F, 0.0F, false);
        chunk_1.setTextureOffset(6, 290).addBox(-12.0F, -148.0F, -24.0F, 24.0F, 4.0F, 48.0F, 0.0F, false);

        chunk_2 = new ModelRenderer(this);
        chunk_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        chunks.addChild(chunk_2);
        setRotationAngle(chunk_2, 0.0F, -1.0472F, 0.0F);
        chunk_2.setTextureOffset(116, 232).addBox(-12.0F, -64.0F, -24.0F, 24.0F, 16.0F, 48.0F, 0.0F, false);
        chunk_2.setTextureOffset(6, 290).addBox(-12.0F, -148.0F, -24.0F, 24.0F, 4.0F, 48.0F, 0.0F, false);

        chunk_3 = new ModelRenderer(this);
        chunk_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        chunks.addChild(chunk_3);
        setRotationAngle(chunk_3, 0.0F, -2.0944F, 0.0F);
        chunk_3.setTextureOffset(111, 227).addBox(-12.0F, -64.0F, -24.0F, 24.0F, 16.0F, 48.0F, 0.0F, false);
        chunk_3.setTextureOffset(6, 290).addBox(-12.0F, -148.0F, -24.0F, 24.0F, 4.0F, 48.0F, 0.0F, false);

        stations = new ModelRenderer(this);
        stations.setRotationPoint(0.0F, 0.0F, 0.0F);
        console.addChild(stations);
        

        station_1 = new ModelRenderer(this);
        station_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        stations.addChild(station_1);
        

        edge_1 = new ModelRenderer(this);
        edge_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_1.addChild(edge_1);
        edge_1.setTextureOffset(51, 12).addBox(-34.0F, -60.0F, -61.0F, 68.0F, 4.0F, 6.0F, 0.0F, false);
        edge_1.setTextureOffset(177, 229).addBox(-10.0F, -79.0F, -21.0F, 20.0F, 16.0F, 10.0F, 0.0F, false);
        edge_1.setTextureOffset(157, 251).addBox(-10.0F, -85.0F, -17.0F, 20.0F, 6.0F, 6.0F, 0.0F, false);
        edge_1.setTextureOffset(144, 226).addBox(-10.0F, -144.0F, -17.0F, 20.0F, 6.0F, 6.0F, 0.0F, false);
        edge_1.setTextureOffset(160, 235).addBox(-10.0F, -89.0F, -17.0F, 20.0F, 2.0F, 6.0F, 0.0F, false);
        edge_1.setTextureOffset(166, 292).addBox(-10.0F, -136.0F, -17.0F, 20.0F, 2.0F, 6.0F, 0.0F, false);
        edge_1.setTextureOffset(270, 99).addBox(-10.0F, -87.0F, -15.0F, 20.0F, 4.0F, 4.0F, 0.0F, false);
        edge_1.setTextureOffset(276, 94).addBox(-10.0F, -140.0F, -15.0F, 20.0F, 4.0F, 4.0F, 0.0F, false);

        cooling_blades_1 = new ModelRenderer(this);
        cooling_blades_1.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_1.addChild(cooling_blades_1);
        cooling_blades_1.setTextureOffset(287, 100).addBox(-11.0F, -25.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_1.setTextureOffset(287, 100).addBox(-11.0F, -37.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_1.setTextureOffset(287, 100).addBox(-12.0F, -26.0F, -24.0F, 24.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_1.setTextureOffset(287, 100).addBox(-12.0F, -38.0F, -24.0F, 24.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_1.setTextureOffset(287, 100).addBox(-11.0F, -27.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_1.setTextureOffset(287, 100).addBox(-11.0F, -39.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_1.setTextureOffset(287, 100).addBox(-8.0F, -27.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_1.setTextureOffset(287, 100).addBox(-8.0F, -39.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_1.setTextureOffset(287, 100).addBox(-8.0F, -25.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_1.setTextureOffset(287, 100).addBox(-8.0F, -37.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);

        plasma_coil_1 = new ModelRenderer(this);
        plasma_coil_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        edge_1.addChild(plasma_coil_1);
        plasma_coil_1.setTextureOffset(212, 310).addBox(-10.0F, -81.0F, -19.0F, 20.0F, 2.0F, 2.0F, 0.0F, false);

        glow_coil_1 = new LightModelRenderer(this);
        glow_coil_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        plasma_coil_1.addChild(glow_coil_1);
        glow_coil_1.setTextureOffset(437, 357).addBox(-5.1F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        glow_coil_1.setTextureOffset(437, 357).addBox(-1.1F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        glow_coil_1.setTextureOffset(437, 357).addBox(2.9F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        belly_1 = new ModelRenderer(this);
        belly_1.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_1.addChild(belly_1);
        belly_1.setTextureOffset(270, 89).addBox(-8.0F, -15.0F, -24.0F, 16.0F, 4.0F, 8.0F, 0.0F, false);
        belly_1.setTextureOffset(270, 89).addBox(-8.0F, -1.0F, -24.0F, 16.0F, 3.0F, 8.0F, 0.0F, false);
        belly_1.setTextureOffset(313, 55).addBox(-4.0F, -1.0F, -28.0F, 8.0F, 3.0F, 4.0F, 0.0F, false);
        belly_1.setTextureOffset(270, 89).addBox(-8.0F, -11.0F, -21.0F, 3.0F, 10.0F, 5.0F, 0.0F, false);
        belly_1.setTextureOffset(270, 89).addBox(5.0F, -11.0F, -21.0F, 3.0F, 10.0F, 5.0F, 0.0F, false);
        belly_1.setTextureOffset(270, 89).addBox(2.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_1.setTextureOffset(270, 89).addBox(-4.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_1.setTextureOffset(270, 89).addBox(-1.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_1.setTextureOffset(270, 89).addBox(-5.0F, -12.0F, -21.0F, 10.0F, 2.0F, 5.0F, 0.0F, false);
        belly_1.setTextureOffset(270, 89).addBox(-5.0F, -2.0F, -21.0F, 10.0F, 2.0F, 5.0F, 0.0F, false);
        belly_1.setTextureOffset(270, 89).addBox(-11.0F, -15.0F, -24.0F, 3.0F, 17.0F, 8.0F, 0.0F, false);
        belly_1.setTextureOffset(270, 89).addBox(8.0F, -15.0F, -24.0F, 3.0F, 17.0F, 8.0F, 0.0F, false);
        belly_1.setTextureOffset(270, 89).addBox(-8.0F, -17.0F, -19.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);

        plane_1 = new ModelRenderer(this);
        plane_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_1.addChild(plane_1);
        setRotationAngle(plane_1, 0.5236F, 0.0F, 0.0F);
        plane_1.setTextureOffset(31, 132).addBox(-31.2F, -80.0F, -20.0F, 62.0F, 4.0F, 6.0F, 0.0F, false);
        plane_1.setTextureOffset(37, 124).addBox(-28.2F, -80.0F, -14.0F, 56.0F, 4.0F, 6.0F, 0.0F, false);
        plane_1.setTextureOffset(41, 108).addBox(-26.0F, -80.0F, -8.0F, 51.0F, 4.0F, 6.0F, 0.0F, false);
        plane_1.setTextureOffset(52, 96).addBox(-22.0F, -80.0F, -2.0F, 44.0F, 4.0F, 6.0F, 0.0F, false);
        plane_1.setTextureOffset(253, 82).addBox(-17.0F, -77.0F, -17.0F, 34.0F, 8.0F, 30.0F, 0.0F, false);
        plane_1.setTextureOffset(267, 84).addBox(-11.0F, -69.0F, -11.0F, 22.0F, 4.0F, 24.0F, 0.0F, false);
        plane_1.setTextureOffset(67, 85).addBox(-19.0F, -80.0F, 4.0F, 38.0F, 4.0F, 6.0F, 0.0F, false);
        plane_1.setTextureOffset(70, 76).addBox(-16.0F, -80.0F, 10.0F, 32.0F, 4.0F, 6.0F, 0.0F, false);
        plane_1.setTextureOffset(67, 64).addBox(-14.0F, -80.0F, 16.0F, 27.0F, 4.0F, 6.0F, 0.0F, false);

        rib_1 = new ModelRenderer(this);
        rib_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_1.addChild(rib_1);
        setRotationAngle(rib_1, 0.0F, -0.5236F, 0.0F);
        

        rib_tilt_1 = new ModelRenderer(this);
        rib_tilt_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_1.addChild(rib_tilt_1);
        setRotationAngle(rib_tilt_1, 0.4363F, 0.0F, 0.0F);
        rib_tilt_1.setTextureOffset(180, 260).addBox(-2.0F, -84.0F, -40.0F, 4.0F, 8.0F, 52.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(56, 277).addBox(-2.0F, -76.0F, -16.0F, 4.0F, 6.0F, 29.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(75, 280).addBox(-1.0F, -76.0F, -20.0F, 2.0F, 26.0F, 4.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(78, 275).addBox(-1.0F, -76.0F, -12.0F, 2.0F, 26.0F, 10.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(56, 277).addBox(-1.0F, -53.0F, -16.0F, 2.0F, 3.0F, 4.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(56, 277).addBox(-1.0F, -59.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(65, 292).addBox(-1.0F, -65.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(65, 292).addBox(-1.0F, -71.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(105, 297).addBox(-2.0F, -70.0F, 0.0F, 4.0F, 6.0F, 14.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(75, 321).addBox(-1.0F, -23.8F, -45.0F, 2.0F, 2.0F, 7.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(72, 306).addBox(-1.0F, -21.8F, -41.0F, 2.0F, 2.0F, 17.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(63, 312).addBox(-1.0F, -25.8F, -49.0F, 2.0F, 2.0F, 11.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(61, 321).addBox(-1.0F, -25.8F, -34.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(61, 320).addBox(-1.0F, -19.8F, -34.0F, 2.0F, 3.0F, 9.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(91, 324).addBox(-1.0F, -25.8F, -26.3F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(73, 300).addBox(-1.0F, -27.8F, -49.0F, 2.0F, 2.0F, 28.0F, 0.0F, false);

        rib_deco_1 = new ModelRenderer(this);
        rib_deco_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_tilt_1.addChild(rib_deco_1);
        rib_deco_1.setTextureOffset(76, 308).addBox(-4.0F, -85.0F, -41.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_1.setTextureOffset(80, 304).addBox(-3.0F, -79.0F, -42.0F, 6.0F, 4.0F, 4.0F, 0.0F, false);
        rib_deco_1.setTextureOffset(91, 291).addBox(-4.0F, -85.0F, 7.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_1.setTextureOffset(180, 260).addBox(-3.0F, -84.0F, 8.0F, 6.0F, 7.0F, 8.0F, 0.0F, false);
        rib_deco_1.setTextureOffset(306, 89).addBox(-4.0F, -85.3F, -9.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_1.setTextureOffset(317, 89).addBox(-4.0F, -84.9F, -13.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_1.setTextureOffset(310, 100).addBox(-4.0F, -84.2F, -17.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_1.setTextureOffset(90, 301).addBox(-4.0F, -85.0F, 4.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);

        base_fin_1 = new ModelRenderer(this);
        base_fin_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_1.addChild(base_fin_1);
        setRotationAngle(base_fin_1, 0.0F, -0.5236F, 0.0F);
        base_fin_1.setTextureOffset(48, 225).addBox(-1.4F, -64.0F, -31.0F, 3.0F, 63.0F, 23.0F, 0.0F, false);
        base_fin_1.setTextureOffset(44, 261).addBox(-0.4F, -40.0F, -39.0F, 1.0F, 28.0F, 2.0F, 0.0F, false);
        base_fin_1.setTextureOffset(44, 261).addBox(-0.4F, -41.0F, -33.0F, 1.0F, 28.0F, 2.0F, 0.0F, false);
        base_fin_1.setTextureOffset(44, 261).addBox(-0.4F, -16.0F, -37.0F, 1.0F, 4.0F, 6.0F, 0.0F, false);
        base_fin_1.setTextureOffset(44, 261).addBox(-0.4F, -24.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        base_fin_1.setTextureOffset(44, 261).addBox(-0.4F, -32.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        base_fin_1.setTextureOffset(44, 261).addBox(-0.4F, -40.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);

        clawfoot_1 = new ModelRenderer(this);
        clawfoot_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_1.addChild(clawfoot_1);
        setRotationAngle(clawfoot_1, 0.0F, -0.5236F, 0.0F);
        

        leg_1 = new ModelRenderer(this);
        leg_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_1.addChild(leg_1);
        leg_1.setTextureOffset(46, 309).addBox(-2.4F, -4.0F, -52.0F, 5.0F, 4.0F, 36.0F, 0.0F, false);
        leg_1.setTextureOffset(61, 326).addBox(-2.2F, -3.9F, -55.0F, 4.0F, 3.0F, 3.0F, 0.0F, false);

        ball_1 = new ModelRenderer(this);
        ball_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_1.addChild(ball_1);
        ball_1.setTextureOffset(267, 95).addBox(-1.4F, -8.0F, -63.0F, 3.0F, 8.0F, 8.0F, 0.0F, false);
        ball_1.setTextureOffset(267, 95).addBox(-2.4F, -7.5F, -62.5F, 5.0F, 7.0F, 7.0F, 0.0F, false);
        ball_1.setTextureOffset(267, 95).addBox(-3.4F, -7.0F, -62.1F, 7.0F, 6.0F, 6.0F, 0.0F, false);
        ball_1.setTextureOffset(267, 95).addBox(-4.4F, -6.5F, -61.7F, 9.0F, 5.0F, 5.0F, 0.0F, false);

        glow_ball_1 = new LightModelRenderer(this);
        glow_ball_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        ball_1.addChild(glow_ball_1);
        glow_ball_1.setTextureOffset(228, 408).addBox(-5.4F, -5.5F, -60.7F, 11.0F, 3.0F, 3.0F, 0.0F, false);

        
        station_2 = new ModelRenderer(this);
        station_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        stations.addChild(station_2);
        setRotationAngle(station_2, 0.0F, -1.0472F, 0.0F);
        

        edge_2 = new ModelRenderer(this);
        edge_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_2.addChild(edge_2);
        edge_2.setTextureOffset(51, 12).addBox(-34.0F, -60.0F, -61.0F, 68.0F, 4.0F, 6.0F, 0.0F, false);
        edge_2.setTextureOffset(177, 229).addBox(-10.0F, -79.0F, -21.0F, 20.0F, 16.0F, 10.0F, 0.0F, false);
        edge_2.setTextureOffset(157, 251).addBox(-10.0F, -85.0F, -17.0F, 20.0F, 6.0F, 6.0F, 0.0F, false);
        edge_2.setTextureOffset(144, 226).addBox(-10.0F, -144.0F, -17.0F, 20.0F, 6.0F, 6.0F, 0.0F, false);
        edge_2.setTextureOffset(160, 235).addBox(-10.0F, -89.0F, -17.0F, 20.0F, 2.0F, 6.0F, 0.0F, false);
        edge_2.setTextureOffset(166, 292).addBox(-10.0F, -136.0F, -17.0F, 20.0F, 2.0F, 6.0F, 0.0F, false);
        edge_2.setTextureOffset(270, 99).addBox(-10.0F, -87.0F, -15.0F, 20.0F, 4.0F, 4.0F, 0.0F, false);
        edge_2.setTextureOffset(276, 94).addBox(-10.0F, -140.0F, -15.0F, 20.0F, 4.0F, 4.0F, 0.0F, false);

        cooling_blades_2 = new ModelRenderer(this);
        cooling_blades_2.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_2.addChild(cooling_blades_2);
        cooling_blades_2.setTextureOffset(287, 100).addBox(-11.0F, -25.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_2.setTextureOffset(287, 100).addBox(-11.0F, -37.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_2.setTextureOffset(287, 100).addBox(-12.0F, -26.0F, -24.0F, 24.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_2.setTextureOffset(287, 100).addBox(-12.0F, -38.0F, -24.0F, 24.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_2.setTextureOffset(287, 100).addBox(-11.0F, -27.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_2.setTextureOffset(287, 100).addBox(-11.0F, -39.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_2.setTextureOffset(287, 100).addBox(-8.0F, -27.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_2.setTextureOffset(287, 100).addBox(-8.0F, -39.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_2.setTextureOffset(287, 100).addBox(-8.0F, -25.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_2.setTextureOffset(287, 100).addBox(-8.0F, -37.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);

        plasma_coil_2 = new ModelRenderer(this);
        plasma_coil_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        edge_2.addChild(plasma_coil_2);
        plasma_coil_2.setTextureOffset(212, 310).addBox(-10.0F, -81.0F, -19.0F, 20.0F, 2.0F, 2.0F, 0.0F, false);

        glow_coil_2 = new LightModelRenderer(this);
        glow_coil_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        plasma_coil_2.addChild(glow_coil_2);
        glow_coil_2.setTextureOffset(437, 357).addBox(-5.1F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        glow_coil_2.setTextureOffset(437, 357).addBox(-1.1F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        glow_coil_2.setTextureOffset(437, 357).addBox(2.9F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        belly_2 = new ModelRenderer(this);
        belly_2.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_2.addChild(belly_2);
        belly_2.setTextureOffset(270, 89).addBox(-8.0F, -15.0F, -24.0F, 16.0F, 4.0F, 8.0F, 0.0F, false);
        belly_2.setTextureOffset(270, 89).addBox(-8.0F, -1.0F, -24.0F, 16.0F, 3.0F, 8.0F, 0.0F, false);
        belly_2.setTextureOffset(313, 55).addBox(-4.0F, -1.0F, -28.0F, 8.0F, 3.0F, 4.0F, 0.0F, false);
        belly_2.setTextureOffset(270, 89).addBox(-8.0F, -11.0F, -21.0F, 3.0F, 10.0F, 5.0F, 0.0F, false);
        belly_2.setTextureOffset(270, 89).addBox(5.0F, -11.0F, -21.0F, 3.0F, 10.0F, 5.0F, 0.0F, false);
        belly_2.setTextureOffset(270, 89).addBox(2.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_2.setTextureOffset(270, 89).addBox(-4.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_2.setTextureOffset(270, 89).addBox(-1.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_2.setTextureOffset(270, 89).addBox(-5.0F, -12.0F, -21.0F, 10.0F, 2.0F, 5.0F, 0.0F, false);
        belly_2.setTextureOffset(270, 89).addBox(-5.0F, -2.0F, -21.0F, 10.0F, 2.0F, 5.0F, 0.0F, false);
        belly_2.setTextureOffset(270, 89).addBox(-11.0F, -15.0F, -24.0F, 3.0F, 17.0F, 8.0F, 0.0F, false);
        belly_2.setTextureOffset(270, 89).addBox(8.0F, -15.0F, -24.0F, 3.0F, 17.0F, 8.0F, 0.0F, false);
        belly_2.setTextureOffset(270, 89).addBox(-8.0F, -17.0F, -19.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);

        plane_2 = new ModelRenderer(this);
        plane_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_2.addChild(plane_2);
        setRotationAngle(plane_2, 0.5236F, 0.0F, 0.0F);
        plane_2.setTextureOffset(31, 132).addBox(-31.2F, -80.0F, -20.0F, 62.0F, 4.0F, 6.0F, 0.0F, false);
        plane_2.setTextureOffset(37, 124).addBox(-28.2F, -80.0F, -14.0F, 56.0F, 4.0F, 6.0F, 0.0F, false);
        plane_2.setTextureOffset(41, 108).addBox(-26.0F, -80.0F, -8.0F, 51.0F, 4.0F, 6.0F, 0.0F, false);
        plane_2.setTextureOffset(52, 96).addBox(-22.0F, -80.0F, -2.0F, 44.0F, 4.0F, 6.0F, 0.0F, false);
        plane_2.setTextureOffset(253, 82).addBox(-17.0F, -77.0F, -17.0F, 34.0F, 8.0F, 30.0F, 0.0F, false);
        plane_2.setTextureOffset(267, 84).addBox(-11.0F, -69.0F, -11.0F, 22.0F, 4.0F, 24.0F, 0.0F, false);
        plane_2.setTextureOffset(67, 85).addBox(-19.0F, -80.0F, 4.0F, 38.0F, 4.0F, 6.0F, 0.0F, false);
        plane_2.setTextureOffset(70, 76).addBox(-16.0F, -80.0F, 10.0F, 32.0F, 4.0F, 6.0F, 0.0F, false);
        plane_2.setTextureOffset(67, 64).addBox(-14.0F, -80.0F, 16.0F, 27.0F, 4.0F, 6.0F, 0.0F, false);

        rib_2 = new ModelRenderer(this);
        rib_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_2.addChild(rib_2);
        setRotationAngle(rib_2, 0.0F, -0.5236F, 0.0F);
        

        rib_tilt_2 = new ModelRenderer(this);
        rib_tilt_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_2.addChild(rib_tilt_2);
        setRotationAngle(rib_tilt_2, 0.4363F, 0.0F, 0.0F);
        rib_tilt_2.setTextureOffset(180, 260).addBox(-2.0F, -84.0F, -40.0F, 4.0F, 8.0F, 52.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(56, 277).addBox(-2.0F, -76.0F, -16.0F, 4.0F, 6.0F, 29.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(75, 280).addBox(-1.0F, -76.0F, -20.0F, 2.0F, 26.0F, 4.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(78, 275).addBox(-1.0F, -76.0F, -12.0F, 2.0F, 26.0F, 10.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(56, 277).addBox(-1.0F, -53.0F, -16.0F, 2.0F, 3.0F, 4.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(56, 277).addBox(-1.0F, -59.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(65, 292).addBox(-1.0F, -65.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(65, 292).addBox(-1.0F, -71.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(112, 293).addBox(-2.0F, -70.0F, 0.0F, 4.0F, 6.0F, 14.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(75, 321).addBox(-1.0F, -23.8F, -45.0F, 2.0F, 2.0F, 7.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(72, 306).addBox(-1.0F, -21.8F, -41.0F, 2.0F, 2.0F, 17.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(63, 312).addBox(-1.0F, -25.8F, -49.0F, 2.0F, 2.0F, 11.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(61, 321).addBox(-1.0F, -25.8F, -34.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(61, 320).addBox(-1.0F, -19.8F, -34.0F, 2.0F, 3.0F, 9.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(91, 324).addBox(-1.0F, -25.8F, -26.3F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(73, 300).addBox(-1.0F, -27.8F, -49.0F, 2.0F, 2.0F, 28.0F, 0.0F, false);

        rib_deco_2 = new ModelRenderer(this);
        rib_deco_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_tilt_2.addChild(rib_deco_2);
        rib_deco_2.setTextureOffset(76, 308).addBox(-4.0F, -85.0F, -41.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_2.setTextureOffset(80, 304).addBox(-3.0F, -79.0F, -42.0F, 6.0F, 4.0F, 4.0F, 0.0F, false);
        rib_deco_2.setTextureOffset(91, 291).addBox(-4.0F, -85.0F, 7.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_2.setTextureOffset(180, 260).addBox(-3.0F, -84.0F, 8.0F, 6.0F, 7.0F, 8.0F, 0.0F, false);
        rib_deco_2.setTextureOffset(306, 89).addBox(-4.0F, -85.3F, -9.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_2.setTextureOffset(317, 89).addBox(-4.0F, -84.9F, -13.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_2.setTextureOffset(310, 100).addBox(-4.0F, -84.2F, -17.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_2.setTextureOffset(90, 301).addBox(-4.0F, -85.0F, 4.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);

        base_fin_2 = new ModelRenderer(this);
        base_fin_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_2.addChild(base_fin_2);
        setRotationAngle(base_fin_2, 0.0F, -0.5236F, 0.0F);
        base_fin_2.setTextureOffset(48, 225).addBox(-1.4F, -64.0F, -31.0F, 3.0F, 63.0F, 23.0F, 0.0F, false);
        base_fin_2.setTextureOffset(44, 261).addBox(-0.4F, -40.0F, -39.0F, 1.0F, 28.0F, 2.0F, 0.0F, false);
        base_fin_2.setTextureOffset(44, 261).addBox(-0.4F, -41.0F, -33.0F, 1.0F, 28.0F, 2.0F, 0.0F, false);
        base_fin_2.setTextureOffset(44, 261).addBox(-0.4F, -16.0F, -37.0F, 1.0F, 4.0F, 6.0F, 0.0F, false);
        base_fin_2.setTextureOffset(44, 261).addBox(-0.4F, -24.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        base_fin_2.setTextureOffset(44, 261).addBox(-0.4F, -32.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        base_fin_2.setTextureOffset(44, 261).addBox(-0.4F, -40.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);

        clawfoot_2 = new ModelRenderer(this);
        clawfoot_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_2.addChild(clawfoot_2);
        setRotationAngle(clawfoot_2, 0.0F, -0.5236F, 0.0F);
        

        leg_2 = new ModelRenderer(this);
        leg_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_2.addChild(leg_2);
        leg_2.setTextureOffset(46, 309).addBox(-2.4F, -4.0F, -52.0F, 5.0F, 4.0F, 36.0F, 0.0F, false);
        leg_2.setTextureOffset(61, 326).addBox(-2.2F, -3.9F, -55.0F, 4.0F, 3.0F, 3.0F, 0.0F, false);

        ball_2 = new ModelRenderer(this);
        ball_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_2.addChild(ball_2);
        ball_2.setTextureOffset(267, 95).addBox(-1.4F, -8.0F, -63.0F, 3.0F, 8.0F, 8.0F, 0.0F, false);
        ball_2.setTextureOffset(267, 95).addBox(-2.4F, -7.5F, -62.5F, 5.0F, 7.0F, 7.0F, 0.0F, false);
        ball_2.setTextureOffset(267, 95).addBox(-3.4F, -7.0F, -62.1F, 7.0F, 6.0F, 6.0F, 0.0F, false);
        ball_2.setTextureOffset(267, 95).addBox(-4.4F, -6.5F, -61.7F, 9.0F, 5.0F, 5.0F, 0.0F, false);

        glow_ball_2 = new LightModelRenderer(this);
        glow_ball_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        ball_2.addChild(glow_ball_2);
        glow_ball_2.setTextureOffset(228, 408).addBox(-5.4F, -5.5F, -60.7F, 11.0F, 3.0F, 3.0F, 0.0F, false);

        station_3 = new ModelRenderer(this);
        station_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        stations.addChild(station_3);
        setRotationAngle(station_3, 0.0F, -2.0944F, 0.0F);
        

        edge_3 = new ModelRenderer(this);
        edge_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_3.addChild(edge_3);
        edge_3.setTextureOffset(51, 12).addBox(-34.0F, -60.0F, -61.0F, 68.0F, 4.0F, 6.0F, 0.0F, false);
        edge_3.setTextureOffset(177, 229).addBox(-10.0F, -79.0F, -21.0F, 20.0F, 16.0F, 10.0F, 0.0F, false);
        edge_3.setTextureOffset(157, 251).addBox(-10.0F, -85.0F, -17.0F, 20.0F, 6.0F, 6.0F, 0.0F, false);
        edge_3.setTextureOffset(144, 226).addBox(-10.0F, -144.0F, -17.0F, 20.0F, 6.0F, 6.0F, 0.0F, false);
        edge_3.setTextureOffset(160, 235).addBox(-10.0F, -89.0F, -17.0F, 20.0F, 2.0F, 6.0F, 0.0F, false);
        edge_3.setTextureOffset(166, 292).addBox(-10.0F, -136.0F, -17.0F, 20.0F, 2.0F, 6.0F, 0.0F, false);
        edge_3.setTextureOffset(270, 99).addBox(-10.0F, -87.0F, -15.0F, 20.0F, 4.0F, 4.0F, 0.0F, false);
        edge_3.setTextureOffset(276, 94).addBox(-10.0F, -140.0F, -15.0F, 20.0F, 4.0F, 4.0F, 0.0F, false);

        cooling_blades_3 = new ModelRenderer(this);
        cooling_blades_3.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_3.addChild(cooling_blades_3);
        cooling_blades_3.setTextureOffset(287, 100).addBox(-11.0F, -25.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_3.setTextureOffset(287, 100).addBox(-11.0F, -37.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_3.setTextureOffset(287, 100).addBox(-12.0F, -26.0F, -24.0F, 24.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_3.setTextureOffset(287, 100).addBox(-12.0F, -38.0F, -24.0F, 24.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_3.setTextureOffset(287, 100).addBox(-11.0F, -27.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_3.setTextureOffset(287, 100).addBox(-11.0F, -39.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_3.setTextureOffset(287, 100).addBox(-8.0F, -27.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_3.setTextureOffset(287, 100).addBox(-8.0F, -39.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_3.setTextureOffset(287, 100).addBox(-8.0F, -25.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_3.setTextureOffset(287, 100).addBox(-8.0F, -37.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);

        plasma_coil_3 = new ModelRenderer(this);
        plasma_coil_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        edge_3.addChild(plasma_coil_3);
        plasma_coil_3.setTextureOffset(212, 310).addBox(-10.0F, -81.0F, -19.0F, 20.0F, 2.0F, 2.0F, 0.0F, false);

        glow_coil_3 = new LightModelRenderer(this);
        glow_coil_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        plasma_coil_3.addChild(glow_coil_3);
        glow_coil_3.setTextureOffset(437, 357).addBox(-5.1F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        glow_coil_3.setTextureOffset(437, 357).addBox(-1.1F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        glow_coil_3.setTextureOffset(437, 357).addBox(2.9F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        belly_3 = new ModelRenderer(this);
        belly_3.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_3.addChild(belly_3);
        belly_3.setTextureOffset(270, 89).addBox(-8.0F, -15.0F, -24.0F, 16.0F, 4.0F, 8.0F, 0.0F, false);
        belly_3.setTextureOffset(270, 89).addBox(-8.0F, -1.0F, -24.0F, 16.0F, 3.0F, 8.0F, 0.0F, false);
        belly_3.setTextureOffset(313, 55).addBox(-4.0F, -1.0F, -28.0F, 8.0F, 3.0F, 4.0F, 0.0F, false);
        belly_3.setTextureOffset(270, 89).addBox(-8.0F, -11.0F, -21.0F, 3.0F, 10.0F, 5.0F, 0.0F, false);
        belly_3.setTextureOffset(270, 89).addBox(5.0F, -11.0F, -21.0F, 3.0F, 10.0F, 5.0F, 0.0F, false);
        belly_3.setTextureOffset(270, 89).addBox(2.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_3.setTextureOffset(270, 89).addBox(-4.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_3.setTextureOffset(270, 89).addBox(-1.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_3.setTextureOffset(270, 89).addBox(-5.0F, -12.0F, -21.0F, 10.0F, 2.0F, 5.0F, 0.0F, false);
        belly_3.setTextureOffset(270, 89).addBox(-5.0F, -2.0F, -21.0F, 10.0F, 2.0F, 5.0F, 0.0F, false);
        belly_3.setTextureOffset(270, 89).addBox(-11.0F, -15.0F, -24.0F, 3.0F, 17.0F, 8.0F, 0.0F, false);
        belly_3.setTextureOffset(270, 89).addBox(8.0F, -15.0F, -24.0F, 3.0F, 17.0F, 8.0F, 0.0F, false);
        belly_3.setTextureOffset(270, 89).addBox(-8.0F, -17.0F, -19.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);

        plane_3 = new ModelRenderer(this);
        plane_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_3.addChild(plane_3);
        setRotationAngle(plane_3, 0.5236F, 0.0F, 0.0F);
        plane_3.setTextureOffset(31, 132).addBox(-31.2F, -80.0F, -20.0F, 62.0F, 4.0F, 6.0F, 0.0F, false);
        plane_3.setTextureOffset(37, 124).addBox(-28.2F, -80.0F, -14.0F, 56.0F, 4.0F, 6.0F, 0.0F, false);
        plane_3.setTextureOffset(41, 108).addBox(-26.0F, -80.0F, -8.0F, 51.0F, 4.0F, 6.0F, 0.0F, false);
        plane_3.setTextureOffset(52, 96).addBox(-22.0F, -80.0F, -2.0F, 44.0F, 4.0F, 6.0F, 0.0F, false);
        plane_3.setTextureOffset(253, 82).addBox(-17.0F, -77.0F, -17.0F, 34.0F, 8.0F, 30.0F, 0.0F, false);
        plane_3.setTextureOffset(267, 84).addBox(-11.0F, -69.0F, -11.0F, 22.0F, 4.0F, 24.0F, 0.0F, false);
        plane_3.setTextureOffset(67, 85).addBox(-19.0F, -80.0F, 4.0F, 38.0F, 4.0F, 6.0F, 0.0F, false);
        plane_3.setTextureOffset(70, 76).addBox(-16.0F, -80.0F, 10.0F, 32.0F, 4.0F, 6.0F, 0.0F, false);
        plane_3.setTextureOffset(67, 64).addBox(-14.0F, -80.0F, 16.0F, 27.0F, 4.0F, 6.0F, 0.0F, false);

        rib_3 = new ModelRenderer(this);
        rib_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_3.addChild(rib_3);
        setRotationAngle(rib_3, 0.0F, -0.5236F, 0.0F);
        

        rib_tilt_3 = new ModelRenderer(this);
        rib_tilt_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_3.addChild(rib_tilt_3);
        setRotationAngle(rib_tilt_3, 0.4363F, 0.0F, 0.0F);
        rib_tilt_3.setTextureOffset(180, 260).addBox(-2.0F, -84.0F, -40.0F, 4.0F, 8.0F, 52.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(56, 277).addBox(-2.0F, -76.0F, -16.0F, 4.0F, 6.0F, 29.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(75, 280).addBox(-1.0F, -76.0F, -20.0F, 2.0F, 26.0F, 4.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(78, 275).addBox(-1.0F, -76.0F, -12.0F, 2.0F, 26.0F, 10.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(56, 277).addBox(-1.0F, -53.0F, -16.0F, 2.0F, 3.0F, 4.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(56, 277).addBox(-1.0F, -59.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(65, 292).addBox(-1.0F, -65.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(65, 292).addBox(-1.0F, -71.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(120, 290).addBox(-2.0F, -70.0F, 0.0F, 4.0F, 6.0F, 14.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(75, 321).addBox(-1.0F, -23.8F, -45.0F, 2.0F, 2.0F, 7.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(72, 306).addBox(-1.0F, -21.8F, -41.0F, 2.0F, 2.0F, 17.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(63, 312).addBox(-1.0F, -25.8F, -49.0F, 2.0F, 2.0F, 11.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(61, 321).addBox(-1.0F, -25.8F, -34.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(61, 320).addBox(-1.0F, -19.8F, -34.0F, 2.0F, 3.0F, 9.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(91, 324).addBox(-1.0F, -25.8F, -26.3F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(73, 300).addBox(-1.0F, -27.8F, -49.0F, 2.0F, 2.0F, 28.0F, 0.0F, false);

        rib_deco_3 = new ModelRenderer(this);
        rib_deco_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_tilt_3.addChild(rib_deco_3);
        rib_deco_3.setTextureOffset(76, 308).addBox(-4.0F, -85.0F, -41.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_3.setTextureOffset(80, 304).addBox(-3.0F, -79.0F, -42.0F, 6.0F, 4.0F, 4.0F, 0.0F, false);
        rib_deco_3.setTextureOffset(91, 291).addBox(-4.0F, -85.0F, 7.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_3.setTextureOffset(180, 260).addBox(-3.0F, -84.0F, 8.0F, 6.0F, 7.0F, 8.0F, 0.0F, false);
        rib_deco_3.setTextureOffset(306, 89).addBox(-4.0F, -85.3F, -9.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_3.setTextureOffset(317, 89).addBox(-4.0F, -84.9F, -13.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_3.setTextureOffset(310, 100).addBox(-4.0F, -84.2F, -17.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_3.setTextureOffset(90, 301).addBox(-4.0F, -85.0F, 4.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);

        base_fin_3 = new ModelRenderer(this);
        base_fin_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_3.addChild(base_fin_3);
        setRotationAngle(base_fin_3, 0.0F, -0.5236F, 0.0F);
        base_fin_3.setTextureOffset(48, 225).addBox(-1.4F, -64.0F, -31.0F, 3.0F, 63.0F, 23.0F, 0.0F, false);
        base_fin_3.setTextureOffset(44, 261).addBox(-0.4F, -40.0F, -39.0F, 1.0F, 28.0F, 2.0F, 0.0F, false);
        base_fin_3.setTextureOffset(44, 261).addBox(-0.4F, -41.0F, -33.0F, 1.0F, 28.0F, 2.0F, 0.0F, false);
        base_fin_3.setTextureOffset(44, 261).addBox(-0.4F, -16.0F, -37.0F, 1.0F, 4.0F, 6.0F, 0.0F, false);
        base_fin_3.setTextureOffset(44, 261).addBox(-0.4F, -24.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        base_fin_3.setTextureOffset(44, 261).addBox(-0.4F, -32.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        base_fin_3.setTextureOffset(44, 261).addBox(-0.4F, -40.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);

        clawfoot_3 = new ModelRenderer(this);
        clawfoot_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_3.addChild(clawfoot_3);
        setRotationAngle(clawfoot_3, 0.0F, -0.5236F, 0.0F);
        

        leg_3 = new ModelRenderer(this);
        leg_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_3.addChild(leg_3);
        leg_3.setTextureOffset(46, 309).addBox(-2.4F, -4.0F, -52.0F, 5.0F, 4.0F, 36.0F, 0.0F, false);
        leg_3.setTextureOffset(61, 326).addBox(-2.2F, -3.9F, -55.0F, 4.0F, 3.0F, 3.0F, 0.0F, false);

        ball_3 = new ModelRenderer(this);
        ball_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_3.addChild(ball_3);
        ball_3.setTextureOffset(267, 95).addBox(-1.4F, -8.0F, -63.0F, 3.0F, 8.0F, 8.0F, 0.0F, false);
        ball_3.setTextureOffset(267, 95).addBox(-2.4F, -7.5F, -62.5F, 5.0F, 7.0F, 7.0F, 0.0F, false);
        ball_3.setTextureOffset(267, 95).addBox(-3.4F, -7.0F, -62.1F, 7.0F, 6.0F, 6.0F, 0.0F, false);
        ball_3.setTextureOffset(267, 95).addBox(-4.4F, -6.5F, -61.7F, 9.0F, 5.0F, 5.0F, 0.0F, false);

        glow_ball_3 = new LightModelRenderer(this);
        glow_ball_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        ball_3.addChild(glow_ball_3);
        glow_ball_3.setTextureOffset(228, 408).addBox(-5.4F, -5.5F, -60.7F, 11.0F, 3.0F, 3.0F, 0.0F, false);

        station_4 = new ModelRenderer(this);
        station_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        stations.addChild(station_4);
        setRotationAngle(station_4, 0.0F, 3.1416F, 0.0F);
        

        edge_4 = new ModelRenderer(this);
        edge_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_4.addChild(edge_4);
        edge_4.setTextureOffset(51, 12).addBox(-34.0F, -60.0F, -61.0F, 68.0F, 4.0F, 6.0F, 0.0F, false);
        edge_4.setTextureOffset(177, 229).addBox(-10.0F, -79.0F, -21.0F, 20.0F, 16.0F, 10.0F, 0.0F, false);
        edge_4.setTextureOffset(157, 251).addBox(-10.0F, -85.0F, -17.0F, 20.0F, 6.0F, 6.0F, 0.0F, false);
        edge_4.setTextureOffset(144, 226).addBox(-10.0F, -144.0F, -17.0F, 20.0F, 6.0F, 6.0F, 0.0F, false);
        edge_4.setTextureOffset(160, 235).addBox(-10.0F, -89.0F, -17.0F, 20.0F, 2.0F, 6.0F, 0.0F, false);
        edge_4.setTextureOffset(166, 292).addBox(-10.0F, -136.0F, -17.0F, 20.0F, 2.0F, 6.0F, 0.0F, false);
        edge_4.setTextureOffset(270, 99).addBox(-10.0F, -87.0F, -15.0F, 20.0F, 4.0F, 4.0F, 0.0F, false);
        edge_4.setTextureOffset(276, 94).addBox(-10.0F, -140.0F, -15.0F, 20.0F, 4.0F, 4.0F, 0.0F, false);

        cooling_blades_4 = new ModelRenderer(this);
        cooling_blades_4.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_4.addChild(cooling_blades_4);
        cooling_blades_4.setTextureOffset(287, 100).addBox(-11.0F, -25.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_4.setTextureOffset(287, 100).addBox(-11.0F, -37.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_4.setTextureOffset(287, 100).addBox(-12.0F, -26.0F, -24.0F, 24.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_4.setTextureOffset(287, 100).addBox(-12.0F, -38.0F, -24.0F, 24.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_4.setTextureOffset(287, 100).addBox(-11.0F, -27.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_4.setTextureOffset(287, 100).addBox(-11.0F, -39.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_4.setTextureOffset(287, 100).addBox(-8.0F, -27.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_4.setTextureOffset(287, 100).addBox(-8.0F, -39.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_4.setTextureOffset(287, 100).addBox(-8.0F, -25.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_4.setTextureOffset(287, 100).addBox(-8.0F, -37.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);

        plasma_coil_4 = new ModelRenderer(this);
        plasma_coil_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        edge_4.addChild(plasma_coil_4);
        plasma_coil_4.setTextureOffset(212, 310).addBox(-10.0F, -81.0F, -19.0F, 20.0F, 2.0F, 2.0F, 0.0F, false);

        glow_coil_4 = new LightModelRenderer(this);
        glow_coil_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        plasma_coil_4.addChild(glow_coil_4);
        glow_coil_4.setTextureOffset(437, 357).addBox(-5.1F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        glow_coil_4.setTextureOffset(437, 357).addBox(-1.1F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        glow_coil_4.setTextureOffset(437, 357).addBox(2.9F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        belly_4 = new ModelRenderer(this);
        belly_4.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_4.addChild(belly_4);
        belly_4.setTextureOffset(270, 89).addBox(-8.0F, -15.0F, -24.0F, 16.0F, 4.0F, 8.0F, 0.0F, false);
        belly_4.setTextureOffset(270, 89).addBox(-8.0F, -1.0F, -24.0F, 16.0F, 3.0F, 8.0F, 0.0F, false);
        belly_4.setTextureOffset(313, 55).addBox(-4.0F, -1.0F, -28.0F, 8.0F, 3.0F, 4.0F, 0.0F, false);
        belly_4.setTextureOffset(270, 89).addBox(-8.0F, -11.0F, -21.0F, 3.0F, 10.0F, 5.0F, 0.0F, false);
        belly_4.setTextureOffset(270, 89).addBox(5.0F, -11.0F, -21.0F, 3.0F, 10.0F, 5.0F, 0.0F, false);
        belly_4.setTextureOffset(270, 89).addBox(2.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_4.setTextureOffset(270, 89).addBox(-4.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_4.setTextureOffset(270, 89).addBox(-1.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_4.setTextureOffset(270, 89).addBox(-5.0F, -12.0F, -21.0F, 10.0F, 2.0F, 5.0F, 0.0F, false);
        belly_4.setTextureOffset(270, 89).addBox(-5.0F, -2.0F, -21.0F, 10.0F, 2.0F, 5.0F, 0.0F, false);
        belly_4.setTextureOffset(270, 89).addBox(-11.0F, -15.0F, -24.0F, 3.0F, 17.0F, 8.0F, 0.0F, false);
        belly_4.setTextureOffset(270, 89).addBox(8.0F, -15.0F, -24.0F, 3.0F, 17.0F, 8.0F, 0.0F, false);
        belly_4.setTextureOffset(270, 89).addBox(-8.0F, -17.0F, -19.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);

        plane_4 = new ModelRenderer(this);
        plane_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_4.addChild(plane_4);
        setRotationAngle(plane_4, 0.5236F, 0.0F, 0.0F);
        plane_4.setTextureOffset(31, 132).addBox(-31.2F, -80.0F, -20.0F, 62.0F, 4.0F, 6.0F, 0.0F, false);
        plane_4.setTextureOffset(37, 124).addBox(-28.2F, -80.0F, -14.0F, 56.0F, 4.0F, 6.0F, 0.0F, false);
        plane_4.setTextureOffset(41, 108).addBox(-26.0F, -80.0F, -8.0F, 51.0F, 4.0F, 6.0F, 0.0F, false);
        plane_4.setTextureOffset(52, 96).addBox(-22.0F, -80.0F, -2.0F, 44.0F, 4.0F, 6.0F, 0.0F, false);
        plane_4.setTextureOffset(253, 82).addBox(-17.0F, -77.0F, -17.0F, 34.0F, 8.0F, 30.0F, 0.0F, false);
        plane_4.setTextureOffset(267, 84).addBox(-11.0F, -69.0F, -11.0F, 22.0F, 4.0F, 24.0F, 0.0F, false);
        plane_4.setTextureOffset(67, 85).addBox(-19.0F, -80.0F, 4.0F, 38.0F, 4.0F, 6.0F, 0.0F, false);
        plane_4.setTextureOffset(70, 76).addBox(-16.0F, -80.0F, 10.0F, 32.0F, 4.0F, 6.0F, 0.0F, false);
        plane_4.setTextureOffset(67, 64).addBox(-14.0F, -80.0F, 16.0F, 27.0F, 4.0F, 6.0F, 0.0F, false);

        rib_4 = new ModelRenderer(this);
        rib_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_4.addChild(rib_4);
        setRotationAngle(rib_4, 0.0F, -0.5236F, 0.0F);
        

        rib_tilt_4 = new ModelRenderer(this);
        rib_tilt_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_4.addChild(rib_tilt_4);
        setRotationAngle(rib_tilt_4, 0.4363F, 0.0F, 0.0F);
        rib_tilt_4.setTextureOffset(180, 260).addBox(-2.0F, -84.0F, -40.0F, 4.0F, 8.0F, 52.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(56, 277).addBox(-2.0F, -76.0F, -16.0F, 4.0F, 6.0F, 29.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(75, 280).addBox(-1.0F, -76.0F, -20.0F, 2.0F, 26.0F, 4.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(78, 275).addBox(-1.0F, -76.0F, -12.0F, 2.0F, 26.0F, 10.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(56, 277).addBox(-1.0F, -53.0F, -16.0F, 2.0F, 3.0F, 4.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(56, 277).addBox(-1.0F, -59.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(65, 292).addBox(-1.0F, -65.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(65, 292).addBox(-1.0F, -71.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(124, 282).addBox(-2.0F, -70.0F, 0.0F, 4.0F, 6.0F, 14.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(75, 321).addBox(-1.0F, -23.8F, -45.0F, 2.0F, 2.0F, 7.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(72, 306).addBox(-1.0F, -21.8F, -41.0F, 2.0F, 2.0F, 17.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(63, 312).addBox(-1.0F, -25.8F, -49.0F, 2.0F, 2.0F, 11.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(61, 321).addBox(-1.0F, -25.8F, -34.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(61, 320).addBox(-1.0F, -19.8F, -34.0F, 2.0F, 3.0F, 9.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(91, 324).addBox(-1.0F, -25.8F, -26.3F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(73, 300).addBox(-1.0F, -27.8F, -49.0F, 2.0F, 2.0F, 28.0F, 0.0F, false);

        rib_deco_4 = new ModelRenderer(this);
        rib_deco_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_tilt_4.addChild(rib_deco_4);
        rib_deco_4.setTextureOffset(76, 308).addBox(-4.0F, -85.0F, -41.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_4.setTextureOffset(80, 304).addBox(-3.0F, -79.0F, -42.0F, 6.0F, 4.0F, 4.0F, 0.0F, false);
        rib_deco_4.setTextureOffset(91, 291).addBox(-4.0F, -85.0F, 7.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_4.setTextureOffset(180, 260).addBox(-3.0F, -84.0F, 8.0F, 6.0F, 7.0F, 8.0F, 0.0F, false);
        rib_deco_4.setTextureOffset(306, 89).addBox(-4.0F, -85.3F, -9.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_4.setTextureOffset(317, 89).addBox(-4.0F, -84.9F, -13.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_4.setTextureOffset(310, 100).addBox(-4.0F, -84.2F, -17.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_4.setTextureOffset(90, 301).addBox(-4.0F, -85.0F, 4.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);

        base_fin_4 = new ModelRenderer(this);
        base_fin_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_4.addChild(base_fin_4);
        setRotationAngle(base_fin_4, 0.0F, -0.5236F, 0.0F);
        base_fin_4.setTextureOffset(48, 225).addBox(-1.4F, -64.0F, -31.0F, 3.0F, 63.0F, 23.0F, 0.0F, false);
        base_fin_4.setTextureOffset(44, 261).addBox(-0.4F, -40.0F, -39.0F, 1.0F, 28.0F, 2.0F, 0.0F, false);
        base_fin_4.setTextureOffset(44, 261).addBox(-0.4F, -41.0F, -33.0F, 1.0F, 28.0F, 2.0F, 0.0F, false);
        base_fin_4.setTextureOffset(44, 261).addBox(-0.4F, -16.0F, -37.0F, 1.0F, 4.0F, 6.0F, 0.0F, false);
        base_fin_4.setTextureOffset(44, 261).addBox(-0.4F, -24.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        base_fin_4.setTextureOffset(44, 261).addBox(-0.4F, -32.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        base_fin_4.setTextureOffset(44, 261).addBox(-0.4F, -40.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);

        clawfoot_4 = new ModelRenderer(this);
        clawfoot_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_4.addChild(clawfoot_4);
        setRotationAngle(clawfoot_4, 0.0F, -0.5236F, 0.0F);
        

        leg_4 = new ModelRenderer(this);
        leg_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_4.addChild(leg_4);
        leg_4.setTextureOffset(46, 309).addBox(-2.4F, -4.0F, -52.0F, 5.0F, 4.0F, 36.0F, 0.0F, false);
        leg_4.setTextureOffset(61, 326).addBox(-2.2F, -3.9F, -55.0F, 4.0F, 3.0F, 3.0F, 0.0F, false);

        ball_4 = new ModelRenderer(this);
        ball_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_4.addChild(ball_4);
        ball_4.setTextureOffset(267, 95).addBox(-1.4F, -8.0F, -63.0F, 3.0F, 8.0F, 8.0F, 0.0F, false);
        ball_4.setTextureOffset(267, 95).addBox(-2.4F, -7.5F, -62.5F, 5.0F, 7.0F, 7.0F, 0.0F, false);
        ball_4.setTextureOffset(267, 95).addBox(-3.4F, -7.0F, -62.1F, 7.0F, 6.0F, 6.0F, 0.0F, false);
        ball_4.setTextureOffset(267, 95).addBox(-4.4F, -6.5F, -61.7F, 9.0F, 5.0F, 5.0F, 0.0F, false);

        glow_ball_4 = new LightModelRenderer(this);
        glow_ball_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        ball_4.addChild(glow_ball_4);
        glow_ball_4.setTextureOffset(228, 408).addBox(-5.4F, -5.5F, -60.7F, 11.0F, 3.0F, 3.0F, 0.0F, false);

        station_5 = new ModelRenderer(this);
        station_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        stations.addChild(station_5);
        setRotationAngle(station_5, 0.0F, 2.0944F, 0.0F);
        

        edge_5 = new ModelRenderer(this);
        edge_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_5.addChild(edge_5);
        edge_5.setTextureOffset(51, 12).addBox(-34.0F, -60.0F, -61.0F, 68.0F, 4.0F, 6.0F, 0.0F, false);
        edge_5.setTextureOffset(177, 229).addBox(-10.0F, -79.0F, -21.0F, 20.0F, 16.0F, 10.0F, 0.0F, false);
        edge_5.setTextureOffset(157, 251).addBox(-10.0F, -85.0F, -17.0F, 20.0F, 6.0F, 6.0F, 0.0F, false);
        edge_5.setTextureOffset(144, 226).addBox(-10.0F, -144.0F, -17.0F, 20.0F, 6.0F, 6.0F, 0.0F, false);
        edge_5.setTextureOffset(160, 235).addBox(-10.0F, -89.0F, -17.0F, 20.0F, 2.0F, 6.0F, 0.0F, false);
        edge_5.setTextureOffset(166, 292).addBox(-10.0F, -136.0F, -17.0F, 20.0F, 2.0F, 6.0F, 0.0F, false);
        edge_5.setTextureOffset(270, 99).addBox(-10.0F, -87.0F, -15.0F, 20.0F, 4.0F, 4.0F, 0.0F, false);
        edge_5.setTextureOffset(276, 94).addBox(-10.0F, -140.0F, -15.0F, 20.0F, 4.0F, 4.0F, 0.0F, false);

        cooling_blades_5 = new ModelRenderer(this);
        cooling_blades_5.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_5.addChild(cooling_blades_5);
        cooling_blades_5.setTextureOffset(287, 100).addBox(-11.0F, -25.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_5.setTextureOffset(287, 100).addBox(-11.0F, -37.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_5.setTextureOffset(287, 100).addBox(-12.0F, -26.0F, -24.0F, 24.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_5.setTextureOffset(287, 100).addBox(-12.0F, -38.0F, -24.0F, 24.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_5.setTextureOffset(287, 100).addBox(-11.0F, -27.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_5.setTextureOffset(287, 100).addBox(-11.0F, -39.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_5.setTextureOffset(287, 100).addBox(-8.0F, -27.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_5.setTextureOffset(287, 100).addBox(-8.0F, -39.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_5.setTextureOffset(287, 100).addBox(-8.0F, -25.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_5.setTextureOffset(287, 100).addBox(-8.0F, -37.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);

        plasma_coil_5 = new ModelRenderer(this);
        plasma_coil_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        edge_5.addChild(plasma_coil_5);
        plasma_coil_5.setTextureOffset(212, 310).addBox(-10.0F, -81.0F, -19.0F, 20.0F, 2.0F, 2.0F, 0.0F, false);

        glow_coil_5 = new LightModelRenderer(this);
        glow_coil_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        plasma_coil_5.addChild(glow_coil_5);
        glow_coil_5.setTextureOffset(437, 357).addBox(-5.1F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        glow_coil_5.setTextureOffset(437, 357).addBox(-1.1F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        glow_coil_5.setTextureOffset(437, 357).addBox(2.9F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        belly_5 = new ModelRenderer(this);
        belly_5.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_5.addChild(belly_5);
        belly_5.setTextureOffset(270, 89).addBox(-8.0F, -15.0F, -24.0F, 16.0F, 4.0F, 8.0F, 0.0F, false);
        belly_5.setTextureOffset(270, 89).addBox(-8.0F, -1.0F, -24.0F, 16.0F, 3.0F, 8.0F, 0.0F, false);
        belly_5.setTextureOffset(313, 55).addBox(-4.0F, -1.0F, -28.0F, 8.0F, 3.0F, 4.0F, 0.0F, false);
        belly_5.setTextureOffset(270, 89).addBox(-8.0F, -11.0F, -21.0F, 3.0F, 10.0F, 5.0F, 0.0F, false);
        belly_5.setTextureOffset(270, 89).addBox(5.0F, -11.0F, -21.0F, 3.0F, 10.0F, 5.0F, 0.0F, false);
        belly_5.setTextureOffset(270, 89).addBox(2.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_5.setTextureOffset(270, 89).addBox(-4.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_5.setTextureOffset(270, 89).addBox(-1.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_5.setTextureOffset(270, 89).addBox(-5.0F, -12.0F, -21.0F, 10.0F, 2.0F, 5.0F, 0.0F, false);
        belly_5.setTextureOffset(270, 89).addBox(-5.0F, -2.0F, -21.0F, 10.0F, 2.0F, 5.0F, 0.0F, false);
        belly_5.setTextureOffset(270, 89).addBox(-11.0F, -15.0F, -24.0F, 3.0F, 17.0F, 8.0F, 0.0F, false);
        belly_5.setTextureOffset(270, 89).addBox(8.0F, -15.0F, -24.0F, 3.0F, 17.0F, 8.0F, 0.0F, false);
        belly_5.setTextureOffset(270, 89).addBox(-8.0F, -17.0F, -19.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);

        plane_5 = new ModelRenderer(this);
        plane_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_5.addChild(plane_5);
        setRotationAngle(plane_5, 0.5236F, 0.0F, 0.0F);
        plane_5.setTextureOffset(31, 132).addBox(-31.2F, -80.0F, -20.0F, 62.0F, 4.0F, 6.0F, 0.0F, false);
        plane_5.setTextureOffset(37, 124).addBox(-28.2F, -80.0F, -14.0F, 56.0F, 4.0F, 6.0F, 0.0F, false);
        plane_5.setTextureOffset(41, 108).addBox(-26.0F, -80.0F, -8.0F, 51.0F, 4.0F, 6.0F, 0.0F, false);
        plane_5.setTextureOffset(52, 96).addBox(-22.0F, -80.0F, -2.0F, 44.0F, 4.0F, 6.0F, 0.0F, false);
        plane_5.setTextureOffset(253, 82).addBox(-17.0F, -77.0F, -17.0F, 34.0F, 8.0F, 30.0F, 0.0F, false);
        plane_5.setTextureOffset(267, 84).addBox(-11.0F, -69.0F, -11.0F, 22.0F, 4.0F, 24.0F, 0.0F, false);
        plane_5.setTextureOffset(67, 85).addBox(-19.0F, -80.0F, 4.0F, 38.0F, 4.0F, 6.0F, 0.0F, false);
        plane_5.setTextureOffset(70, 76).addBox(-16.0F, -80.0F, 10.0F, 32.0F, 4.0F, 6.0F, 0.0F, false);
        plane_5.setTextureOffset(67, 64).addBox(-14.0F, -80.0F, 16.0F, 27.0F, 4.0F, 6.0F, 0.0F, false);

        rib_5 = new ModelRenderer(this);
        rib_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_5.addChild(rib_5);
        setRotationAngle(rib_5, 0.0F, -0.5236F, 0.0F);
        

        rib_tilt_5 = new ModelRenderer(this);
        rib_tilt_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_5.addChild(rib_tilt_5);
        setRotationAngle(rib_tilt_5, 0.4363F, 0.0F, 0.0F);
        rib_tilt_5.setTextureOffset(180, 260).addBox(-2.0F, -84.0F, -40.0F, 4.0F, 8.0F, 52.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(56, 277).addBox(-2.0F, -76.0F, -16.0F, 4.0F, 6.0F, 29.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(75, 280).addBox(-1.0F, -76.0F, -20.0F, 2.0F, 26.0F, 4.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(78, 275).addBox(-1.0F, -76.0F, -12.0F, 2.0F, 26.0F, 10.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(56, 277).addBox(-1.0F, -53.0F, -16.0F, 2.0F, 3.0F, 4.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(56, 277).addBox(-1.0F, -59.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(65, 292).addBox(-1.0F, -65.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(65, 292).addBox(-1.0F, -71.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(130, 289).addBox(-2.0F, -70.0F, 0.0F, 4.0F, 6.0F, 14.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(75, 321).addBox(-1.0F, -23.8F, -45.0F, 2.0F, 2.0F, 7.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(72, 306).addBox(-1.0F, -21.8F, -41.0F, 2.0F, 2.0F, 17.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(63, 312).addBox(-1.0F, -25.8F, -49.0F, 2.0F, 2.0F, 11.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(61, 321).addBox(-1.0F, -25.8F, -34.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(61, 320).addBox(-1.0F, -19.8F, -34.0F, 2.0F, 3.0F, 9.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(91, 324).addBox(-1.0F, -25.8F, -26.3F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(73, 300).addBox(-1.0F, -27.8F, -49.0F, 2.0F, 2.0F, 28.0F, 0.0F, false);

        rib_deco_5 = new ModelRenderer(this);
        rib_deco_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_tilt_5.addChild(rib_deco_5);
        rib_deco_5.setTextureOffset(76, 308).addBox(-4.0F, -85.0F, -41.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_5.setTextureOffset(80, 304).addBox(-3.0F, -79.0F, -42.0F, 6.0F, 4.0F, 4.0F, 0.0F, false);
        rib_deco_5.setTextureOffset(91, 291).addBox(-4.0F, -85.0F, 7.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_5.setTextureOffset(180, 260).addBox(-3.0F, -84.0F, 8.0F, 6.0F, 7.0F, 8.0F, 0.0F, false);
        rib_deco_5.setTextureOffset(306, 89).addBox(-4.0F, -85.3F, -9.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_5.setTextureOffset(317, 89).addBox(-4.0F, -84.9F, -13.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_5.setTextureOffset(310, 100).addBox(-4.0F, -84.2F, -17.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_5.setTextureOffset(90, 301).addBox(-4.0F, -85.0F, 4.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);

        base_fin_5 = new ModelRenderer(this);
        base_fin_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_5.addChild(base_fin_5);
        setRotationAngle(base_fin_5, 0.0F, -0.5236F, 0.0F);
        base_fin_5.setTextureOffset(48, 225).addBox(-1.4F, -64.0F, -31.0F, 3.0F, 63.0F, 23.0F, 0.0F, false);
        base_fin_5.setTextureOffset(44, 261).addBox(-0.4F, -40.0F, -39.0F, 1.0F, 28.0F, 2.0F, 0.0F, false);
        base_fin_5.setTextureOffset(44, 261).addBox(-0.4F, -41.0F, -33.0F, 1.0F, 28.0F, 2.0F, 0.0F, false);
        base_fin_5.setTextureOffset(44, 261).addBox(-0.4F, -16.0F, -37.0F, 1.0F, 4.0F, 6.0F, 0.0F, false);
        base_fin_5.setTextureOffset(44, 261).addBox(-0.4F, -24.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        base_fin_5.setTextureOffset(44, 261).addBox(-0.4F, -32.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        base_fin_5.setTextureOffset(44, 261).addBox(-0.4F, -40.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);

        clawfoot_5 = new ModelRenderer(this);
        clawfoot_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_5.addChild(clawfoot_5);
        setRotationAngle(clawfoot_5, 0.0F, -0.5236F, 0.0F);
        

        leg_5 = new ModelRenderer(this);
        leg_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_5.addChild(leg_5);
        leg_5.setTextureOffset(46, 309).addBox(-2.4F, -4.0F, -52.0F, 5.0F, 4.0F, 36.0F, 0.0F, false);
        leg_5.setTextureOffset(61, 326).addBox(-2.2F, -3.9F, -55.0F, 4.0F, 3.0F, 3.0F, 0.0F, false);

        ball_5 = new ModelRenderer(this);
        ball_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_5.addChild(ball_5);
        ball_5.setTextureOffset(267, 95).addBox(-1.4F, -8.0F, -63.0F, 3.0F, 8.0F, 8.0F, 0.0F, false);
        ball_5.setTextureOffset(267, 95).addBox(-2.4F, -7.5F, -62.5F, 5.0F, 7.0F, 7.0F, 0.0F, false);
        ball_5.setTextureOffset(267, 95).addBox(-3.4F, -7.0F, -62.1F, 7.0F, 6.0F, 6.0F, 0.0F, false);
        ball_5.setTextureOffset(267, 95).addBox(-4.4F, -6.5F, -61.7F, 9.0F, 5.0F, 5.0F, 0.0F, false);

        glow_ball_5 = new LightModelRenderer(this);
        glow_ball_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        ball_5.addChild(glow_ball_5);
        glow_ball_5.setTextureOffset(228, 408).addBox(-5.4F, -5.5F, -60.7F, 11.0F, 3.0F, 3.0F, 0.0F, false);

        station_6 = new ModelRenderer(this);
        station_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        stations.addChild(station_6);
        setRotationAngle(station_6, 0.0F, 1.0472F, 0.0F);
        

        edge_6 = new ModelRenderer(this);
        edge_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_6.addChild(edge_6);
        edge_6.setTextureOffset(51, 12).addBox(-34.0F, -60.0F, -61.0F, 68.0F, 4.0F, 6.0F, 0.0F, false);
        edge_6.setTextureOffset(177, 229).addBox(-10.0F, -79.0F, -21.0F, 20.0F, 16.0F, 10.0F, 0.0F, false);
        edge_6.setTextureOffset(157, 251).addBox(-10.0F, -85.0F, -17.0F, 20.0F, 6.0F, 6.0F, 0.0F, false);
        edge_6.setTextureOffset(144, 226).addBox(-10.0F, -144.0F, -17.0F, 20.0F, 6.0F, 6.0F, 0.0F, false);
        edge_6.setTextureOffset(160, 235).addBox(-10.0F, -89.0F, -17.0F, 20.0F, 2.0F, 6.0F, 0.0F, false);
        edge_6.setTextureOffset(166, 292).addBox(-10.0F, -136.0F, -17.0F, 20.0F, 2.0F, 6.0F, 0.0F, false);
        edge_6.setTextureOffset(270, 99).addBox(-10.0F, -87.0F, -15.0F, 20.0F, 4.0F, 4.0F, 0.0F, false);
        edge_6.setTextureOffset(276, 94).addBox(-10.0F, -140.0F, -15.0F, 20.0F, 4.0F, 4.0F, 0.0F, false);

        cooling_blades_6 = new ModelRenderer(this);
        cooling_blades_6.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_6.addChild(cooling_blades_6);
        cooling_blades_6.setTextureOffset(287, 100).addBox(-11.0F, -25.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_6.setTextureOffset(287, 100).addBox(-11.0F, -37.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_6.setTextureOffset(287, 100).addBox(-12.0F, -26.0F, -24.0F, 24.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_6.setTextureOffset(287, 100).addBox(-12.0F, -38.0F, -24.0F, 24.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_6.setTextureOffset(287, 100).addBox(-11.0F, -27.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_6.setTextureOffset(287, 100).addBox(-11.0F, -39.0F, -22.0F, 22.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_6.setTextureOffset(287, 100).addBox(-8.0F, -27.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_6.setTextureOffset(287, 100).addBox(-8.0F, -39.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_6.setTextureOffset(287, 100).addBox(-8.0F, -25.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);
        cooling_blades_6.setTextureOffset(287, 100).addBox(-8.0F, -37.0F, -17.0F, 16.0F, 1.0F, 5.0F, 0.0F, false);

        plasma_coil_6 = new ModelRenderer(this);
        plasma_coil_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        edge_6.addChild(plasma_coil_6);
        plasma_coil_6.setTextureOffset(212, 310).addBox(-10.0F, -81.0F, -19.0F, 20.0F, 2.0F, 2.0F, 0.0F, false);

        glow_coil_6 = new LightModelRenderer(this);
        glow_coil_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        plasma_coil_6.addChild(glow_coil_6);
        glow_coil_6.setTextureOffset(437, 357).addBox(-5.1F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        glow_coil_6.setTextureOffset(437, 357).addBox(-1.1F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        glow_coil_6.setTextureOffset(437, 357).addBox(2.9F, -83.0F, -21.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        belly_6 = new ModelRenderer(this);
        belly_6.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_6.addChild(belly_6);
        belly_6.setTextureOffset(270, 89).addBox(-8.0F, -15.0F, -24.0F, 16.0F, 4.0F, 8.0F, 0.0F, false);
        belly_6.setTextureOffset(270, 89).addBox(-8.0F, -1.0F, -24.0F, 16.0F, 3.0F, 8.0F, 0.0F, false);
        belly_6.setTextureOffset(313, 55).addBox(-4.0F, -1.0F, -28.0F, 8.0F, 3.0F, 4.0F, 0.0F, false);
        belly_6.setTextureOffset(270, 89).addBox(-8.0F, -11.0F, -21.0F, 3.0F, 10.0F, 5.0F, 0.0F, false);
        belly_6.setTextureOffset(270, 89).addBox(5.0F, -11.0F, -21.0F, 3.0F, 10.0F, 5.0F, 0.0F, false);
        belly_6.setTextureOffset(270, 89).addBox(2.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_6.setTextureOffset(270, 89).addBox(-4.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_6.setTextureOffset(270, 89).addBox(-1.0F, -10.0F, -21.0F, 2.0F, 8.0F, 5.0F, 0.0F, false);
        belly_6.setTextureOffset(270, 89).addBox(-5.0F, -12.0F, -21.0F, 10.0F, 2.0F, 5.0F, 0.0F, false);
        belly_6.setTextureOffset(270, 89).addBox(-5.0F, -2.0F, -21.0F, 10.0F, 2.0F, 5.0F, 0.0F, false);
        belly_6.setTextureOffset(270, 89).addBox(-11.0F, -15.0F, -24.0F, 3.0F, 17.0F, 8.0F, 0.0F, false);
        belly_6.setTextureOffset(270, 89).addBox(8.0F, -15.0F, -24.0F, 3.0F, 17.0F, 8.0F, 0.0F, false);
        belly_6.setTextureOffset(270, 89).addBox(-8.0F, -17.0F, -19.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);

        plane_6 = new ModelRenderer(this);
        plane_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_6.addChild(plane_6);
        setRotationAngle(plane_6, 0.5236F, 0.0F, 0.0F);
        plane_6.setTextureOffset(31, 132).addBox(-31.2F, -80.0F, -20.0F, 62.0F, 4.0F, 6.0F, 0.0F, false);
        plane_6.setTextureOffset(37, 124).addBox(-28.2F, -80.0F, -14.0F, 56.0F, 4.0F, 6.0F, 0.0F, false);
        plane_6.setTextureOffset(41, 108).addBox(-26.0F, -80.0F, -8.0F, 51.0F, 4.0F, 6.0F, 0.0F, false);
        plane_6.setTextureOffset(52, 96).addBox(-22.0F, -80.0F, -2.0F, 44.0F, 4.0F, 6.0F, 0.0F, false);
        plane_6.setTextureOffset(253, 82).addBox(-17.0F, -77.0F, -17.0F, 34.0F, 8.0F, 30.0F, 0.0F, false);
        plane_6.setTextureOffset(267, 84).addBox(-11.0F, -69.0F, -11.0F, 22.0F, 4.0F, 24.0F, 0.0F, false);
        plane_6.setTextureOffset(67, 85).addBox(-19.0F, -80.0F, 4.0F, 38.0F, 4.0F, 6.0F, 0.0F, false);
        plane_6.setTextureOffset(70, 76).addBox(-16.0F, -80.0F, 10.0F, 32.0F, 4.0F, 6.0F, 0.0F, false);
        plane_6.setTextureOffset(67, 64).addBox(-14.0F, -80.0F, 16.0F, 27.0F, 4.0F, 6.0F, 0.0F, false);

        rib_6 = new ModelRenderer(this);
        rib_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_6.addChild(rib_6);
        setRotationAngle(rib_6, 0.0F, -0.5236F, 0.0F);
        

        rib_tilt_6 = new ModelRenderer(this);
        rib_tilt_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_6.addChild(rib_tilt_6);
        setRotationAngle(rib_tilt_6, 0.4363F, 0.0F, 0.0F);
        rib_tilt_6.setTextureOffset(180, 260).addBox(-2.0F, -84.0F, -40.0F, 4.0F, 8.0F, 52.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(56, 277).addBox(-2.0F, -76.0F, -16.0F, 4.0F, 6.0F, 29.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(75, 280).addBox(-1.0F, -76.0F, -20.0F, 2.0F, 26.0F, 4.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(78, 275).addBox(-1.0F, -76.0F, -12.0F, 2.0F, 26.0F, 10.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(56, 277).addBox(-1.0F, -53.0F, -16.0F, 2.0F, 3.0F, 4.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(56, 277).addBox(-1.0F, -59.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(65, 292).addBox(-1.0F, -65.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(65, 292).addBox(-1.0F, -71.0F, -16.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(104, 287).addBox(-2.0F, -70.0F, 0.0F, 4.0F, 6.0F, 14.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(75, 321).addBox(-1.0F, -23.8F, -45.0F, 2.0F, 2.0F, 7.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(72, 306).addBox(-1.0F, -21.8F, -41.0F, 2.0F, 2.0F, 17.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(63, 312).addBox(-1.0F, -25.8F, -49.0F, 2.0F, 2.0F, 11.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(61, 321).addBox(-1.0F, -25.8F, -34.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(61, 320).addBox(-1.0F, -19.8F, -34.0F, 2.0F, 3.0F, 9.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(91, 324).addBox(-1.0F, -25.8F, -26.3F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(73, 300).addBox(-1.0F, -27.8F, -49.0F, 2.0F, 2.0F, 28.0F, 0.0F, false);

        rib_deco_6 = new ModelRenderer(this);
        rib_deco_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_tilt_6.addChild(rib_deco_6);
        rib_deco_6.setTextureOffset(76, 308).addBox(-4.0F, -85.0F, -41.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_6.setTextureOffset(80, 304).addBox(-3.0F, -79.0F, -42.0F, 6.0F, 4.0F, 4.0F, 0.0F, false);
        rib_deco_6.setTextureOffset(91, 291).addBox(-4.0F, -85.0F, 7.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_6.setTextureOffset(180, 260).addBox(-3.0F, -84.0F, 8.0F, 6.0F, 7.0F, 8.0F, 0.0F, false);
        rib_deco_6.setTextureOffset(306, 89).addBox(-4.0F, -85.3F, -9.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_6.setTextureOffset(317, 89).addBox(-4.0F, -84.9F, -13.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_6.setTextureOffset(310, 100).addBox(-4.0F, -84.2F, -17.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);
        rib_deco_6.setTextureOffset(90, 301).addBox(-4.0F, -85.0F, 4.0F, 8.0F, 10.0F, 2.0F, 0.0F, false);

        base_fin_6 = new ModelRenderer(this);
        base_fin_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_6.addChild(base_fin_6);
        setRotationAngle(base_fin_6, 0.0F, -0.5236F, 0.0F);
        base_fin_6.setTextureOffset(48, 225).addBox(-1.4F, -64.0F, -31.0F, 3.0F, 63.0F, 23.0F, 0.0F, false);
        base_fin_6.setTextureOffset(44, 261).addBox(-0.4F, -40.0F, -39.0F, 1.0F, 28.0F, 2.0F, 0.0F, false);
        base_fin_6.setTextureOffset(44, 261).addBox(-0.4F, -41.0F, -33.0F, 1.0F, 28.0F, 2.0F, 0.0F, false);
        base_fin_6.setTextureOffset(44, 261).addBox(-0.4F, -16.0F, -37.0F, 1.0F, 4.0F, 6.0F, 0.0F, false);
        base_fin_6.setTextureOffset(44, 261).addBox(-0.4F, -24.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        base_fin_6.setTextureOffset(44, 261).addBox(-0.4F, -32.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        base_fin_6.setTextureOffset(44, 261).addBox(-0.4F, -40.0F, -37.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);

        clawfoot_6 = new ModelRenderer(this);
        clawfoot_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_6.addChild(clawfoot_6);
        setRotationAngle(clawfoot_6, 0.0F, -0.5236F, 0.0F);
        

        leg_6 = new ModelRenderer(this);
        leg_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_6.addChild(leg_6);
        leg_6.setTextureOffset(46, 309).addBox(-2.4F, -4.0F, -52.0F, 5.0F, 4.0F, 36.0F, 0.0F, false);
        leg_6.setTextureOffset(61, 326).addBox(-2.2F, -3.9F, -55.0F, 4.0F, 3.0F, 3.0F, 0.0F, false);

        ball_6 = new ModelRenderer(this);
        ball_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_6.addChild(ball_6);
        ball_6.setTextureOffset(267, 95).addBox(-1.4F, -8.0F, -63.0F, 3.0F, 8.0F, 8.0F, 0.0F, false);
        ball_6.setTextureOffset(267, 95).addBox(-2.4F, -7.5F, -62.5F, 5.0F, 7.0F, 7.0F, 0.0F, false);
        ball_6.setTextureOffset(267, 95).addBox(-3.4F, -7.0F, -62.1F, 7.0F, 6.0F, 6.0F, 0.0F, false);
        ball_6.setTextureOffset(267, 95).addBox(-4.4F, -6.5F, -61.7F, 9.0F, 5.0F, 5.0F, 0.0F, false);

        glow_ball_6 = new LightModelRenderer(this);
        glow_ball_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        ball_6.addChild(glow_ball_6);
        glow_ball_6.setTextureOffset(228, 408).addBox(-5.4F, -5.5F, -60.7F, 11.0F, 3.0F, 3.0F, 0.0F, false);

        rotor_rotate_y = new ModelRenderer(this);
        rotor_rotate_y.setRotationPoint(-0.0167F, -87.1667F, 0.0F);
        rotor_rotate_y.setTextureOffset(257, 99).addBox(-27.9833F, 23.9667F, -2.0F, 56.0F, 2.0F, 4.0F, 0.0F, false);
        rotor_rotate_y.setTextureOffset(249, 99).addBox(-27.9833F, -26.8333F, -2.0F, 56.0F, 2.0F, 4.0F, 0.0F, false);
        rotor_rotate_y.setTextureOffset(268, 73).addBox(25.6167F, -25.0333F, -1.5F, 2.0F, 49.0F, 3.0F, 0.0F, false);
        rotor_rotate_y.setTextureOffset(273, 67).addBox(-27.6833F, -25.0333F, -1.5F, 2.0F, 49.0F, 3.0F, 0.0F, false);
        rotor_rotate_y.setTextureOffset(212, 400).addBox(-28.9833F, -1.0333F, -4.0F, 4.0F, 4.0F, 8.0F, 0.0F, false);
        rotor_rotate_y.setTextureOffset(214, 413).addBox(24.6167F, -1.0333F, -4.0F, 4.0F, 4.0F, 8.0F, 0.0F, false);
        rotor_rotate_y.setTextureOffset(338, 314).addBox(-30.9833F, -3.0333F, -2.4F, 8.0F, 8.0F, 5.0F, 0.0F, false);
        rotor_rotate_y.setTextureOffset(338, 314).addBox(22.6167F, -3.0333F, -2.4F, 8.0F, 8.0F, 5.0F, 0.0F, false);

        glow_rotor_rotate_y = new LightModelRenderer(this);
        glow_rotor_rotate_y.setRotationPoint(0.0F, 0.0F, 0.0F);
        rotor_rotate_y.addChild(glow_rotor_rotate_y);
        glow_rotor_rotate_y.setTextureOffset(214, 413).addBox(24.6167F, -1.0333F, -4.0F, 4.0F, 4.0F, 8.0F, 0.0F, false);
        glow_rotor_rotate_y.setTextureOffset(212, 400).addBox(-28.9833F, -1.0333F, -4.0F, 4.0F, 4.0F, 8.0F, 0.0F, false);
        
        controls = new ModelRenderer(this);
        controls.setRotationPoint(0.0F, 24.0F, 0.0F);
        

        controls_1 = new ModelRenderer(this);
        controls_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(controls_1);
        

        fast_return = new ModelRenderer(this);
        fast_return.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_1.addChild(fast_return);
        

        return_button = new ModelRenderer(this);
        return_button.setRotationPoint(-18.0F, -62.0F, -54.0F);
        fast_return.addChild(return_button);
        setRotationAngle(return_button, 0.5236F, 0.0F, 0.0F);
        return_button.setTextureOffset(68, 397).addBox(32.0F, 0.0F, -1.0F, 6.0F, 4.0F, 8.0F, 0.0F, false);

        glow_fast_return = new LightModelRenderer(this);
        glow_fast_return.setRotationPoint(0.0F, 0.0F, 0.0F);
        return_button.addChild(glow_fast_return);
        glow_fast_return.setTextureOffset(102, 14).addBox(33.0F, -1.0F, 1.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        waypoints = new ModelRenderer(this);
        waypoints.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_1.addChild(waypoints);
        

        way_dial_1 = new ModelRenderer(this);
        way_dial_1.setRotationPoint(-18.0F, -62.0F, -54.0F);
        waypoints.addChild(way_dial_1);
        setRotationAngle(way_dial_1, 0.5236F, 0.0F, 0.0F);
        way_dial_1.setTextureOffset(68, 397).addBox(6.2F, -0.6F, 19.3F, 6.0F, 2.0F, 2.0F, 0.0F, false);
        way_dial_1.setTextureOffset(93, 282).addBox(5.2F, -1.1F, 18.8F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        way_dial_1.setTextureOffset(116, 279).addBox(12.2F, -1.1F, 18.8F, 1.0F, 3.0F, 3.0F, 0.0F, false);

        way_dial_2 = new ModelRenderer(this);
        way_dial_2.setRotationPoint(-18.0F, -62.0F, -54.0F);
        waypoints.addChild(way_dial_2);
        setRotationAngle(way_dial_2, 0.5236F, 0.0F, 0.0F);
        way_dial_2.setTextureOffset(68, 397).addBox(14.7F, -0.6F, 19.3F, 6.0F, 2.0F, 2.0F, 0.0F, false);
        way_dial_2.setTextureOffset(104, 292).addBox(13.7F, -1.1F, 18.8F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        way_dial_2.setTextureOffset(108, 289).addBox(20.7F, -1.1F, 18.8F, 1.0F, 3.0F, 3.0F, 0.0F, false);

        way_dial_3 = new ModelRenderer(this);
        way_dial_3.setRotationPoint(-18.0F, -62.0F, -54.0F);
        waypoints.addChild(way_dial_3);
        setRotationAngle(way_dial_3, 0.5236F, 0.0F, 0.0F);
        way_dial_3.setTextureOffset(68, 397).addBox(23.2F, -0.6F, 19.3F, 6.0F, 2.0F, 2.0F, 0.0F, false);
        way_dial_3.setTextureOffset(72, 305).addBox(22.2F, -1.1F, 18.8F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        way_dial_3.setTextureOffset(72, 305).addBox(29.2F, -1.1F, 18.8F, 1.0F, 3.0F, 3.0F, 0.0F, false);

        way_dial_4 = new ModelRenderer(this);
        way_dial_4.setRotationPoint(-18.0F, -62.0F, -54.0F);
        waypoints.addChild(way_dial_4);
        setRotationAngle(way_dial_4, 0.5236F, 0.0F, 0.0F);
        way_dial_4.setTextureOffset(68, 397).addBox(6.2F, -0.6F, 15.6F, 6.0F, 2.0F, 2.0F, 0.0F, false);
        way_dial_4.setTextureOffset(92, 294).addBox(5.2F, -1.1F, 15.1F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        way_dial_4.setTextureOffset(59, 300).addBox(12.2F, -1.1F, 15.1F, 1.0F, 3.0F, 3.0F, 0.0F, false);

        way_dial_5 = new ModelRenderer(this);
        way_dial_5.setRotationPoint(-18.0F, -62.0F, -54.0F);
        waypoints.addChild(way_dial_5);
        setRotationAngle(way_dial_5, 0.5236F, 0.0F, 0.0F);
        way_dial_5.setTextureOffset(68, 397).addBox(14.8F, -0.6F, 15.6F, 6.0F, 2.0F, 2.0F, 0.0F, false);
        way_dial_5.setTextureOffset(74, 326).addBox(13.8F, -1.1F, 15.1F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        way_dial_5.setTextureOffset(84, 319).addBox(20.8F, -1.1F, 15.1F, 1.0F, 3.0F, 3.0F, 0.0F, false);

        way_dial_6 = new ModelRenderer(this);
        way_dial_6.setRotationPoint(-18.0F, -62.0F, -54.0F);
        waypoints.addChild(way_dial_6);
        setRotationAngle(way_dial_6, 0.5236F, 0.0F, 0.0F);
        way_dial_6.setTextureOffset(68, 397).addBox(23.2F, -0.6F, 15.6F, 6.0F, 2.0F, 2.0F, 0.0F, false);
        way_dial_6.setTextureOffset(83, 304).addBox(22.2F, -1.1F, 15.1F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        way_dial_6.setTextureOffset(144, 280).addBox(29.2F, -1.1F, 15.1F, 1.0F, 3.0F, 3.0F, 0.0F, false);

        way_dial_7 = new ModelRenderer(this);
        way_dial_7.setRotationPoint(-18.0F, -62.0F, -54.0F);
        waypoints.addChild(way_dial_7);
        setRotationAngle(way_dial_7, 0.5236F, 0.0F, 0.0F);
        way_dial_7.setTextureOffset(68, 397).addBox(6.2F, -0.6F, 12.0F, 6.0F, 2.0F, 2.0F, 0.0F, false);
        way_dial_7.setTextureOffset(100, 297).addBox(5.2F, -1.1F, 11.5F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        way_dial_7.setTextureOffset(80, 283).addBox(12.2F, -1.1F, 11.5F, 1.0F, 3.0F, 3.0F, 0.0F, false);

        way_dial_8 = new ModelRenderer(this);
        way_dial_8.setRotationPoint(-18.0F, -62.0F, -54.0F);
        waypoints.addChild(way_dial_8);
        setRotationAngle(way_dial_8, 0.5236F, 0.0F, 0.0F);
        way_dial_8.setTextureOffset(68, 397).addBox(14.8F, -0.6F, 12.0F, 6.0F, 2.0F, 2.0F, 0.0F, false);
        way_dial_8.setTextureOffset(73, 316).addBox(13.8F, -1.1F, 11.5F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        way_dial_8.setTextureOffset(78, 298).addBox(20.8F, -1.1F, 11.5F, 1.0F, 3.0F, 3.0F, 0.0F, false);

        way_dial_9 = new ModelRenderer(this);
        way_dial_9.setRotationPoint(-18.0F, -62.0F, -54.0F);
        waypoints.addChild(way_dial_9);
        setRotationAngle(way_dial_9, 0.5236F, 0.0F, 0.0F);
        way_dial_9.setTextureOffset(68, 397).addBox(23.2F, -0.6F, 12.0F, 6.0F, 2.0F, 2.0F, 0.0F, false);
        way_dial_9.setTextureOffset(78, 329).addBox(22.2F, -1.1F, 11.5F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        way_dial_9.setTextureOffset(74, 332).addBox(29.2F, -1.1F, 11.5F, 1.0F, 3.0F, 3.0F, 0.0F, false);

        dummy_aa_1 = new LightModelRenderer(this);
        dummy_aa_1.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_1.addChild(dummy_aa_1);
        setRotationAngle(dummy_aa_1, 0.5236F, 0.0F, 0.0F);
        dummy_aa_1.setTextureOffset(430, 367).addBox(-4.3F, 0.0F, 29.4F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        dummy_aa_4 = new LightModelRenderer(this);
        dummy_aa_4.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_1.addChild(dummy_aa_4);
        setRotationAngle(dummy_aa_4, 0.5236F, 0.0F, 0.0F);
        dummy_aa_4.setTextureOffset(430, 367).addBox(-4.3F, 0.0F, 25.4F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        dummy_aa_2 = new LightModelRenderer(this);
        dummy_aa_2.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_1.addChild(dummy_aa_2);
        setRotationAngle(dummy_aa_2, 0.5236F, 0.0F, 0.0F);
        dummy_aa_2.setTextureOffset(430, 367).addBox(0.3F, 0.0F, 29.4F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        dummy_aa_5 = new LightModelRenderer(this);
        dummy_aa_5.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_1.addChild(dummy_aa_5);
        setRotationAngle(dummy_aa_5, 0.5236F, 0.0F, 0.0F);
        dummy_aa_5.setTextureOffset(430, 367).addBox(0.3F, 0.0F, 25.4F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        dummy_aa_3 = new LightModelRenderer(this);
        dummy_aa_3.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_1.addChild(dummy_aa_3);
        setRotationAngle(dummy_aa_3, 0.5236F, 0.0F, 0.0F);
        dummy_aa_3.setTextureOffset(430, 367).addBox(5.0F, 0.0F, 29.4F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        dummy_aa_6 = new LightModelRenderer(this);
        dummy_aa_6.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_1.addChild(dummy_aa_6);
        setRotationAngle(dummy_aa_6, 0.5236F, 0.0F, 0.0F);
        dummy_aa_6.setTextureOffset(430, 367).addBox(5.1F, 0.0F, 25.4F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        dummy_aa_7 = new ModelRenderer(this);
        dummy_aa_7.setRotationPoint(-12.0F, -62.0F, -54.0F);
        controls_1.addChild(dummy_aa_7);
        setRotationAngle(dummy_aa_7, 0.5236F, 0.0F, 0.0F);
        dummy_aa_7.setTextureOffset(68, 397).addBox(-2.0F, 0.0F, 2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        toggle_aa_7 = new ModelRenderer(this);
        toggle_aa_7.setRotationPoint(0.0F, 1.0F, 4.0F);
        dummy_aa_7.addChild(toggle_aa_7);
        setRotationAngle(toggle_aa_7, -0.5236F, 0.0F, 0.0F);
        toggle_aa_7.setTextureOffset(102, 232).addBox(-1.0F, -1.7F, -1.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        toggle_aa_7.setTextureOffset(102, 232).addBox(-1.0F, -3.7F, -0.9F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        dummy_aa_8 = new ModelRenderer(this);
        dummy_aa_8.setRotationPoint(-7.0F, -62.0F, -54.0F);
        controls_1.addChild(dummy_aa_8);
        setRotationAngle(dummy_aa_8, 0.5236F, 0.0F, 0.0F);
        dummy_aa_8.setTextureOffset(68, 397).addBox(-2.0F, 0.0F, 2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        toggle_aa_2 = new ModelRenderer(this);
        toggle_aa_2.setRotationPoint(0.0F, 1.0F, 4.0F);
        dummy_aa_8.addChild(toggle_aa_2);
        setRotationAngle(toggle_aa_2, -0.5236F, 0.0F, 0.0F);
        toggle_aa_2.setTextureOffset(111, 300).addBox(-1.0F, -1.7F, -1.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        toggle_aa_2.setTextureOffset(111, 300).addBox(-1.0F, -3.7F, -0.9F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        dummy_aa_9 = new ModelRenderer(this);
        dummy_aa_9.setRotationPoint(-2.0F, -62.0F, -54.0F);
        controls_1.addChild(dummy_aa_9);
        setRotationAngle(dummy_aa_9, 0.5236F, 0.0F, 0.0F);
        dummy_aa_9.setTextureOffset(68, 397).addBox(-2.0F, 0.0F, 2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        toggle_aa_3 = new ModelRenderer(this);
        toggle_aa_3.setRotationPoint(0.0F, 1.0F, 4.0F);
        dummy_aa_9.addChild(toggle_aa_3);
        setRotationAngle(toggle_aa_3, -0.5236F, 0.0F, 0.0F);
        toggle_aa_3.setTextureOffset(71, 265).addBox(-1.0F, -1.7F, -1.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        toggle_aa_3.setTextureOffset(71, 265).addBox(-1.0F, -3.7F, -0.9F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        dummy_aa_10 = new ModelRenderer(this);
        dummy_aa_10.setRotationPoint(3.0F, -62.0F, -54.0F);
        controls_1.addChild(dummy_aa_10);
        setRotationAngle(dummy_aa_10, 0.5236F, 0.0F, 0.0F);
        dummy_aa_10.setTextureOffset(68, 397).addBox(-2.0F, 0.0F, 2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        toggle_aa_4 = new ModelRenderer(this);
        toggle_aa_4.setRotationPoint(0.0F, 1.0F, 4.0F);
        dummy_aa_10.addChild(toggle_aa_4);
        setRotationAngle(toggle_aa_4, -0.5236F, 0.0F, 0.0F);
        toggle_aa_4.setTextureOffset(70, 291).addBox(-1.0F, -1.7F, -1.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        toggle_aa_4.setTextureOffset(70, 291).addBox(-1.0F, -3.7F, -0.9F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        dummy_aa_11 = new ModelRenderer(this);
        dummy_aa_11.setRotationPoint(8.0F, -62.0F, -54.0F);
        controls_1.addChild(dummy_aa_11);
        setRotationAngle(dummy_aa_11, 0.5236F, 0.0F, 0.0F);
        dummy_aa_11.setTextureOffset(68, 397).addBox(-2.0F, 0.0F, 2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        toggle_aa_5 = new ModelRenderer(this);
        toggle_aa_5.setRotationPoint(0.0F, 1.0F, 4.0F);
        dummy_aa_11.addChild(toggle_aa_5);
        setRotationAngle(toggle_aa_5, -0.5236F, 0.0F, 0.0F);
        toggle_aa_5.setTextureOffset(103, 276).addBox(-1.0F, -1.7F, -1.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        toggle_aa_5.setTextureOffset(103, 276).addBox(-1.0F, -3.7F, -0.9F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        decoration_1 = new ModelRenderer(this);
        decoration_1.setRotationPoint(-18.0F, -62.0F, -54.0F);
        controls_1.addChild(decoration_1);
        setRotationAngle(decoration_1, 0.5236F, 0.0F, 0.0F);
        decoration_1.setTextureOffset(105, 291).addBox(10.0F, 0.4F, 34.7F, 16.0F, 4.0F, 1.0F, 0.0F, false);
        decoration_1.setTextureOffset(105, 291).addBox(10.0F, 0.4F, 24.7F, 16.0F, 4.0F, 1.0F, 0.0F, false);
        decoration_1.setTextureOffset(105, 291).addBox(10.0F, 0.4F, 25.7F, 1.0F, 4.0F, 9.0F, 0.0F, false);
        decoration_1.setTextureOffset(105, 291).addBox(25.0F, 0.4F, 25.7F, 1.0F, 4.0F, 9.0F, 0.0F, false);

        controls_2 = new ModelRenderer(this);
        controls_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(controls_2);
        setRotationAngle(controls_2, 0.0F, 1.0472F, 0.0F);
        

        stablizers = new ModelRenderer(this);
        stablizers.setRotationPoint(-18.0F, -62.0F, -54.0F);
        controls_2.addChild(stablizers);
        setRotationAngle(stablizers, 0.5236F, 0.0F, 0.0F);
        stablizers.setTextureOffset(68, 397).addBox(29.0F, 0.0F, 3.0F, 8.0F, 4.0F, 4.0F, 0.0F, false);
        stablizers.setTextureOffset(168, 251).addBox(28.5F, 0.3F, 2.5F, 9.0F, 4.0F, 5.0F, 0.0F, false);

        stablizer_button_rotate_x = new ModelRenderer(this);
        stablizer_button_rotate_x.setRotationPoint(33.0F, 0.5F, -33.0F);
        stablizers.addChild(stablizer_button_rotate_x);
        setRotationAngle(stablizer_button_rotate_x, 0.0873F, 0.0F, 0.0F);
        stablizer_button_rotate_x.setTextureOffset(369, 79).addBox(-3.0F, 0.25F, 37.0F, 2.0F, 5.0F, 2.0F, 0.0F, false);
        stablizer_button_rotate_x.setTextureOffset(369, 79).addBox(1.0F, 0.25F, 37.0F, 2.0F, 5.0F, 2.0F, 0.0F, false);
        stablizer_button_rotate_x.setTextureOffset(472, 18).addBox(-3.5F, -0.5F, 37.5F, 7.0F, 5.0F, 1.0F, 0.0F, false);

        telpathic_circuits = new ModelRenderer(this);
        telpathic_circuits.setRotationPoint(-1.0F, 0.0F, 0.0F);
        controls_2.addChild(telpathic_circuits);
        

        tp_screen = new ModelRenderer(this);
        tp_screen.setRotationPoint(-0.4F, -80.8F, -41.2F);
        telpathic_circuits.addChild(tp_screen);
        setRotationAngle(tp_screen, -1.0472F, 0.0F, 0.0F);
        tp_screen.setTextureOffset(203, 263).addBox(-10.2F, -4.0F, 9.2F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        tp_screen.setTextureOffset(41, 288).addBox(10.8F, -4.0F, 9.2F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        tp_screen.setTextureOffset(11, 373).addBox(-9.7F, -3.6F, 9.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        tp_screen.setTextureOffset(11, 373).addBox(-6.7F, -6.6F, 9.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        tp_screen.setTextureOffset(11, 373).addBox(-6.7F, -0.6F, 9.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        tp_screen.setTextureOffset(11, 373).addBox(-3.7F, -3.6F, 9.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        tp_screen.setTextureOffset(11, 373).addBox(-0.7F, -6.6F, 9.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        tp_screen.setTextureOffset(11, 373).addBox(-0.7F, -0.6F, 9.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        tp_screen.setTextureOffset(11, 373).addBox(2.3F, -3.6F, 9.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        tp_screen.setTextureOffset(11, 373).addBox(5.3F, -6.6F, 9.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        tp_screen.setTextureOffset(11, 373).addBox(5.3F, -0.6F, 9.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        tp_screen.setTextureOffset(11, 373).addBox(8.3F, -3.6F, 9.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        glow_tp_screen = new LightModelRenderer(this);
        glow_tp_screen.setRotationPoint(1.4F, 80.8F, 41.2F);
        tp_screen.addChild(glow_tp_screen);
        glow_tp_screen.setTextureOffset(193, 397).addBox(-10.6F, -86.8F, -32.0F, 20.0F, 8.0F, 4.0F, 0.0F, false);

        tp_frame = new ModelRenderer(this);
        tp_frame.setRotationPoint(0.4F, 80.8F, 41.2F);
        tp_screen.addChild(tp_frame);
        tp_frame.setTextureOffset(203, 263).addBox(-11.6F, -85.2F, -33.0F, 1.0F, 5.0F, 4.0F, 0.0F, false);
        tp_frame.setTextureOffset(203, 263).addBox(-10.6F, -80.8F, -33.0F, 1.0F, 3.0F, 4.0F, 0.0F, false);
        tp_frame.setTextureOffset(203, 263).addBox(11.4F, -85.2F, -33.0F, 1.0F, 5.0F, 4.0F, 0.0F, false);
        tp_frame.setTextureOffset(203, 263).addBox(-9.6F, -87.8F, -33.0F, 20.0F, 1.0F, 4.0F, 0.0F, false);
        tp_frame.setTextureOffset(203, 263).addBox(-4.3F, -93.8F, -30.8F, 1.0F, 5.0F, 4.0F, 0.0F, false);
        tp_frame.setTextureOffset(203, 263).addBox(3.7F, -93.8F, -30.8F, 1.0F, 5.0F, 4.0F, 0.0F, false);
        tp_frame.setTextureOffset(203, 263).addBox(-5.3F, -93.4F, -30.8F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        tp_frame.setTextureOffset(203, 263).addBox(4.7F, -93.4F, -30.8F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        tp_frame.setTextureOffset(203, 263).addBox(-7.3F, -92.4F, -30.8F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        tp_frame.setTextureOffset(203, 263).addBox(5.7F, -92.4F, -30.8F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        tp_frame.setTextureOffset(203, 263).addBox(-3.3F, -94.6F, -30.8F, 7.0F, 7.0F, 4.0F, 0.0F, false);
        tp_frame.setTextureOffset(203, 263).addBox(-9.6F, -78.8F, -33.0F, 20.0F, 1.0F, 4.0F, 0.0F, false);
        tp_frame.setTextureOffset(203, 263).addBox(-10.6F, -87.8F, -33.0F, 1.0F, 3.0F, 4.0F, 0.0F, false);
        tp_frame.setTextureOffset(203, 263).addBox(10.4F, -87.8F, -33.0F, 1.0F, 3.0F, 4.0F, 0.0F, false);
        tp_frame.setTextureOffset(203, 263).addBox(10.4F, -80.8F, -33.0F, 1.0F, 3.0F, 4.0F, 0.0F, false);

        eye = new ModelRenderer(this);
        eye.setRotationPoint(0.0F, 0.0F, 0.0F);
        tp_frame.addChild(eye);
        eye.setTextureOffset(6, 349).addBox(-1.3F, -92.8F, -32.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        glow_eye = new LightModelRenderer(this);
        glow_eye.setRotationPoint(0.0F, 0.0F, 0.0F);
        eye.addChild(glow_eye);
        glow_eye.setTextureOffset(78, 416).addBox(-4.3F, -92.9F, -31.1F, 9.0F, 3.0F, 4.0F, 0.0F, false);
        glow_eye.setTextureOffset(206, 405).addBox(-2.3F, -93.8F, -31.8F, 5.0F, 5.0F, 4.0F, 0.0F, false);
        glow_eye.setTextureOffset(72, 407).addBox(-3.3F, -93.3F, -31.4F, 7.0F, 4.0F, 4.0F, 0.0F, false);

        door = new ModelRenderer(this);
        door.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_2.addChild(door);
        

        keyhole_base = new ModelRenderer(this);
        keyhole_base.setRotationPoint(-17.0F, -62.0F, -50.0F);
        door.addChild(keyhole_base);
        setRotationAngle(keyhole_base, 0.5236F, 0.0F, 0.0F);
        keyhole_base.setTextureOffset(68, 397).addBox(-2.0F, -2.0F, -1.0F, 1.0F, 4.0F, 6.0F, 0.0F, false);
        keyhole_base.setTextureOffset(68, 397).addBox(1.0F, -2.0F, -1.0F, 1.0F, 4.0F, 6.0F, 0.0F, false);
        keyhole_base.setTextureOffset(68, 397).addBox(-1.0F, -2.0F, -2.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        keyhole_base.setTextureOffset(68, 397).addBox(-1.0F, -2.0F, 4.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        keyhole_base.setTextureOffset(68, 397).addBox(-1.0F, -2.0F, 0.0F, 1.0F, 4.0F, 2.0F, 0.0F, false);

        key_rotate_y = new ModelRenderer(this);
        key_rotate_y.setRotationPoint(0.0F, -3.2641F, 3.1464F);
        keyhole_base.addChild(key_rotate_y);
        setRotationAngle(key_rotate_y, 0.0F, 0.6981F, 0.0F);
        key_rotate_y.setTextureOffset(263, 124).addBox(-0.5F, -1.0F, -0.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);
        key_rotate_y.setTextureOffset(263, 124).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        key_rotate_y.setTextureOffset(263, 124).addBox(-1.5F, -2.0F, -0.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);
        key_rotate_y.setTextureOffset(263, 124).addBox(-1.5F, -4.0F, -0.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);
        key_rotate_y.setTextureOffset(263, 124).addBox(-0.5F, -5.0F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        key_rotate_y.setTextureOffset(263, 124).addBox(-1.5F, -3.0F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        key_rotate_y.setTextureOffset(263, 124).addBox(0.5F, -3.0F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        dummy_b_1 = new ModelRenderer(this);
        dummy_b_1.setRotationPoint(-6.4F, -61.2F, -55.6F);
        controls_2.addChild(dummy_b_1);
        setRotationAngle(dummy_b_1, 0.5236F, 0.0F, 0.0F);
        

        toggle_b_1_rotate_x = new ModelRenderer(this);
        toggle_b_1_rotate_x.setRotationPoint(0.0F, 1.0F, 6.6F);
        dummy_b_1.addChild(toggle_b_1_rotate_x);
        setRotationAngle(toggle_b_1_rotate_x, -0.2618F, 0.0F, 0.0F);
        

        togglebottom_b1 = new ModelRenderer(this);
        togglebottom_b1.setRotationPoint(0.0F, 1.0F, -0.6F);
        toggle_b_1_rotate_x.addChild(togglebottom_b1);
        setRotationAngle(togglebottom_b1, -0.3491F, 0.0F, 0.0F);
        togglebottom_b1.setTextureOffset(68, 397).addBox(-1.0F, -1.6F, -2.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        toggletop_b1 = new ModelRenderer(this);
        toggletop_b1.setRotationPoint(0.0F, 1.0F, -0.6F);
        toggle_b_1_rotate_x.addChild(toggletop_b1);
        setRotationAngle(toggletop_b1, 0.2618F, 0.0F, 0.0F);
        toggletop_b1.setTextureOffset(68, 397).addBox(-1.0F, -1.6F, -1.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        dummy_b_2 = new ModelRenderer(this);
        dummy_b_2.setRotationPoint(-2.4F, -61.2F, -55.6F);
        controls_2.addChild(dummy_b_2);
        setRotationAngle(dummy_b_2, 0.5236F, 0.0F, 0.0F);
        

        toggle_b2_rotate_x = new ModelRenderer(this);
        toggle_b2_rotate_x.setRotationPoint(0.0F, 1.0F, 6.6F);
        dummy_b_2.addChild(toggle_b2_rotate_x);
        setRotationAngle(toggle_b2_rotate_x, -0.2618F, 0.0F, 0.0F);
        

        togglebottom_b2 = new ModelRenderer(this);
        togglebottom_b2.setRotationPoint(0.0F, 1.0F, -0.6F);
        toggle_b2_rotate_x.addChild(togglebottom_b2);
        setRotationAngle(togglebottom_b2, -0.3491F, 0.0F, 0.0F);
        togglebottom_b2.setTextureOffset(68, 397).addBox(-1.0F, -1.6F, -2.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        toggletop_b2 = new ModelRenderer(this);
        toggletop_b2.setRotationPoint(0.0F, 1.0F, -0.6F);
        toggle_b2_rotate_x.addChild(toggletop_b2);
        setRotationAngle(toggletop_b2, 0.2618F, 0.0F, 0.0F);
        toggletop_b2.setTextureOffset(68, 397).addBox(-1.0F, -1.6F, -1.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        dummy_b_3 = new ModelRenderer(this);
        dummy_b_3.setRotationPoint(1.6F, -61.2F, -55.6F);
        controls_2.addChild(dummy_b_3);
        setRotationAngle(dummy_b_3, 0.5236F, 0.0F, 0.0F);
        

        toggle_b3_rotate_x = new ModelRenderer(this);
        toggle_b3_rotate_x.setRotationPoint(0.0F, 1.0F, 6.6F);
        dummy_b_3.addChild(toggle_b3_rotate_x);
        setRotationAngle(toggle_b3_rotate_x, 0.2618F, 0.0F, 0.0F);
        

        togglebottom_b3 = new ModelRenderer(this);
        togglebottom_b3.setRotationPoint(0.0F, 1.0F, -0.6F);
        toggle_b3_rotate_x.addChild(togglebottom_b3);
        setRotationAngle(togglebottom_b3, -0.3491F, 0.0F, 0.0F);
        togglebottom_b3.setTextureOffset(68, 397).addBox(-1.0F, -1.6F, -2.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        toggletop_b3 = new ModelRenderer(this);
        toggletop_b3.setRotationPoint(0.0F, 1.0F, -0.6F);
        toggle_b3_rotate_x.addChild(toggletop_b3);
        setRotationAngle(toggletop_b3, 0.2618F, 0.0F, 0.0F);
        toggletop_b3.setTextureOffset(68, 397).addBox(-1.0F, -1.6F, -1.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        dummy_b_4 = new ModelRenderer(this);
        dummy_b_4.setRotationPoint(5.6F, -61.2F, -55.6F);
        controls_2.addChild(dummy_b_4);
        setRotationAngle(dummy_b_4, 0.5236F, 0.0F, 0.0F);
        

        toggle_b4_rotate_x = new ModelRenderer(this);
        toggle_b4_rotate_x.setRotationPoint(0.0F, 1.0F, 6.6F);
        dummy_b_4.addChild(toggle_b4_rotate_x);
        setRotationAngle(toggle_b4_rotate_x, -0.2618F, 0.0F, 0.0F);
        

        togglebottom_b4 = new ModelRenderer(this);
        togglebottom_b4.setRotationPoint(0.0F, 1.0F, -0.6F);
        toggle_b4_rotate_x.addChild(togglebottom_b4);
        setRotationAngle(togglebottom_b4, -0.3491F, 0.0F, 0.0F);
        togglebottom_b4.setTextureOffset(68, 397).addBox(-1.0F, -1.6F, -2.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        toggletop_b4 = new ModelRenderer(this);
        toggletop_b4.setRotationPoint(0.0F, 1.0F, -0.6F);
        toggle_b4_rotate_x.addChild(toggletop_b4);
        setRotationAngle(toggletop_b4, 0.2618F, 0.0F, 0.0F);
        toggletop_b4.setTextureOffset(68, 397).addBox(-1.0F, -1.6F, -1.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        dummy_b_5 = new ModelRenderer(this);
        dummy_b_5.setRotationPoint(-18.0F, -62.0F, -54.0F);
        controls_2.addChild(dummy_b_5);
        setRotationAngle(dummy_b_5, 0.5236F, 0.0F, 0.0F);
        dummy_b_5.setTextureOffset(107, 284).addBox(9.0F, 0.0F, 11.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        dummy_b_6 = new ModelRenderer(this);
        dummy_b_6.setRotationPoint(-18.0F, -62.0F, -54.0F);
        controls_2.addChild(dummy_b_6);
        setRotationAngle(dummy_b_6, 0.5236F, 0.0F, 0.0F);
        dummy_b_6.setTextureOffset(122, 279).addBox(16.0F, 0.0F, 11.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        dummy_b_7 = new ModelRenderer(this);
        dummy_b_7.setRotationPoint(-18.0F, -62.0F, -54.0F);
        controls_2.addChild(dummy_b_7);
        setRotationAngle(dummy_b_7, 0.5236F, 0.0F, 0.0F);
        dummy_b_7.setTextureOffset(134, 266).addBox(23.0F, 0.0F, 11.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        decoration_2 = new ModelRenderer(this);
        decoration_2.setRotationPoint(-18.0F, -62.0F, -54.0F);
        controls_2.addChild(decoration_2);
        setRotationAngle(decoration_2, 0.5236F, 0.0F, 0.0F);
        decoration_2.setTextureOffset(227, 274).addBox(9.6F, 0.4F, 8.0F, 16.0F, 4.0F, 1.0F, 0.0F, false);
        decoration_2.setTextureOffset(227, 274).addBox(9.6F, 0.4F, 0.0F, 16.0F, 4.0F, 1.0F, 0.0F, false);
        decoration_2.setTextureOffset(227, 274).addBox(8.6F, 0.4F, 0.0F, 1.0F, 4.0F, 9.0F, 0.0F, false);
        decoration_2.setTextureOffset(227, 274).addBox(-0.5F, 0.4F, 1.0F, 3.0F, 4.0F, 9.0F, 0.0F, false);
        decoration_2.setTextureOffset(227, 274).addBox(2.5F, 0.4F, 2.0F, 1.0F, 4.0F, 7.0F, 0.0F, false);
        decoration_2.setTextureOffset(227, 274).addBox(-1.5F, 0.4F, 2.0F, 1.0F, 4.0F, 7.0F, 0.0F, false);
        decoration_2.setTextureOffset(227, 274).addBox(25.6F, 0.4F, 0.0F, 1.0F, 4.0F, 9.0F, 0.0F, false);

        controls_3 = new ModelRenderer(this);
        controls_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(controls_3);
        setRotationAngle(controls_3, 0.0F, 2.0944F, 0.0F);
        

        refueler = new ModelRenderer(this);
        refueler.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_3.addChild(refueler);
        

        dial_base = new ModelRenderer(this);
        dial_base.setRotationPoint(15.0F, -63.0F, -49.0F);
        refueler.addChild(dial_base);
        setRotationAngle(dial_base, 0.6109F, 0.0F, 0.0F);
        dial_base.setTextureOffset(128, 277).addBox(-4.0F, -2.0F, -4.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);

        round = new LightModelRenderer(this);
        round.setRotationPoint(-15.0F, 63.0F, 49.0F);
        dial_base.addChild(round);
        round.setTextureOffset(49, 424).addBox(12.5F, -65.4F, -52.6F, 5.0F, 4.0F, 7.0F, 0.0F, false);
        round.setTextureOffset(57, 437).addBox(11.5F, -65.4F, -51.6F, 1.0F, 4.0F, 5.0F, 0.0F, false);
        round.setTextureOffset(87, 416).addBox(17.5F, -65.4F, -51.6F, 1.0F, 4.0F, 5.0F, 0.0F, false);

        curve = new ModelRenderer(this);
        curve.setRotationPoint(-15.0F, 63.0F, 49.0F);
        dial_base.addChild(curve);
        curve.setTextureOffset(128, 277).addBox(17.0F, -66.0F, -52.9F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        curve.setTextureOffset(128, 277).addBox(13.0F, -66.0F, -52.9F, 4.0F, 4.0F, 2.0F, 0.0F, false);
        curve.setTextureOffset(128, 277).addBox(12.0F, -66.0F, -52.9F, 1.0F, 4.0F, 1.0F, 0.0F, false);

        pointer_rotate_y = new ModelRenderer(this);
        pointer_rotate_y.setRotationPoint(0.1F, -0.8F, -2.7F);
        dial_base.addChild(pointer_rotate_y);
        setRotationAngle(pointer_rotate_y, 0.0F, -0.7418F, 0.0F);
        pointer_rotate_y.setTextureOffset(4, 348).addBox(-0.5F, -2.0F, -0.9F, 1.0F, 4.0F, 5.0F, 0.0F, false);

        sonic_port = new ModelRenderer(this);
        sonic_port.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_3.addChild(sonic_port);
        

        sonic_base = new ModelRenderer(this);
        sonic_base.setRotationPoint(-2.0F, -70.0F, -40.0F);
        sonic_port.addChild(sonic_base);
        setRotationAngle(sonic_base, 0.5236F, 0.0F, 0.0F);
        sonic_base.setTextureOffset(68, 397).addBox(-2.0F, -0.4F, 4.0F, 2.0F, 4.0F, 8.0F, 0.0F, false);
        sonic_base.setTextureOffset(68, 397).addBox(-4.0F, -0.4F, 5.0F, 2.0F, 4.0F, 6.0F, 0.0F, false);
        sonic_base.setTextureOffset(68, 397).addBox(6.0F, -0.4F, 5.0F, 2.0F, 4.0F, 6.0F, 0.0F, false);
        sonic_base.setTextureOffset(68, 397).addBox(-7.0F, -0.4F, 7.0F, 3.0F, 4.0F, 2.0F, 0.0F, false);
        sonic_base.setTextureOffset(68, 397).addBox(8.0F, -0.4F, 7.0F, 3.0F, 4.0F, 2.0F, 0.0F, false);
        sonic_base.setTextureOffset(68, 397).addBox(4.0F, -0.4F, 4.0F, 2.0F, 4.0F, 8.0F, 0.0F, false);
        sonic_base.setTextureOffset(68, 397).addBox(0.0F, -0.4F, 10.0F, 4.0F, 4.0F, 2.0F, 0.0F, false);
        sonic_base.setTextureOffset(68, 397).addBox(0.0F, -0.4F, 4.0F, 4.0F, 4.0F, 2.0F, 0.0F, false);
        sonic_base.setTextureOffset(4, 348).addBox(0.0F, 0.0F, 6.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        randomizer = new ModelRenderer(this);
        randomizer.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_3.addChild(randomizer);
        
        rando_plate = new ModelRenderer(this);
        rando_plate.setRotationPoint(-7.0F, -32.0F, -25.0F);
        randomizer.addChild(rando_plate);
        setRotationAngle(rando_plate, 0.5236F, 0.0F, 0.0F);
        rando_plate.setTextureOffset(68, 397).addBox(-12.0F, -41.0F, -9.4F, 8.0F, 4.0F, 8.0F, 0.0F, false);
        rando_plate.setTextureOffset(149, 10).addBox(-10.0F, -42.0F, -7.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        
        glow_randomizer = new LightModelRenderer(this);
        glow_randomizer.setRotationPoint(7.0F, 32.0F, 25.0F);
        rando_plate.addChild(glow_randomizer);
        glow_randomizer.setTextureOffset(149, 10).addBox(-17.0F, -74.0F, -32.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        
        diamond = new ModelRenderer(this);
        diamond.setRotationPoint(-8.0F, -39.0F, -5.4F);
        rando_plate.addChild(diamond);
        setRotationAngle(diamond, 0.0F, -0.7854F, 0.0F);
        diamond.setTextureOffset(74, 294).addBox(-4.0F, -1.6F, -4.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);

        dummy_c_1 = new ModelRenderer(this);
        dummy_c_1.setRotationPoint(-5.0F, -77.0F, -29.0F);
        controls_3.addChild(dummy_c_1);
        setRotationAngle(dummy_c_1, 0.5236F, 0.0F, 0.0F);
        dummy_c_1.setTextureOffset(68, 397).addBox(-2.0F, 0.0F, 2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        toggle_c_1 = new ModelRenderer(this);
        toggle_c_1.setRotationPoint(0.0F, 1.0F, 4.0F);
        dummy_c_1.addChild(toggle_c_1);
        setRotationAngle(toggle_c_1, -0.5236F, 0.0F, 0.0F);
        toggle_c_1.setTextureOffset(117, 241).addBox(-1.0F, -1.7F, -1.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        toggle_c_1.setTextureOffset(117, 241).addBox(-1.0F, -3.7F, -0.9F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        dummy_c_2 = new ModelRenderer(this);
        dummy_c_2.setRotationPoint(0.0F, -77.0F, -29.0F);
        controls_3.addChild(dummy_c_2);
        setRotationAngle(dummy_c_2, 0.5236F, 0.0F, 0.0F);
        dummy_c_2.setTextureOffset(68, 397).addBox(-2.0F, 0.0F, 2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        toggle_c_2 = new ModelRenderer(this);
        toggle_c_2.setRotationPoint(0.0F, 1.0F, 4.0F);
        dummy_c_2.addChild(toggle_c_2);
        setRotationAngle(toggle_c_2, -0.5236F, 0.0F, 0.0F);
        toggle_c_2.setTextureOffset(189, 253).addBox(-1.0F, -1.7F, -1.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        toggle_c_2.setTextureOffset(189, 253).addBox(-1.0F, -3.7F, -0.9F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        dummy_c_3 = new ModelRenderer(this);
        dummy_c_3.setRotationPoint(5.0F, -77.0F, -29.0F);
        controls_3.addChild(dummy_c_3);
        setRotationAngle(dummy_c_3, 0.5236F, 0.0F, 0.0F);
        dummy_c_3.setTextureOffset(68, 397).addBox(-2.0F, 0.0F, 2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        toggle_c_3 = new ModelRenderer(this);
        toggle_c_3.setRotationPoint(0.0F, 1.0F, 4.0F);
        dummy_c_3.addChild(toggle_c_3);
        setRotationAngle(toggle_c_3, -0.5236F, 0.0F, 0.0F);
        toggle_c_3.setTextureOffset(157, 245).addBox(-1.0F, -1.7F, -1.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        toggle_c_3.setTextureOffset(157, 245).addBox(-1.0F, -3.7F, -0.9F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        dummy_c_4 = new ModelRenderer(this);
        dummy_c_4.setRotationPoint(-7.0F, -67.0F, -46.0F);
        controls_3.addChild(dummy_c_4);
        setRotationAngle(dummy_c_4, 0.5236F, 0.0F, 0.0F);
        dummy_c_4.setTextureOffset(68, 397).addBox(-2.0F, 0.0F, 2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        toggle_c_4 = new ModelRenderer(this);
        toggle_c_4.setRotationPoint(0.0F, 1.0F, 4.0F);
        dummy_c_4.addChild(toggle_c_4);
        setRotationAngle(toggle_c_4, -0.5236F, 0.0F, 0.0F);
        toggle_c_4.setTextureOffset(123, 281).addBox(-1.0F, -1.7F, -1.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        toggle_c_4.setTextureOffset(123, 281).addBox(-1.0F, -3.7F, -0.9F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        dummy_c_5 = new ModelRenderer(this);
        dummy_c_5.setRotationPoint(0.0F, -67.0F, -46.0F);
        controls_3.addChild(dummy_c_5);
        setRotationAngle(dummy_c_5, 0.5236F, 0.0F, 0.0F);
        dummy_c_5.setTextureOffset(68, 397).addBox(-2.0F, 0.0F, 2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        toggle_c_5 = new ModelRenderer(this);
        toggle_c_5.setRotationPoint(0.0F, 1.0F, 4.0F);
        dummy_c_5.addChild(toggle_c_5);
        setRotationAngle(toggle_c_5, -0.5236F, 0.0F, 0.0F);
        toggle_c_5.setTextureOffset(100, 253).addBox(-1.0F, -1.7F, -1.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        toggle_c_5.setTextureOffset(100, 253).addBox(-1.0F, -3.7F, -0.9F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        dummy_c_6 = new ModelRenderer(this);
        dummy_c_6.setRotationPoint(7.0F, -67.0F, -46.0F);
        controls_3.addChild(dummy_c_6);
        setRotationAngle(dummy_c_6, 0.5236F, 0.0F, 0.0F);
        dummy_c_6.setTextureOffset(68, 397).addBox(-2.0F, 0.0F, 2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        toggle_c_6 = new ModelRenderer(this);
        toggle_c_6.setRotationPoint(0.0F, 1.0F, 4.0F);
        dummy_c_6.addChild(toggle_c_6);
        setRotationAngle(toggle_c_6, -0.5236F, 0.0F, 0.0F);
        toggle_c_6.setTextureOffset(121, 258).addBox(-1.0F, -1.7F, -1.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        toggle_c_6.setTextureOffset(121, 258).addBox(-1.0F, -3.7F, -0.9F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        dummy_c_7 = new ModelRenderer(this);
        dummy_c_7.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_3.addChild(dummy_c_7);
        setRotationAngle(dummy_c_7, 0.5236F, 0.0F, 0.0F);
        dummy_c_7.setTextureOffset(68, 397).addBox(-2.5F, 0.0F, 3.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        dummy_c_9 = new ModelRenderer(this);
        dummy_c_9.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_3.addChild(dummy_c_9);
        setRotationAngle(dummy_c_9, 0.5236F, 0.0F, 0.0F);
        dummy_c_9.setTextureOffset(78, 398).addBox(-2.5F, 0.0F, 0.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_c_9.setTextureOffset(64, 416).addBox(-0.5F, 0.0F, -2.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);

        glow_c9 = new LightModelRenderer(this);
        glow_c9.setRotationPoint(-16.0F, 62.0F, 54.0F);
        dummy_c_9.addChild(glow_c9);
        glow_c9.setTextureOffset(241, 413).addBox(13.5F, -62.0F, -56.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        glow_c9.setTextureOffset(253, 405).addBox(15.5F, -62.0F, -54.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);

        dummy_c_8 = new ModelRenderer(this);
        dummy_c_8.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_3.addChild(dummy_c_8);
        setRotationAngle(dummy_c_8, 0.5236F, 0.0F, 0.0F);
        dummy_c_8.setTextureOffset(113, 416).addBox(4.4F, 0.0F, 5.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_c_8.setTextureOffset(74, 409).addBox(2.4F, 0.0F, 3.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);

        glow_c8 = new LightModelRenderer(this);
        glow_c8.setRotationPoint(-16.0F, 62.0F, 54.0F);
        dummy_c_8.addChild(glow_c8);
        glow_c8.setTextureOffset(215, 388).addBox(18.4F, -62.0F, -49.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        glow_c8.setTextureOffset(228, 413).addBox(20.4F, -62.0F, -51.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);

        dummy_c_10 = new ModelRenderer(this);
        dummy_c_10.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_3.addChild(dummy_c_10);
        setRotationAngle(dummy_c_10, 0.5236F, 0.0F, 0.0F);
        dummy_c_10.setTextureOffset(68, 397).addBox(2.4F, 0.0F, -2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        controls_4 = new ModelRenderer(this);
        controls_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(controls_4);
        setRotationAngle(controls_4, 0.0F, 3.1416F, 0.0F);
        

        throttle = new ModelRenderer(this);
        throttle.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_4.addChild(throttle);
        

        throttle_plate = new ModelRenderer(this);
        throttle_plate.setRotationPoint(-17.0F, -62.0F, -50.0F);
        throttle.addChild(throttle_plate);
        setRotationAngle(throttle_plate, 0.5236F, 0.0F, 0.0F);
        throttle_plate.setTextureOffset(298, 87).addBox(29.0F, -2.0F, -3.0F, 10.0F, 4.0F, 5.0F, 0.0F, false);
        throttle_plate.setTextureOffset(68, 397).addBox(29.4F, -2.4F, -2.4F, 9.0F, 4.0F, 4.0F, 0.0F, false);
        throttle_plate.setTextureOffset(81, 283).addBox(30.5F, -3.7641F, -2.0536F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        throttle_plate.setTextureOffset(117, 268).addBox(36.5F, -3.7641F, -2.0536F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        throttle_plate.setTextureOffset(202, 279).addBox(32.5F, -3.7641F, -2.0536F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        throttle_plate.setTextureOffset(87, 247).addBox(31.5F, -3.3641F, -1.5536F, 5.0F, 3.0F, 2.0F, 0.0F, false);

        throttle_rotate_x = new ModelRenderer(this);
        throttle_rotate_x.setRotationPoint(34.0F, -2.25F, 0.3F);
        throttle_plate.addChild(throttle_rotate_x);
        throttle_rotate_x.setTextureOffset(307, 96).addBox(1.5F, -8.0141F, -1.4536F, 1.0F, 9.0F, 1.0F, 0.0F, false);
        throttle_rotate_x.setTextureOffset(311, 96).addBox(-2.5F, -8.0141F, -1.4536F, 1.0F, 9.0F, 1.0F, 0.0F, false);
        throttle_rotate_x.setTextureOffset(115, 251).addBox(2.8F, -10.6141F, -2.5536F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        throttle_rotate_x.setTextureOffset(115, 251).addBox(1.6F, -10.6141F, -2.5536F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        throttle_rotate_x.setTextureOffset(115, 251).addBox(0.4F, -10.6141F, -2.5536F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        throttle_rotate_x.setTextureOffset(115, 251).addBox(-0.8F, -10.6141F, -2.5536F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        throttle_rotate_x.setTextureOffset(115, 251).addBox(-2.0F, -10.6141F, -2.5536F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        throttle_rotate_x.setTextureOffset(115, 251).addBox(-3.2F, -10.6141F, -2.5536F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        throttle_rotate_x.setTextureOffset(115, 251).addBox(-4.4F, -10.6141F, -2.5536F, 1.0F, 3.0F, 3.0F, 0.0F, false);
        throttle_rotate_x.setTextureOffset(59, 428).addBox(-4.8F, -10.0141F, -1.9536F, 9.0F, 2.0F, 2.0F, 0.0F, false);
        throttle_rotate_x.setTextureOffset(136, 19).addBox(-5.2F, -9.7141F, -1.5536F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        dimentional_con = new ModelRenderer(this);
        dimentional_con.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_4.addChild(dimentional_con);
        

        dim_screen = new ModelRenderer(this);
        dim_screen.setRotationPoint(0.0F, -70.0F, -42.0F);
        dimentional_con.addChild(dim_screen);
        setRotationAngle(dim_screen, 0.9599F, 0.0F, 0.0F);
        dim_screen.setTextureOffset(117, 264).addBox(-4.8F, 5.2F, 6.0F, 1.0F, 9.0F, 10.0F, 0.0F, false);
        dim_screen.setTextureOffset(117, 264).addBox(4.2F, 5.2F, 6.0F, 1.0F, 9.0F, 10.0F, 0.0F, false);
        dim_screen.setTextureOffset(117, 264).addBox(-4.8F, 5.2F, 16.0F, 10.0F, 9.0F, 1.0F, 0.0F, false);

        glow_dim = new LightModelRenderer(this);
        glow_dim.setRotationPoint(0.0F, 70.0F, 42.0F);
        dim_screen.addChild(glow_dim);
        glow_dim.setTextureOffset(224, 401).addBox(-3.8F, -64.4F, -35.0F, 8.0F, 4.0F, 9.0F, 0.0F, false);

        handbreak = new ModelRenderer(this);
        handbreak.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_4.addChild(handbreak);
        

        handbreak_plate = new ModelRenderer(this);
        handbreak_plate.setRotationPoint(-17.0F, -62.0F, -50.0F);
        handbreak.addChild(handbreak_plate);
        setRotationAngle(handbreak_plate, 0.5236F, 0.0F, 0.0F);
        handbreak_plate.setTextureOffset(68, 397).addBox(0.0F, -2.0F, -2.0F, 4.0F, 4.0F, 6.0F, 0.0F, false);
        handbreak_plate.setTextureOffset(68, 397).addBox(0.5F, -5.2641F, -0.3536F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        handbreak_rotate_y = new ModelRenderer(this);
        handbreak_rotate_y.setRotationPoint(2.0F, -6.7641F, 1.5631F);
        handbreak_plate.addChild(handbreak_rotate_y);
        setRotationAngle(handbreak_rotate_y, 0.0F, 0.8727F, 0.0F);
        handbreak_rotate_y.setTextureOffset(258, 100).addBox(-2.0F, -1.5F, -2.5167F, 4.0F, 3.0F, 4.0F, 0.0F, false);
        handbreak_rotate_y.setTextureOffset(68, 397).addBox(-1.0F, -1.0F, -13.5167F, 2.0F, 2.0F, 11.0F, 0.0F, false);
        handbreak_rotate_y.setTextureOffset(151, 19).addBox(-1.5F, -1.5F, -5.9167F, 3.0F, 3.0F, 2.0F, 0.0F, false);
        handbreak_rotate_y.setTextureOffset(119, 14).addBox(-1.5F, -1.5F, -8.2167F, 3.0F, 3.0F, 2.0F, 0.0F, false);
        handbreak_rotate_y.setTextureOffset(157, 25).addBox(-1.5F, -1.5F, -10.5167F, 3.0F, 3.0F, 2.0F, 0.0F, false);
        handbreak_rotate_y.setTextureOffset(149, 27).addBox(-1.5F, -1.5F, -12.8167F, 3.0F, 3.0F, 2.0F, 0.0F, false);

        dummy_d_1 = new LightModelRenderer(this);
        dummy_d_1.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_4.addChild(dummy_d_1);
        setRotationAngle(dummy_d_1, 0.5236F, 0.0F, 0.0F);
        dummy_d_1.setTextureOffset(68, 397).addBox(1.0F, 0.0F, 12.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(-2.0F, 0.0F, 12.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(-5.0F, 0.0F, 12.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(4.0F, 0.0F, 12.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(7.0F, 0.0F, 12.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(10.0F, 0.0F, 12.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(10.0F, 0.0F, 6.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(7.0F, 0.0F, 6.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(4.0F, 0.0F, 6.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(1.0F, 0.0F, 6.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(-2.0F, 0.0F, 6.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(-5.0F, 0.0F, 6.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(-8.0F, 0.0F, 6.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(-8.0F, 0.0F, 12.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(-6.4F, 0.0F, 9.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(-6.4F, 0.0F, 3.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(-3.4F, 0.0F, 9.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(-0.4F, 0.0F, 9.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(2.6F, 0.0F, 9.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(5.6F, 0.0F, 9.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(292, 108).addBox(-3.4F, 0.0F, 3.0F, 11.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(8.6F, 0.0F, 9.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        dummy_d_1.setTextureOffset(68, 397).addBox(8.6F, 0.0F, 3.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);

        dummy_d_2 = new LightModelRenderer(this);
        dummy_d_2.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_4.addChild(dummy_d_2);
        setRotationAngle(dummy_d_2, 0.5236F, 0.0F, 0.0F);
        dummy_d_2.setTextureOffset(68, 397).addBox(-8.8F, 0.0F, 21.0F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        dummy_d_3 = new LightModelRenderer(this);
        dummy_d_3.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_4.addChild(dummy_d_3);
        setRotationAngle(dummy_d_3, 0.5236F, 0.0F, 0.0F);
        dummy_d_3.setTextureOffset(68, 397).addBox(-8.8F, 0.0F, 17.0F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        dummy_d_4 = new LightModelRenderer(this);
        dummy_d_4.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_4.addChild(dummy_d_4);
        setRotationAngle(dummy_d_4, 0.5236F, 0.0F, 0.0F);
        dummy_d_4.setTextureOffset(68, 397).addBox(10.0F, 0.0F, 21.0F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        dummy_d_5 = new LightModelRenderer(this);
        dummy_d_5.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_4.addChild(dummy_d_5);
        setRotationAngle(dummy_d_5, 0.5236F, 0.0F, 0.0F);
        dummy_d_5.setTextureOffset(68, 397).addBox(10.0F, 0.0F, 17.0F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        dummy_d_6 = new ModelRenderer(this);
        dummy_d_6.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_4.addChild(dummy_d_6);
        setRotationAngle(dummy_d_6, 0.5236F, 0.0F, 0.0F);
        dummy_d_6.setTextureOffset(302, 104).addBox(-3.4F, 0.0F, 17.0F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        dummy_d_7 = new ModelRenderer(this);
        dummy_d_7.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_4.addChild(dummy_d_7);
        setRotationAngle(dummy_d_7, 0.5236F, 0.0F, 0.0F);
        dummy_d_7.setTextureOffset(300, 91).addBox(0.6F, 0.0F, 17.0F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        dummy_d_8 = new ModelRenderer(this);
        dummy_d_8.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_4.addChild(dummy_d_8);
        setRotationAngle(dummy_d_8, 0.5236F, 0.0F, 0.0F);
        dummy_d_8.setTextureOffset(309, 89).addBox(4.6F, 0.0F, 17.0F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        decoration_4 = new ModelRenderer(this);
        decoration_4.setRotationPoint(-18.0F, -62.0F, -54.0F);
        controls_4.addChild(decoration_4);
        setRotationAngle(decoration_4, 0.5236F, 0.0F, 0.0F);
        decoration_4.setTextureOffset(138, 241).addBox(12.3F, 0.4F, 22.6F, 12.0F, 4.0F, 12.0F, 0.0F, false);

        controls_5 = new ModelRenderer(this);
        controls_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(controls_5);
        setRotationAngle(controls_5, 0.0F, -2.0944F, 0.0F);
        

        landing_type = new ModelRenderer(this);
        landing_type.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_5.addChild(landing_type);
        

        landing_screen = new ModelRenderer(this);
        landing_screen.setRotationPoint(0.0F, -70.0F, -42.0F);
        landing_type.addChild(landing_screen);
        setRotationAngle(landing_screen, 0.9599F, 0.0F, 0.0F);
        landing_screen.setTextureOffset(4, 339).addBox(8.2F, -3.7F, -11.0F, 1.0F, 9.0F, 9.0F, 0.0F, false);
        landing_screen.setTextureOffset(4, 339).addBox(17.2F, -3.7F, -11.0F, 1.0F, 5.0F, 9.0F, 0.0F, false);
        landing_screen.setTextureOffset(4, 339).addBox(8.2F, -3.7F, -2.0F, 10.0F, 9.0F, 1.0F, 0.0F, false);

        glow_landing = new LightModelRenderer(this);
        glow_landing.setRotationPoint(0.0F, 70.0F, 42.0F);
        landing_screen.addChild(glow_landing);
        glow_landing.setTextureOffset(217, 398).addBox(9.2F, -73.3F, -53.0F, 8.0F, 4.0F, 9.0F, 0.0F, false);

        increments = new ModelRenderer(this);
        increments.setRotationPoint(0.0F, 0.0F, 0.6F);
        controls_5.addChild(increments);
        

        inc_slider = new ModelRenderer(this);
        inc_slider.setRotationPoint(0.0F, -70.0F, -42.0F);
        increments.addChild(inc_slider);
        setRotationAngle(inc_slider, 0.6109F, 0.0F, 0.0F);
        inc_slider.setTextureOffset(68, 397).addBox(-1.4F, -0.3F, -11.0F, 1.0F, 4.0F, 9.0F, 0.0F, false);
        inc_slider.setTextureOffset(68, 397).addBox(0.6F, -0.3F, -11.0F, 1.0F, 4.0F, 9.0F, 0.0F, false);
        inc_slider.setTextureOffset(68, 397).addBox(-1.4F, -0.3F, -2.0F, 3.0F, 4.0F, 1.0F, 0.0F, false);
        inc_slider.setTextureOffset(68, 397).addBox(-1.4F, -0.3F, -12.0F, 3.0F, 4.0F, 1.0F, 0.0F, false);
        inc_slider.setTextureOffset(5, 345).addBox(-0.4F, 0.1F, -11.0F, 1.0F, 4.0F, 9.0F, 0.0F, false);

        position_rotate_x = new ModelRenderer(this);
        position_rotate_x.setRotationPoint(0.1F, 10.2F, -7.5F);
        inc_slider.addChild(position_rotate_x);
        setRotationAngle(position_rotate_x, 0.3054F, 0.0F, 0.0F);
        position_rotate_x.setTextureOffset(209, 54).addBox(-1.5F, -13.5F, -0.5F, 3.0F, 2.0F, 2.0F, 0.0F, false);
        position_rotate_x.setTextureOffset(76, 427).addBox(-0.5F, -11.5F, 0.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);

        xyz = new ModelRenderer(this);
        xyz.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_5.addChild(xyz);
        

        dial_x = new ModelRenderer(this);
        dial_x.setRotationPoint(0.0F, -70.0F, -42.0F);
        xyz.addChild(dial_x);
        setRotationAngle(dial_x, 0.6109F, 0.0F, 0.0F);
        dial_x.setTextureOffset(72, 411).addBox(-8.6F, 1.7F, 7.0F, 5.0F, 4.0F, 5.0F, 0.0F, false);
        dial_x.setTextureOffset(6, 348).addBox(-7.6F, 0.7F, 8.0F, 3.0F, 4.0F, 2.0F, 0.0F, false);
        dial_x.setTextureOffset(6, 348).addBox(-7.1F, 0.7F, 10.0F, 2.0F, 4.0F, 1.0F, 0.0F, false);

        dial_y = new ModelRenderer(this);
        dial_y.setRotationPoint(0.0F, -70.0F, -42.0F);
        xyz.addChild(dial_y);
        setRotationAngle(dial_y, 0.6109F, 0.0F, 0.0F);
        dial_y.setTextureOffset(85, 422).addBox(-2.6F, 1.7F, 7.0F, 5.0F, 4.0F, 5.0F, 0.0F, false);
        dial_y.setTextureOffset(7, 351).addBox(-1.6F, 0.7F, 8.0F, 3.0F, 4.0F, 2.0F, 0.0F, false);
        dial_y.setTextureOffset(7, 351).addBox(-1.1F, 0.7F, 10.0F, 2.0F, 4.0F, 1.0F, 0.0F, false);

        dial_z = new ModelRenderer(this);
        dial_z.setRotationPoint(0.0F, -70.0F, -42.0F);
        xyz.addChild(dial_z);
        setRotationAngle(dial_z, 0.6109F, 0.0F, 0.0F);
        dial_z.setTextureOffset(49, 422).addBox(3.4F, 1.7F, 7.0F, 5.0F, 4.0F, 5.0F, 0.0F, false);
        dial_z.setTextureOffset(5, 351).addBox(4.4F, 0.7F, 8.0F, 3.0F, 4.0F, 2.0F, 0.0F, false);
        dial_z.setTextureOffset(5, 351).addBox(4.9F, 0.7F, 10.0F, 2.0F, 4.0F, 1.0F, 0.0F, false);

        direction = new ModelRenderer(this);
        direction.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_5.addChild(direction);
        

        direction_screen = new ModelRenderer(this);
        direction_screen.setRotationPoint(0.0F, -70.0F, -42.0F);
        direction.addChild(direction_screen);
        setRotationAngle(direction_screen, 0.9599F, 0.0F, 0.0F);
        direction_screen.setTextureOffset(6, 343).addBox(-17.8F, -3.7F, -11.0F, 1.0F, 5.0F, 9.0F, 0.0F, false);
        direction_screen.setTextureOffset(6, 343).addBox(-8.8F, -3.7F, -11.0F, 1.0F, 8.0F, 9.0F, 0.0F, false);
        direction_screen.setTextureOffset(6, 343).addBox(-17.8F, -3.7F, -2.0F, 10.0F, 8.0F, 1.0F, 0.0F, false);

        glow_direction = new LightModelRenderer(this);
        glow_direction.setRotationPoint(0.0F, 70.0F, 42.0F);
        direction_screen.addChild(glow_direction);
        glow_direction.setTextureOffset(209, 375).addBox(-16.8F, -73.3F, -53.0F, 8.0F, 4.0F, 9.0F, 0.0F, false);

        dummy_e_1 = new ModelRenderer(this);
        dummy_e_1.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_5.addChild(dummy_e_1);
        setRotationAngle(dummy_e_1, 0.5236F, 0.0F, 0.0F);
        dummy_e_1.setTextureOffset(68, 397).addBox(-3.4F, 0.0F, 29.0F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        dummy_e_2 = new ModelRenderer(this);
        dummy_e_2.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_5.addChild(dummy_e_2);
        setRotationAngle(dummy_e_2, 0.5236F, 0.0F, 0.0F);
        dummy_e_2.setTextureOffset(68, 397).addBox(0.6F, 0.0F, 29.0F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        dummy_e_3 = new ModelRenderer(this);
        dummy_e_3.setRotationPoint(-2.0F, -63.0F, -53.0F);
        controls_5.addChild(dummy_e_3);
        setRotationAngle(dummy_e_3, 0.5236F, 0.0F, 0.0F);
        dummy_e_3.setTextureOffset(68, 397).addBox(4.6F, 0.0F, 29.0F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        decoration_5 = new ModelRenderer(this);
        decoration_5.setRotationPoint(-18.0F, -62.0F, -54.0F);
        controls_5.addChild(decoration_5);
        setRotationAngle(decoration_5, 0.5236F, 0.0F, 0.0F);
        decoration_5.setTextureOffset(136, 264).addBox(-0.8F, 0.4F, 2.4F, 12.0F, 4.0F, 12.0F, 0.0F, false);
        decoration_5.setTextureOffset(136, 264).addBox(7.8F, 0.4F, 20.0F, 20.0F, 4.0F, 8.0F, 0.0F, false);
        decoration_5.setTextureOffset(136, 264).addBox(27.8F, 0.4F, 22.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        decoration_5.setTextureOffset(136, 264).addBox(5.8F, 0.4F, 22.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        decoration_5.setTextureOffset(136, 264).addBox(6.8F, 0.4F, 21.0F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        decoration_5.setTextureOffset(136, 264).addBox(6.8F, 0.4F, 26.0F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        decoration_5.setTextureOffset(136, 264).addBox(27.8F, 0.4F, 26.0F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        decoration_5.setTextureOffset(136, 264).addBox(27.8F, 0.4F, 21.0F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        decoration_5.setTextureOffset(136, 264).addBox(25.2F, 0.4F, 2.4F, 12.0F, 4.0F, 12.0F, 0.0F, false);
        decoration_5.setTextureOffset(136, 264).addBox(15.2F, 0.4F, 2.4F, 6.0F, 4.0F, 12.0F, 0.0F, false);
        decoration_5.setTextureOffset(136, 264).addBox(17.2F, 0.4F, 0.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        decoration_5.setTextureOffset(136, 264).addBox(17.2F, 0.4F, 14.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        decoration_5.setTextureOffset(136, 264).addBox(19.2F, 0.4F, 1.4F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        decoration_5.setTextureOffset(136, 264).addBox(19.2F, 0.4F, 14.4F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        decoration_5.setTextureOffset(136, 264).addBox(16.2F, 0.4F, 1.4F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        decoration_5.setTextureOffset(136, 264).addBox(16.2F, 0.4F, 14.4F, 1.0F, 4.0F, 1.0F, 0.0F, false);

        arrows = new ModelRenderer(this);
        arrows.setRotationPoint(5.2F, 2.4F, 8.4F);
        decoration_5.addChild(arrows);
        setRotationAngle(arrows, 0.0F, -0.7854F, 0.0F);
        arrows.setTextureOffset(57, 416).addBox(-5.0F, -1.8F, -5.0F, 10.0F, 4.0F, 10.0F, 0.0F, false);

        arrows2 = new ModelRenderer(this);
        arrows2.setRotationPoint(5.2F, 2.4F, 8.4F);
        decoration_5.addChild(arrows2);
        setRotationAngle(arrows2, 0.0F, -0.7854F, 0.0F);
        arrows2.setTextureOffset(136, 264).addBox(15.6F, -1.8F, -20.5F, 8.0F, 4.0F, 7.0F, 0.0F, false);
        arrows2.setTextureOffset(76, 407).addBox(13.6F, -1.8F, -23.5F, 8.0F, 4.0F, 7.0F, 0.0F, false);

        controls_6 = new ModelRenderer(this);
        controls_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(controls_6);
        setRotationAngle(controls_6, 0.0F, -1.0472F, 0.0F);
        

        monitor = new ModelRenderer(this);
        monitor.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_6.addChild(monitor);
        

        screen = new ModelRenderer(this);
        screen.setRotationPoint(0.0F, -70.0F, -42.0F);
        monitor.addChild(screen);
        setRotationAngle(screen, 0.9599F, 0.0F, 0.0F);
        screen.setTextureOffset(208, 412).addBox(-10.0F, -1.0F, -5.0F, 20.0F, 4.0F, 15.0F, 0.0F, false);
        
        glow_screen_base = new LightModelRenderer(this);
        glow_screen_base.setRotationPoint(0.0F, 70.0F, 42.0F);
        screen.addChild(glow_screen_base);
        glow_screen_base.setTextureOffset(208, 412).addBox(-10.0F, -71.0F, -47.0F, 20.0F, 4.0F, 15.0F, 0.0F, false);        
        
        circles = new LightModelRenderer(this);
        circles.setRotationPoint(0.0F, 71.0F, 42.0F);
        screen.addChild(circles);
        circles.setTextureOffset(440, 393).addBox(-10.0F, -72.1F, -43.0F, 4.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(-6.0F, -72.1F, -42.0F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(-4.0F, -72.1F, -41.0F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(-2.0F, -72.1F, -40.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(-1.0F, -72.1F, -39.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(0.0F, -72.1F, -37.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(1.0F, -72.1F, -35.0F, 1.0F, 1.0F, 3.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(-10.0F, -72.1F, -35.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(5.0F, -72.1F, -46.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(7.0F, -72.1F, -44.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(8.0F, -72.1F, -41.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(8.0F, -72.1F, -37.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(7.0F, -72.1F, -34.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(3.0F, -72.1F, -38.0F, 4.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(6.0F, -72.1F, -39.0F, 4.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(-3.0F, -72.1F, -38.0F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(-5.0F, -72.1F, -37.0F, 3.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(-7.0F, -72.1F, -36.0F, 3.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(3.0F, -72.1F, -36.0F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(-10.0F, -72.1F, -45.0F, 5.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(-5.0F, -72.1F, -44.0F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(-3.0F, -72.1F, -43.0F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(-1.0F, -72.1F, -44.0F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(2.0F, -72.1F, -44.0F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(0.0F, -72.1F, -41.0F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(0.0F, -72.1F, -44.0F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(2.0F, -72.1F, -38.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);
        circles.setTextureOffset(440, 393).addBox(1.0F, -72.1F, -40.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);

        frame = new ModelRenderer(this);
        frame.setRotationPoint(0.0F, 70.0F, 42.0F);
        screen.addChild(frame);
        frame.setTextureOffset(4, 333).addBox(-11.0F, -72.0F, -48.0F, 1.0F, 9.0F, 16.0F, 0.0F, false);
        frame.setTextureOffset(4, 333).addBox(10.0F, -72.0F, -48.0F, 1.0F, 9.0F, 16.0F, 0.0F, false);
        frame.setTextureOffset(4, 333).addBox(-11.0F, -72.0F, -32.0F, 22.0F, 9.0F, 1.0F, 0.0F, false);
        frame.setTextureOffset(4, 333).addBox(-1.1F, -72.0F, -31.0F, 2.0F, 11.0F, 2.0F, 0.0F, false);
        frame.setTextureOffset(4, 333).addBox(-2.1F, -72.0F, -31.0F, 1.0F, 11.0F, 1.0F, 0.0F, false);
        frame.setTextureOffset(4, 333).addBox(0.9F, -72.0F, -31.0F, 1.0F, 11.0F, 1.0F, 0.0F, false);
        frame.setTextureOffset(4, 333).addBox(-10.0F, -72.0F, -48.0F, 20.0F, 5.0F, 1.0F, 0.0F, false);

        glow_camera = new LightModelRenderer(this);
        glow_camera.setRotationPoint(0.0F, 0.0F, 0.0F);
        frame.addChild(glow_camera);
        glow_camera.setTextureOffset(440, 360).addBox(-0.5F, -72.4F, -30.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        coms = new ModelRenderer(this);
        coms.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_6.addChild(coms);
        

        speaker = new ModelRenderer(this);
        speaker.setRotationPoint(-18.0F, -62.0F, -54.0F);
        coms.addChild(speaker);
        setRotationAngle(speaker, 0.5236F, 0.0F, 0.0F);
        speaker.setTextureOffset(220, 236).addBox(-3.6F, -0.6F, 4.8F, 5.0F, 4.0F, 1.0F, 0.0F, false);
        speaker.setTextureOffset(220, 236).addBox(-4.0F, -0.6F, 3.2F, 6.0F, 4.0F, 1.0F, 0.0F, false);
        speaker.setTextureOffset(220, 236).addBox(-3.6F, -0.6F, 1.6F, 5.0F, 4.0F, 1.0F, 0.0F, false);
        speaker.setTextureOffset(68, 397).addBox(-3.2F, -0.6F, -0.4F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        speaker.setTextureOffset(68, 397).addBox(0.2F, -0.6F, -0.4F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        speaker.setTextureOffset(262, 113).addBox(-5.0F, 0.0F, -1.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);

        dummy_ff_5 = new ModelRenderer(this);
        dummy_ff_5.setRotationPoint(21.0F, -62.0F, -54.0F);
        controls_6.addChild(dummy_ff_5);
        setRotationAngle(dummy_ff_5, 0.5236F, 0.0F, 0.0F);
        dummy_ff_5.setTextureOffset(68, 397).addBox(-2.0F, 0.0F, 2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        toggle_ff_5 = new ModelRenderer(this);
        toggle_ff_5.setRotationPoint(0.0F, 1.0F, 4.0F);
        dummy_ff_5.addChild(toggle_ff_5);
        setRotationAngle(toggle_ff_5, -0.5236F, 0.0F, 0.0F);
        toggle_ff_5.setTextureOffset(93, 247).addBox(-1.0F, -1.7F, -1.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        toggle_ff_5.setTextureOffset(93, 247).addBox(-1.0F, -3.7F, -0.9F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        dummy_ff_1 = new ModelRenderer(this);
        dummy_ff_1.setRotationPoint(-18.0F, -62.0F, -54.0F);
        controls_6.addChild(dummy_ff_1);
        setRotationAngle(dummy_ff_1, 0.5236F, 0.0F, 0.0F);
        dummy_ff_1.setTextureOffset(68, 397).addBox(10.0F, 0.0F, 0.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        dummy_ff_2 = new ModelRenderer(this);
        dummy_ff_2.setRotationPoint(-18.0F, -62.0F, -54.0F);
        controls_6.addChild(dummy_ff_2);
        setRotationAngle(dummy_ff_2, 0.5236F, 0.0F, 0.0F);
        dummy_ff_2.setTextureOffset(68, 397).addBox(22.4F, 0.0F, 0.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        dummy_ff_3 = new ModelRenderer(this);
        dummy_ff_3.setRotationPoint(16.0F, -62.0F, -54.0F);
        controls_6.addChild(dummy_ff_3);
        setRotationAngle(dummy_ff_3, 0.5236F, 0.0F, 0.0F);
        dummy_ff_3.setTextureOffset(68, 397).addBox(-2.0F, 0.0F, 2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        toggle_ff_3 = new ModelRenderer(this);
        toggle_ff_3.setRotationPoint(0.0F, 1.0F, 4.0F);
        dummy_ff_3.addChild(toggle_ff_3);
        setRotationAngle(toggle_ff_3, -0.5236F, 0.0F, 0.0F);
        toggle_ff_3.setTextureOffset(72, 268).addBox(-1.0F, -1.7F, -1.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        toggle_ff_3.setTextureOffset(72, 268).addBox(-1.0F, -3.7F, -0.9F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        dummy_ff_4 = new ModelRenderer(this);
        dummy_ff_4.setRotationPoint(16.0F, -65.0F, -49.0F);
        controls_6.addChild(dummy_ff_4);
        setRotationAngle(dummy_ff_4, 0.5236F, 0.0F, 0.0F);
        dummy_ff_4.setTextureOffset(68, 397).addBox(-2.0F, 0.0F, 2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        toggle_ff_4 = new ModelRenderer(this);
        toggle_ff_4.setRotationPoint(0.0F, 1.0F, 4.0F);
        dummy_ff_4.addChild(toggle_ff_4);
        setRotationAngle(toggle_ff_4, -0.5236F, 0.0F, 0.0F);
        toggle_ff_4.setTextureOffset(78, 238).addBox(-1.0F, -1.7F, -1.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        toggle_ff_4.setTextureOffset(78, 238).addBox(-1.0F, -3.7F, -0.9F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        dummy_ff_6 = new ModelRenderer(this);
        dummy_ff_6.setRotationPoint(-18.0F, -62.0F, -54.0F);
        controls_6.addChild(dummy_ff_6);
        setRotationAngle(dummy_ff_6, 0.5236F, 0.0F, 0.0F);
        dummy_ff_6.setTextureOffset(68, 397).addBox(11.0F, 0.0F, 30.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        dummy_ff_7 = new ModelRenderer(this);
        dummy_ff_7.setRotationPoint(-18.0F, -62.0F, -54.0F);
        controls_6.addChild(dummy_ff_7);
        setRotationAngle(dummy_ff_7, 0.5236F, 0.0F, 0.0F);
        dummy_ff_7.setTextureOffset(68, 397).addBox(16.0F, 0.0F, 32.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        dummy_ff_8 = new ModelRenderer(this);
        dummy_ff_8.setRotationPoint(-18.0F, -62.0F, -54.0F);
        controls_6.addChild(dummy_ff_8);
        setRotationAngle(dummy_ff_8, 0.5236F, 0.0F, 0.0F);
        dummy_ff_8.setTextureOffset(68, 397).addBox(21.0F, 0.0F, 30.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        decoration_6 = new ModelRenderer(this);
        decoration_6.setRotationPoint(-18.0F, -62.0F, -54.0F);
        controls_6.addChild(decoration_6);
        setRotationAngle(decoration_6, 0.5236F, 0.0F, 0.0F);
        decoration_6.setTextureOffset(87, 272).addBox(5.8F, 0.4F, 7.0F, 24.0F, 4.0F, 21.0F, 0.0F, false);
        decoration_6.setTextureOffset(94, 288).addBox(16.4F, 0.4F, 28.0F, 3.0F, 4.0F, 2.0F, 0.0F, false);
        decoration_6.setTextureOffset(95, 285).addBox(19.4F, 0.4F, 28.0F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        decoration_6.setTextureOffset(78, 268).addBox(15.4F, 0.4F, 28.0F, 1.0F, 4.0F, 1.0F, 0.0F, false);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void render(GalvanicConsoleTile console, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
        
        console.getControl(ThrottleControl.class).ifPresent(throttle -> {
            this.throttle_rotate_x.rotateAngleX = (float) Math.toRadians(60 - (throttle.getAmount() * 120));
        });
        
        console.getControl(HandbrakeControl.class).ifPresent(handbrake -> {
            this.handbreak_rotate_y.rotateAngleY = (float) Math.toRadians(handbrake.isFree() ? 0 : 45);
        });
        
        console.getControl(IncModControl.class).ifPresent(inc -> {
            this.position_rotate_x.rotateAngleX = (float)Math.toRadians(20 - (inc.index / (float)IncModControl.COORD_MODS.length) * 50);
        });
        
        console.getControl(RandomiserControl.class).ifPresent(rand -> {
            this.diamond.rotateAngleY = (float) Math.toRadians(45 + rand.getAnimationProgress() * 360.0F);
            this.glow_randomizer.setBright(rand.getAnimationProgress() != 0 ? 1F: 1F);
        });
        
        console.getControl(RefuelerControl.class).ifPresent(refueler -> {
            this.pointer_rotate_y.rotateAngleY = refueler.isRefueling() ? 7F: 5.5F;
        });
        
        console.getControl(DimensionControl.class).ifPresent(dim -> {
            this.glow_dim.setBright(dim.getAnimationTicks() != 0 ? 1.0F : 0.0F);
        });
        
        console.getControl(FacingControl.class).ifPresent(facing -> {
            this.glow_direction.setBright(facing.getAnimationTicks() != 0 ? 1.0F : 0.0F);
        });
        
        console.getControl(LandingTypeControl.class).ifPresent(landing -> {
            this.glow_landing.setBright(landing.getAnimationTicks() != 0 ? 1.0F : 0.0F);
        });
        
        console.getControl(DoorControl.class).ifPresent(door -> {
            
            float angle = 45;
            DoorEntity doorEntity = console.getDoor().orElse(null);
            if(doorEntity != null)
                angle += doorEntity.getOpenState() == EnumDoorState.CLOSED ? 0 : 45;
            
            angle -= door.getAnimationProgress() * 360.0;
            
            this.key_rotate_y.rotateAngleY = (float)Math.toRadians(angle);
        });
        
        this.rotor_rotate_y.rotateAngleY = (float) Math.toRadians(MathHelper.lerp(Minecraft.getInstance().getRenderPartialTicks(), console.prevFlightTicks, console.flightTicks));
        
        this.glow_c8.setBright(1F);
        this.glow_c9.setBright(1F);
        this.glow_camera.setBright(1F);
        this.glow_coil_1.setBright(1F);
        this.glow_coil_1.setBright(1F);
        this.glow_coil_2.setBright(1F);
        this.glow_coil_3.setBright(1F);
        this.glow_coil_4.setBright(1F);
        this.glow_coil_5.setBright(1F);
        this.glow_coil_6.setBright(1F);
        this.glow_shaft_1.setBright(1F);
        this.glow_shaft_2.setBright(1F);
        this.glow_shaft_3.setBright(1F);
        this.glow_screen_base.setBright(1F);
        this.circles.setBright(1F);
        this.round.setBright(1F);
        this.glow_tp_screen.setBright(1F);
        
        this.glow_ball_1.setBright(1F);
        this.glow_ball_2.setBright(1F);
        this.glow_ball_3.setBright(1F);
        this.glow_ball_4.setBright(1F);
        this.glow_ball_5.setBright(1F);
        this.glow_ball_6.setBright(1F);
        
        int glowTime = (int)(console.getWorld().getGameTime() % 120);
        
        this.dummy_aa_1.setBright(glowTime > 60 ? 1.0F : 0.0F);
        this.dummy_aa_5.setBright(glowTime > 60 ? 1.0F : 0.0F);
        this.dummy_aa_4.setBright(glowTime > 30 ? 1.0F : 0.0F);
        this.dummy_aa_2.setBright(glowTime < 30 ? 1.0F : 0.0F);
        this.dummy_aa_6.setBright(glowTime < 20 ? 1.0F : 0.0F);
        
        this.dummy_d_1.setBright(1F);
        this.dummy_d_2.setBright(glowTime < 20 ? 1.0F : 0.0F);
        this.dummy_d_3.setBright(glowTime > 50 ? 1.0F : 0.0F);
        this.dummy_d_4.setBright(glowTime > 20 ? 1.0F : 0.0F);
        this.dummy_d_5.setBright(glowTime < 50 ? 1.0F : 0.0F);
        
        this.glow_shaft_1.render(matrixStack, buffer, packedLight, packedOverlay);
		this.glow_shaft_2.render(matrixStack, buffer, packedLight, packedOverlay);
		this.glow_shaft_3.render(matrixStack, buffer, packedLight, packedOverlay);
		this.console.render(matrixStack, buffer, packedLight, packedOverlay);
		this.rotor_rotate_y.render(matrixStack, buffer, packedLight, packedOverlay);
		this.controls.render(matrixStack, buffer, packedLight, packedOverlay);
    }

    @Override
    public void translateMonitorPos(MatrixStack stack) {
        this.controls.translateRotate(stack);
        this.controls_6.translateRotate(stack);
        this.monitor.translateRotate(stack);
        this.screen.translateRotate(stack);
        
    }
}