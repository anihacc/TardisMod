package net.tardis.mod.client.guis.minigame.circuit;

import java.util.List;

import org.lwjgl.opengl.GL11;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.client.guis.minigame.circuit.piece.DiagonalRightPiece;
import net.tardis.mod.helper.Helper;

public class CircuitGame extends Screen{

	public static final StringTextComponent TITLE = new StringTextComponent("Circuit Minigame");
	public static int GRID_WIDTH = 128;
	public static int GRID_HEIGHT = 66;
	
	private List<Piece> pieces = Lists.newArrayList();
	private Piece currentPiece;
	private int timeLeft = 5 * 20;
	
	public CircuitGame() {
		super(TITLE);
		this.pieces.add(this.currentPiece = new DiagonalRightPiece(30, 30));
	}

	@Override
	public void tick() {
		super.tick();
		
		if(timeLeft > 0) {
			--this.timeLeft;
			if(this.timeLeft == 0)
				this.onPieceExpired();
		}
		
	}
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		
		super.render(matrixStack, mouseX, mouseY, partialTicks);
		
		this.renderBackground(matrixStack);
		
		Minecraft.getInstance().textureManager.bindTexture(Helper.createRL("textures/gui/minigame/circuit.png"));
		this.blit(matrixStack, width / 2 - 176 / 2, height / 2 - 122 / 2, 0, 0, 176, 122);
		
		if(this.currentPiece != null) {
			Vector3i pos = getClosestPoint(this.currentPiece, mouseX, mouseY);
			this.currentPiece.x = pos.getX();
			this.currentPiece.y = pos.getY();
		}
		
		BufferBuilder bb = Tessellator.getInstance().getBuffer();
		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		for(Piece p : this.pieces) {
			p.render(bb);
		}
		Tessellator.getInstance().draw();
		
	}
	
	private void onPieceExpired() {
		//Five seconds
		this.timeLeft = (5 * 20);
	}

	public static boolean isInBounds(int mouseX, int mouseY) {
		return true;
	}
	
	public Vector3i getClosestPoint(Piece piece, int mouseX, int mouseY) {
		
		int x = mouseX;
		int y = mouseY;
		
		int left = (width / 2 - 176 / 2) + 25;
		int right = left + 110;
		if(x < left)
			x = left;
		else if(x > right)
			x = right;
		
		int top = ((height - 122) / 2) + 13;
		int bottom = top + 50;
		if(y < top)
			y = top;
		else if(y > bottom)
			y = bottom;
		
		
		
		return new Vector3i(x, y, 0);
	}

}
