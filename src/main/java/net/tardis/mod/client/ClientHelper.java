package net.tardis.mod.client;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.apache.logging.log4j.Level;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.network.play.NetworkPlayerInfo;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.texture.NativeImage;
import net.minecraft.client.resources.DefaultPlayerSkin;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.resources.IResource;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.DimensionType;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.settings.NoiseSettings;
import net.minecraftforge.common.MinecraftForge;
import net.tardis.api.events.DimensionLightMapModificationEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.client.guis.ARSTabletKillScreen;
import net.tardis.mod.client.guis.ARSTabletScreen;
import net.tardis.mod.client.guis.ArsScreen;
import net.tardis.mod.client.guis.CommunicatorScreen;
import net.tardis.mod.client.guis.DiagnosticGui;
import net.tardis.mod.client.guis.DialogGui;
import net.tardis.mod.client.guis.MonitorRemoteGui;
import net.tardis.mod.client.guis.TardisSendDistressScreen;
import net.tardis.mod.client.guis.TelepathicScreen;
import net.tardis.mod.client.guis.TransductionBarrierEdit;
import net.tardis.mod.client.guis.manual.ManualScreen;
import net.tardis.mod.client.guis.monitors.CoralMonitorScreen;
import net.tardis.mod.client.guis.monitors.EyeMonitorScreen;
import net.tardis.mod.client.guis.monitors.GalvanicMonitorScreen;
import net.tardis.mod.client.guis.monitors.RCAMonitorScreen;
import net.tardis.mod.client.guis.monitors.SpinMonitorScreen;
import net.tardis.mod.client.guis.monitors.SteamMonitorScreen;
import net.tardis.mod.client.guis.monitors.ToyotaMonitorScreen;
import net.tardis.mod.client.guis.monitors.XionMonitorScreen;
import net.tardis.mod.client.guis.radial.SonicModeScreen;
import net.tardis.mod.client.guis.vm.VortexMDistressScreen;
import net.tardis.mod.client.guis.vm.VortexMGui;
import net.tardis.mod.client.guis.vm.VortexMTeleportGui;
import net.tardis.mod.client.models.entity.SpacesuitModel;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.constants.TardisConstants.Gui;
import net.tardis.mod.contexts.gui.EntityContext;
import net.tardis.mod.contexts.gui.GuiContextDiagnostic;
import net.tardis.mod.entity.humanoid.AbstractHumanoidEntity;
import net.tardis.mod.experimental.sound.EntityMovingSound;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.sounds.TSounds;
/**
 * Seperate Class for all Physical Client side only methods
 *
 */
public class ClientHelper {
    /** Cached list of Image Resource locations for Datapack interiors that use an image from the internet*/
    public static final HashMap<ConsoleRoom, ResourceLocation> INTERIOR_PREVIEW_TEXTURES = new HashMap<>();
    
    private static final SpacesuitModel SUIT_HEAD = new SpacesuitModel(1F, EquipmentSlotType.HEAD);
    private static final SpacesuitModel SUIT_BODY = new SpacesuitModel(1F, EquipmentSlotType.CHEST);
    private static final SpacesuitModel SUIT_LEGS = new SpacesuitModel(1F, EquipmentSlotType.LEGS);
    private static final SpacesuitModel SUIT_FEET = new SpacesuitModel(1F, EquipmentSlotType.FEET);
    
    public static List<Entity> ENTITIES_HURT_BY_LASER = new ArrayList<>();
    
    public static Map<Item, BipedModel<?>> getSpacesuitArmorModel(ItemStack itemStack) {
        Map<Item, BipedModel<?>> map = new HashMap<>();
        map.put(TItems.SPACE_HELM.get(), SUIT_HEAD);
        map.put(TItems.SPACE_CHEST.get(), SUIT_BODY);
        map.put(TItems.SPACE_LEGS.get(), SUIT_LEGS);
        map.put(TItems.SPACE_BOOTS.get(), SUIT_FEET);
        return map;
    }
    
    public static ResourceLocation urlToTexture(URL url) {
        URLConnection uc = null;
        NativeImage image = null;
        try {
            uc = url.openConnection();
            uc.connect();
            uc = url.openConnection();
            uc.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36");
            Tardis.LOGGER.warn("Downloading Image from: {}", url.toString());
            image = NativeImage.read(uc.getInputStream());
            return Minecraft.getInstance().getTextureManager().getDynamicTextureLocation("interior_", new DynamicTexture(image));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return DefaultPlayerSkin.getDefaultSkinLegacy();
    }
    
    /** Updates the lightmap in LightTexture, used for adjusting dimension lightmap colours. Called from LightTextureMixin*/
    public static void updateLightmap(float partialTicks, ClientWorld clientworld, float f, float f1, float f3, float f2, Vector3f vector3f, float f4, Vector3f lightMapColours, int i, int j, float blockBrightness, float f6, float f7, float f8) {
        //Do not directly manipulate lightmap colours here, because you can accidentally change the colours used for the game HUD
        //Use the event instead
        blockBrightness = onDimensionLightMapUpdate(clientworld, partialTicks, blockBrightness, lightMapColours).getBlockBrightness();
    }
    
    public static DimensionLightMapModificationEvent onDimensionLightMapUpdate(World world, float partialTicks, float blockBrightness, Vector3f lightMapColors){
        DimensionLightMapModificationEvent event = new DimensionLightMapModificationEvent(world, partialTicks, blockBrightness, lightMapColors);
        MinecraftForge.EVENT_BUS.post(event);
        return event;
    }

    public static ITextComponent getUsernameFromUUID(UUID id) {
        
        StringTextComponent idComp = new StringTextComponent(id.toString());
        
        if(Minecraft.getInstance().getConnection() != null) {
            NetworkPlayerInfo info = Minecraft.getInstance().getConnection().getPlayerInfo(id);
            if(info != null && info.getGameProfile() != null) {
                String name = info.getGameProfile().getName();
                return name == null ? idComp : new StringTextComponent(name);
            }
        }
        return idComp;
    }
    
    public static void openGui(Screen screen) {
        Minecraft.getInstance().displayGuiScreen(screen);
    }
    
    public static void openGUI(int guiId, GuiContext context) {
        switch (guiId) {
            case TardisConstants.Gui.MONITOR_MAIN_STEAM:
                Minecraft.getInstance().displayGuiScreen(new SteamMonitorScreen());
                //Minecraft.getInstance().displayGuiScreen(new MonitorTitleScreen(new SteamMonitorScreen()));
                break;
            case TardisConstants.Gui.VORTEX_MAIN:
                Minecraft.getInstance().displayGuiScreen(new VortexMGui());
                break;
            case TardisConstants.Gui.VORTEX_TELE:
                Minecraft.getInstance().displayGuiScreen(new VortexMTeleportGui());
                break;
            case TardisConstants.Gui.VORTEX_DISTRESS:
                Minecraft.getInstance().displayGuiScreen(new VortexMDistressScreen());
                break;
            case TardisConstants.Gui.MONITOR_MAIN_EYE:
                Minecraft.getInstance().displayGuiScreen(new EyeMonitorScreen());
                break;
            case TardisConstants.Gui.MANUAL:
                Minecraft.getInstance().displayGuiScreen(new ManualScreen(context));
                break;
            case TardisConstants.Gui.ARS_EGG:
                Minecraft.getInstance().displayGuiScreen(new ArsScreen());
                break;
            case TardisConstants.Gui.TELEPATHIC:
                Minecraft.getInstance().displayGuiScreen(new TelepathicScreen());
                break;
            case TardisConstants.Gui.MONITOR_MAIN_GALVANIC:
                Minecraft.getInstance().displayGuiScreen(new GalvanicMonitorScreen());
                break;
            case TardisConstants.Gui.MONITOR_MAIN_RCA:
                Minecraft.getInstance().displayGuiScreen(new RCAMonitorScreen());
                break;
            case TardisConstants.Gui.ARS_TABLET:
                //Minecraft.getInstance().displayGuiScreen(new ArsTabletScreen(context));
                Minecraft.getInstance().displayGuiScreen(new ARSTabletScreen(context));
                break;
            case TardisConstants.Gui.COMMUNICATOR:
                Minecraft.getInstance().displayGuiScreen(new CommunicatorScreen());
                break;
            case Gui.TRANSDUCTION_BARRIER_EDIT:
                Minecraft.getInstance().displayGuiScreen(new TransductionBarrierEdit(context));
                break;
            case Gui.ARS_TABLET_KILL:
                Minecraft.getInstance().displayGuiScreen(new ARSTabletKillScreen(context));
                break;
            case Gui.MONITOR_MAIN_XION:
                Minecraft.getInstance().displayGuiScreen(new XionMonitorScreen());
                break;
            case Gui.MONITOR_MAIN_TOYOTA:
                Minecraft.getInstance().displayGuiScreen(new ToyotaMonitorScreen());
                break;
            case Gui.MONITOR_MAIN_CORAL:
                Minecraft.getInstance().displayGuiScreen(new CoralMonitorScreen());
                break;
            case Gui.MONITOR_MAIN_ROTATE:
                Minecraft.getInstance().displayGuiScreen(new SpinMonitorScreen());
                break;
            case Gui.DIAGNOSTIC:
                Minecraft.getInstance().displayGuiScreen(new DiagnosticGui((GuiContextDiagnostic) context));
                break;
            case Gui.MONITOR_REMOTE:
                Minecraft.getInstance().displayGuiScreen(new MonitorRemoteGui(context));
                break;
            case Gui.TARDIS_DISTRESS:
                openGui(new TardisSendDistressScreen());
                break;
            case Gui.DIALOG:
                openGui(new DialogGui((AbstractHumanoidEntity) ((EntityContext) context).entity));
                break;
            case Gui.SONIC_MODE:
                Minecraft.getInstance().displayGuiScreen(new SonicModeScreen(new StringTextComponent("")));
                break;
            case TardisConstants.Gui.NONE:
                Minecraft.getInstance().displayGuiScreen(null);
                break;
            default:
                break;
        }

    }
    /**
     * Play a sound that moves with the entity
     * @param entity
     * @param soundEvent
     * @param soundCategory
     * @param vol
     * @param repeat
     */
    public static void playMovingSound(Entity entity, SoundEvent soundEvent, SoundCategory soundCategory, float vol, boolean repeat) {
        Minecraft.getInstance().getSoundHandler().play(new EntityMovingSound(entity, soundEvent, soundCategory, vol, repeat));
    }
    
    /**
     * Stops a sound on the physical client
     * @param event - SoundEvent - the sound to stop
     * @param type - Category to stop it in - E.g. Block, Ambient, Player
     */
    public static void shutTheFuckUp(SoundEvent event, SoundCategory type) {
        Minecraft.getInstance().getSoundHandler().stop(event == null ? null : event.getRegistryName(), type);
    }
    
    public static World getClientWorld() {
        return Minecraft.getInstance().world;
    }
    
    public static ClientWorld getClientWorldCasted() {
        return Minecraft.getInstance().world;
    }

    public static PlayerEntity getClientPlayer() {
        return Minecraft.getInstance().player;
    }
    
    
    /**
     * Get a Dimension Type from a client world
     * @param world - ClientWorld
     * @param dimTypeKey
     * @return
     */
    public static DimensionType getDimensionType(ClientWorld world, RegistryKey<DimensionType> dimTypeKey){
        return world.func_241828_r() // get dynamic registries
            .getRegistry(Registry.DIMENSION_TYPE_KEY)
            .getOrThrow(dimTypeKey);
    }
    
    public static void playSteamSoundOnClient(World world, BlockPos pos) {
        world.playSound(Minecraft.getInstance().player, pos, TSounds.STEAM_HISS.get(), SoundCategory.BLOCKS, 1.0F, 1.0F);
    }
    
    public static JsonObject getResourceAsJson(ResourceLocation loc) {
        try{
            IResource resource = Minecraft.getInstance().getResourceManager().getResource(loc);
            if(resource != null){
                return new JsonParser().parse(new InputStreamReader(resource.getInputStream())).getAsJsonObject();
            }
        }
        catch(IOException e){
            Tardis.LOGGER.log(Level.ALL, "Error occured parsing json file " + loc.toString());
            Tardis.LOGGER.catching(Level.ALL, e);
        }
        return null;
    }
    /** NOTE: Some registries such as ConfiguredStructureFeature, Dimension are not present on the client's version of Dynamic Registry
     * <br> If you try to access those they won't exist and will crash servers
     * <p> The following registries are available on the client's DynamicRegistries:
     * <br> - {@linkplain DimensionType}
     * <br> - {@linkplain Biome}
     * <br> - {@linkplain NoiseSettings}*/
    public static <T> Optional<T> getItemFromDynamicRegistry(RegistryKey<Registry<T>> registry, ResourceLocation key){
        return Minecraft.getInstance().world.func_241828_r().getRegistry(registry).getOptional(key);
    }
    
    public static float getPocketWatchRotFromTime(ClientWorld world) {
        //float per = world % 12000.0F / 12000.0F;
        //float rot = per * 360.0F;
//        float rot = ((world.calculateCelestialAngle(world.getDayTime(), 0) * 2.0F) % 360.0F) * 360.0F;
    	float rot = ((world.getCelestialAngleRadians(world.getDayTime()) * 2.0F) % 360.0F) * 360.0F;
        if (rot > 315)
            return 7;
        if (rot > 270)
            return 6;
        if (rot > 225)
            return 5;
        if (rot > 180)
            return 4;
        if (rot > 135)
            return 3;
        if (rot > 90)
            return 2;
        if (rot > 45)
            return 1;

        return 0F;
    }
    
}
