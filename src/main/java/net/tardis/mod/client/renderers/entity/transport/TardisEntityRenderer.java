package net.tardis.mod.client.renderers.entity.transport;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.renderers.exteriors.ExteriorRenderer;
import net.tardis.mod.entity.TardisEntity;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class TardisEntityRenderer extends EntityRenderer<TardisEntity>{
	
	public TardisEntityRenderer(EntityRendererManager renderManager) {
		super(renderManager);
	}
	
	

	@Override
	public void render(TardisEntity entity, float entityYaw, float partialTicks, MatrixStack matrixStackIn,
	        IRenderTypeBuffer bufferIn, int packedLightIn) {
		matrixStackIn.push();
		matrixStackIn.translate(-0.5, 1, -0.5);

		if(entity.hasNoGravity())
			matrixStackIn.translate(0, Math.sin(entity.ticksExisted * 0.05) * 0.1, 0);
		if(entity.getExteriorTile() != null) {
			TileEntityRendererDispatcher tileRendererDispatcher = TileEntityRendererDispatcher.instance;
        	TileEntityRenderer<ExteriorTile> tileRenderer = tileRendererDispatcher.getRenderer(entity.getExteriorTile());
            if (tileRenderer instanceof ExteriorRenderer) {
            	ExteriorRenderer<? extends ExteriorTile> extRenderer = (ExteriorRenderer<? extends ExteriorTile>)tileRenderer;
            	//Use a custom method to render the tile as an entity
            	//This is because the cachedBlockState of our tile entity will be null or Air when the exterior block is removed due to "falling", so the regular TileEntity renderer will not work
            	//Instead, we use a raw render method to achieve this.
            	extRenderer.renderExteriorAsEntity(entity.getExteriorTile(), partialTicks, matrixStackIn, bufferIn, packedLightIn, OverlayTexture.NO_OVERLAY);
            }
		}
		matrixStackIn.pop();
	}

	@Override
	public ResourceLocation getEntityTexture(TardisEntity entity) {
		return null;
	}

}
