package net.tardis.mod.client.renderers.monitor;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.monitors.MonitorTile;

public class MonitorRenderer extends TileEntityRenderer<MonitorTile> {
    
    public MonitorRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
        super(rendererDispatcherIn);
    }

    private static WorldText monitor = new WorldText(0.75F, 0.5F, 0.006F, 0xFFFFFF);
    
    
    //TODO: Clean this up
    @Override
    public void render(MonitorTile tile, float partialTicks, MatrixStack matrixStackIn,
            IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
        
        if(tile.getInfo() == null)
            return;
        
        
        matrixStackIn.push();
        Minecraft.getInstance().gameRenderer.getLightTexture().disableLightmap();
        RenderSystem.color3f(1F, 1F, 1F);
        matrixStackIn.translate(0.5, 1, 0.5);
        
        if (tile.getBlockState().getMaterial() != Material.AIR) {
        	Direction facing = tile.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING);
            boolean isHanging = tile.getBlockState().get(BlockStateProperties.HANGING);
            if (isHanging) { //If in hanging state
                if (tile.getBlockState().getBlock().equals(TBlocks.steampunk_monitor.get())) {
                    if (facing == Direction.NORTH) {
                        matrixStackIn.rotate(Vector3f.XN.rotationDegrees(23));
                    }
                    else if (facing == Direction.EAST) {
                        matrixStackIn.rotate(Vector3f.ZN.rotationDegrees(23));
                    }
                    else if (facing == Direction.SOUTH) {
                        matrixStackIn.rotate(Vector3f.XP.rotationDegrees(23));
                    }
                    else if (facing == Direction.WEST) {
                        matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(23));    
                    }
                    matrixStackIn.rotate(Vector3f.YN.rotationDegrees(WorldHelper.getAngleFromFacing(facing)));
                    matrixStackIn.translate(0, 0, -3.1 / 16.0);
                }
                else  { //Have to do this check, because both blocks have the hanging property...
                    matrixStackIn.rotate(Vector3f.YN.rotationDegrees(WorldHelper.getAngleFromFacing(facing)));
                }
            }
            else { //When in Standing mode or wall mode
                matrixStackIn.rotate(Vector3f.YN.rotationDegrees(WorldHelper.getAngleFromFacing(facing)));
            }
            matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(180)); // Rotates text 180 degrees - so it's the right way up
            matrixStackIn.translate(tile.startX, tile.startY, tile.startZ);
            monitor.renderText(matrixStackIn, bufferIn, LightModelRenderer.MAX_LIGHT, tile.getInfo());
        }
        
        Minecraft.getInstance().gameRenderer.getLightTexture().enableLightmap();

        matrixStackIn.pop();
        
    }
}
