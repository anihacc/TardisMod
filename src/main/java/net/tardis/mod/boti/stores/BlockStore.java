package net.tardis.mod.boti.stores;

import java.util.Optional;

import com.mojang.datafixers.util.Pair;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.NBTDynamicOps;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.LightType;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;

public class BlockStore {

    private BlockState state = Blocks.AIR.getDefaultState();
    private FluidState fluidState = Fluids.EMPTY.getDefaultState();
    private int lightVal = 0;
    private int skyLightLevel = 0;

    public BlockStore(BlockState state, int lightValue, int sktLight) {
        this.state = state;
        this.lightVal = lightValue;
        this.skyLightLevel = sktLight;
    }
    
    public BlockStore(BlockState state, int lightValue, int skyLight, FluidState fluidState) {
        this(state, lightValue, skyLight);
        this.fluidState = fluidState;
    }

    public BlockStore(PacketBuffer buf) {
        this.decode(buf);
    }
    
    public BlockStore(World world, BlockPos pos) {
        this(world.getBlockState(pos), Math.max(world.getChunkProvider().getLightManager().getLightEngine(LightType.BLOCK).getLightFor(pos), world.getBlockState(pos).getLightValue(world, pos)), world.getLightFor(LightType.SKY, pos), world.getFluidState(pos));

    }

    public BlockState getState() {
        return this.state;
    }

    public int getLight() {
        return this.lightVal;
    }

    public int getSkyLight() {
        return this.skyLightLevel;
    }
    
    public FluidState getFluidState() {
    	return this.fluidState;
    }

    public CompoundNBT serialize() {
        CompoundNBT nbt = new CompoundNBT();
        nbt.put("state", NBTUtil.writeBlockState(state));
        nbt.putInt("light", lightVal);
        FluidState.field_237213_a_.encodeStart(NBTDynamicOps.INSTANCE, this.fluidState)
        .resultOrPartial(Tardis.LOGGER::error)
        .ifPresent(data -> {
        	nbt.put("fluid_state", data);
        });
        return nbt;
    }
    
    public static BlockStore deserialize(CompoundNBT tag) {
    	BlockState state = NBTUtil.readBlockState(tag.getCompound("state"));
    	int light = tag.getInt("light");
    	int skyLight = tag.getInt("sky_light");

    	Optional<Pair<FluidState, INBT>> result = FluidState.field_237213_a_.decode(NBTDynamicOps.INSTANCE, tag.get("fluid_state")).resultOrPartial(Tardis.LOGGER::error);
    	FluidState fluidState = result.get().getFirst();
    	if (fluidState != null) {
    		return new BlockStore(state, light, skyLight, fluidState);
    	}
        return new BlockStore(state, light, skyLight);
    }

    public void encode(PacketBuffer buf) {
        buf.writeCompoundTag(NBTUtil.writeBlockState(state));
        buf.writeInt(this.lightVal);
        buf.writeInt(this.skyLightLevel);
        buf.writeCompoundTag((CompoundNBT)FluidState.field_237213_a_.encodeStart(NBTDynamicOps.INSTANCE, this.fluidState).result().orElse(new CompoundNBT()));
    }

    public void decode(PacketBuffer buf) {
        this.state = NBTUtil.readBlockState(buf.readCompoundTag());
        this.lightVal = buf.readInt();
        this.skyLightLevel = buf.readInt();
        FluidState.field_237213_a_.decode(NBTDynamicOps.INSTANCE, buf.readCompoundTag()).result().ifPresent(data -> {
        	this.fluidState = data.getFirst(); 
        });
    }


    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof BlockStore))
            return false;

        BlockStore other = (BlockStore) obj;
        if (other.fluidState != null) {
        	if (this.state.equals(other.state) && this.fluidState.equals(other.fluidState))
                return true;
        }
        else {
        	if (this.state.equals(other.state))
        			return true;
        }
        
        return false;

    }

    @Override
    public int hashCode() {
        return state.hashCode();
   }
}
