package net.tardis.mod.boti.stores;

import net.minecraft.entity.Entity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.World;
import net.tardis.mod.boti.BotiWorld;

public abstract class AbstractEntityStorage {

    public AbstractEntityStorage(PacketBuffer buf) {
        this.decode(buf);
    }

    public AbstractEntityStorage() {
    }

    public abstract void encode(PacketBuffer buf);

    public abstract void decode(PacketBuffer buf);
}
