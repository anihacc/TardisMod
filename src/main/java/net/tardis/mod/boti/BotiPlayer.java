package net.tardis.mod.boti;

import com.mojang.authlib.GameProfile;
import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.world.ClientWorld;

public class BotiPlayer extends AbstractClientPlayerEntity {

    public BotiPlayer(ClientWorld world, GameProfile profile) {
        super(world, profile);
    }
}
