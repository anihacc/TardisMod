package net.tardis.api.events;

import net.minecraft.entity.Entity;
import net.minecraftforge.event.entity.EntityEvent;

/**
 * <p>Allows modders to handle what happens when an {@code Entity} falls out of the bottom of a world.</p>
 * <p>This event is cancellable. If cancelled, the vanilla behavior will be used.
 * <p>This event fired in {@link EntityMixin#handleOutOfWorld}, on the
 * {@link net.minecraftforge.common.MinecraftForge#EVENT_BUS} on both the
 * {@linkplain net.minecraftforge.fml.LogicalSide#SERVER server-side} and {@linkplain net.minecraftforge.fml.LogicalSide#CLIENT client-side}.
 */
@net.minecraftforge.eventbus.api.Cancelable
public class EntityOutOfWorldEvent extends EntityEvent {

	public EntityOutOfWorldEvent(Entity entity) {
		super(entity);
	}

}
